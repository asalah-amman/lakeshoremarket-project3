/*
Created		9/16/2017
Modified		9/29/2017
Project		
Model		
Company		
Author		
Version		
Database		mySQL 5 
*/


drop table IF EXISTS Customer_Payment_Types;
drop table IF EXISTS Partner_Settlements;
drop table IF EXISTS Product_Reviews;
drop table IF EXISTS Pick_Up_Locations;
drop table IF EXISTS Phone_Type;
drop table IF EXISTS Customer_Phone;
drop table IF EXISTS Order_Details;
drop table IF EXISTS Partner_Contacts;
drop table IF EXISTS Partners;
drop table IF EXISTS Orders;
drop table IF EXISTS Address_Types;
drop table IF EXISTS Customer_Addresses;
drop table IF EXISTS Geography_Levels;
drop table IF EXISTS Geographies;
drop table IF EXISTS Customers;
drop table IF EXISTS ProductType;
drop table IF EXISTS Products;


Create table Products (
	product_id Bigint NOT NULL AUTO_INCREMENT,
	product_type_id Bigint NOT NULL,
	partner_id Bigint NOT NULL,
	product_name Varchar(50),
	product_desc Varchar(1000),
	price Float,
	available_quantity Int,
	is_active Tinyint,
	isbn Varchar(50),
	author Varchar(50),
	manufacturer Varchar(50),
	model_number Varchar(50),
	carrier Varchar(50),
	two_in_one Tinyint,
 Primary Key (product_id)) ENGINE = MyISAM;

Create table ProductType (
	product_type_id Bigint NOT NULL AUTO_INCREMENT,
	product_type_name Varchar(50),
	product_type_desc Varchar(1000),
	UNIQUE (product_type_name),
 Primary Key (product_type_id)) ENGINE = MyISAM;

Create table Customers (
	customer_id Bigint NOT NULL AUTO_INCREMENT,
	first_name Varchar(50) NOT NULL,
	last_name Varchar(50) NOT NULL,
	email Varchar(50),
	is_active Tinyint,
	acct_activation_date Date,
	is_pending_approval Tinyint,
 Primary Key (customer_id)) ENGINE = MyISAM;

Create table Geographies (
	geography_id Bigint NOT NULL AUTO_INCREMENT,
	level_id Int NOT NULL,
	parent_id Bigint NOT NULL,
	geography_name Varchar(50) NOT NULL,
	geography_code Varchar(10),
 Primary Key (geography_id)) ENGINE = MyISAM;

Create table Geography_Levels (
	level_id Int NOT NULL AUTO_INCREMENT,
	level_name Varchar(50),
 Primary Key (level_id)) ENGINE = MyISAM;

Create table Customer_Addresses (
	customer_address_id Bigint NOT NULL AUTO_INCREMENT,
	customer_id Bigint NOT NULL,
	address_type_id Int NOT NULL,
	street_address Varchar(100) NOT NULL,
	city_id Bigint NOT NULL,
	zip Varchar(10) NOT NULL,
	state_id Bigint NOT NULL,
	country_id Bigint NOT NULL,
	is_active Tinyint,
 Primary Key (customer_address_id)) ENGINE = MyISAM;

Create table Address_Types (
	address_type_id Int NOT NULL AUTO_INCREMENT,
	type_name Varchar(50) NOT NULL,
 Primary Key (address_type_id)) ENGINE = MyISAM;

Create table Orders (
	order_id Bigint NOT NULL AUTO_INCREMENT,
	customer_id Bigint NOT NULL,
	customer_address_id Bigint NOT NULL,
	order_date_time Datetime NOT NULL,
	order_total Float,
	is_canceled Tinyint,
	status_id Int,
 Primary Key (order_id)) ENGINE = MyISAM;

Create table Partners (
	partner_id Bigint NOT NULL AUTO_INCREMENT,
	street_address Varchar(50) NOT NULL,
	city_id Bigint NOT NULL,
	zip Varchar(10) NOT NULL,
	state_id Bigint NOT NULL,
	country_id Bigint NOT NULL,
	first_name Varchar(50),
	last_name Varchar(50),
	is_pending_approval Tinyint,
	acct_approval_date Date,
	is_active Tinyint,
	UNIQUE (first_name,last_name,street_address),
 Primary Key (partner_id)) ENGINE = MyISAM;

Create table Partner_Contacts (
	partner_contract_id Bigint NOT NULL AUTO_INCREMENT,
	partner_id Bigint NOT NULL,
	first_name Varchar(50) NOT NULL,
	last_name Varchar(50),
	email Varchar(50),
	phone_number Varchar(15),
	is_active Tinyint,
 Primary Key (partner_contract_id)) ENGINE = MyISAM;

Create table Order_Details (
	order_id Bigint NOT NULL,
	product_id Bigint NOT NULL,
	quantity Int NOT NULL,
	price Float NOT NULL,
	item_removed Tinyint,
 Primary Key (order_id,product_id)) ENGINE = MyISAM;

Create table Customer_Phone (
	phone_id Int NOT NULL AUTO_INCREMENT,
	phone_type_id Int NOT NULL,
	customer_id Bigint NOT NULL,
	phone_number Varchar(20),
	is_active Tinyint,
 Primary Key (phone_id)) ENGINE = MyISAM;

Create table Phone_Type (
	phone_type_id Int NOT NULL AUTO_INCREMENT,
	type_name Varchar(50),
	UNIQUE (type_name),
 Primary Key (phone_type_id)) ENGINE = MyISAM;

Create table Pick_Up_Locations (
	location_id Bigint NOT NULL AUTO_INCREMENT,
	zip_id Bigint NOT NULL,
	location_name Varchar(50),
	street_address Varchar(50),
	phone_number Varchar(15),
 Primary Key (location_id)) ENGINE = MyISAM;

Create table Product_Reviews (
	review_id Int NOT NULL AUTO_INCREMENT,
	customer_id Bigint NOT NULL,
	product_id Bigint NOT NULL,
	review Varchar(4000) NOT NULL,
	review_date Date NOT NULL,
 Primary Key (review_id)) ENGINE = MyISAM;

Create table Partner_Settlements (
	settlement_id Bigint NOT NULL AUTO_INCREMENT,
	order_id Bigint NOT NULL,
	product_id Bigint NOT NULL,
	settlement_date Date NOT NULL,
	settlement_amount Float NOT NULL,
 Primary Key (settlement_id)) ENGINE = MyISAM;

Create table Customer_Payment_Types (
	payment_type_id Int NOT NULL AUTO_INCREMENT,
	customer_id Bigint NOT NULL,
	network_type_id Int NOT NULL,
	acct_name Varchar(20) NOT NULL,
	account_number Varchar(20) NOT NULL,
	acct_exp_date Date,
	security_code Int NOT NULL,
	is_default Tinyint,
	is_active Tinyint,
 Primary Key (payment_type_id)) ENGINE = MyISAM;


Alter table Order_Details add Foreign Key (product_id) references Products (product_id) on delete  restrict on update  restrict;
Alter table Product_Reviews add Foreign Key (product_id) references Products (product_id) on delete  restrict on update  restrict;
Alter table Products add Foreign Key (product_type_id) references ProductType (product_type_id) on delete  restrict on update  restrict;
Alter table Customer_Addresses add Foreign Key (customer_id) references Customers (customer_id) on delete  restrict on update  restrict;
Alter table Orders add Foreign Key (customer_id) references Customers (customer_id) on delete  restrict on update  restrict;
Alter table Customer_Phone add Foreign Key (customer_id) references Customers (customer_id) on delete  restrict on update  restrict;
Alter table Product_Reviews add Foreign Key (customer_id) references Customers (customer_id) on delete  restrict on update  restrict;
Alter table Customer_Payment_Types add Foreign Key (customer_id) references Customers (customer_id) on delete  restrict on update  restrict;
Alter table Geographies add Foreign Key (parent_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Customer_Addresses add Foreign Key (city_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Customer_Addresses add Foreign Key (state_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Customer_Addresses add Foreign Key (country_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Partners add Foreign Key (city_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Partners add Foreign Key (state_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Partners add Foreign Key (country_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Pick_Up_Locations add Foreign Key (zip_id) references Geographies (geography_id) on delete  restrict on update  restrict;
Alter table Geographies add Foreign Key (level_id) references Geography_Levels (level_id) on delete  restrict on update  restrict;
Alter table Orders add Foreign Key (customer_address_id) references Customer_Addresses (customer_address_id) on delete  restrict on update  restrict;
Alter table Customer_Addresses add Foreign Key (address_type_id) references Address_Types (address_type_id) on delete  restrict on update  restrict;
Alter table Order_Details add Foreign Key (order_id) references Orders (order_id) on delete  restrict on update  restrict;
Alter table Products add Foreign Key (partner_id) references Partners (partner_id) on delete  restrict on update  restrict;
Alter table Partner_Contacts add Foreign Key (partner_id) references Partners (partner_id) on delete  restrict on update  restrict;
Alter table Partner_Settlements add Foreign Key (order_id,product_id) references Order_Details (order_id,product_id) on delete  restrict on update  restrict;
Alter table Customer_Phone add Foreign Key (phone_type_id) references Phone_Type (phone_type_id) on delete  restrict on update  restrict;


