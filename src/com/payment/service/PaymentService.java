package com.payment.service;

import java.util.ArrayList;

import com.payment.service.representation.PaymentRepresentation;
import com.payment.service.representation.PaymentRequest;


public interface PaymentService {
	public ArrayList<PaymentRepresentation> getPayments(String customerId);
	public PaymentRepresentation getPayment(String pmtId);
	public PaymentRepresentation createCustomerPayment(PaymentRequest customerRequest)throws Exception;

	
	
}
