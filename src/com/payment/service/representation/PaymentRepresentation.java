package com.payment.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PaymentRepresentation {

	private String paymentId;
	private String customerId;
	private String acctNumber;
	private String pmtURI;

	public PaymentRepresentation() {}

	public PaymentRepresentation(String paymentId, String customerId, String acctNumber, String pmtURI){
		 this.paymentId=paymentId;
		 this.customerId=customerId;
		 this.acctNumber=acctNumber;
		 this.pmtURI=pmtURI;
	}

	public String getPaymentId() {
		return paymentId;
	}
	
	public String getCustomerId() {
		return customerId;
	}

	public String getAcctNumber() {
		return acctNumber;
	}

	public String getPmtURI() {
		return pmtURI;
	}
	
	
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public void setAcctNumber(String acctNumber) {
		this.acctNumber = acctNumber;
	}
	
	public void setPmtURI(String pmtURI) {
		this.pmtURI = pmtURI;
	}
		
}
