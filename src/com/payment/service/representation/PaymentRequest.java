package com.payment.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.payment.CardNetwork;

import java.sql.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class PaymentRequest {
	
	private String paymentId;
	private String customerId;
	private String cName;
	private String acctNumber;
	private int acctCode;
	private String pmtURI;
	private Date expDate;
	private CardNetwork cNetwork;


	//Methods
	public String getPaymentId() {
		return paymentId;
	}
	
	public String getcName() {
		return cName;
	}

	public int getAcctCode() {
		return acctCode;
	}

	public Date getExpDate() {
		return expDate;
	}

	public String getCustomerId() {
		return customerId;
	}
	
	public String getAcctNumber() {
		return acctNumber;
	}
	
	public String getPmtURI() {
		return pmtURI;
	}
		
	public CardNetwork getcNetwork() {
		return cNetwork;
	}

	
	
	
	public void setcName(String cName) {
		this.cName = cName;
	}
	
	public void setAcctCode(int acctCode) {
		this.acctCode = acctCode;
	}
	
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public void setAcctNumber(String acctNumber) {
		this.acctNumber = acctNumber;
	}
	
	public void setPmtURI(String pmtURI) {
		this.pmtURI = pmtURI;
	}
	
	public void setcNetwork(CardNetwork cNetwork) {
		this.cNetwork = cNetwork;
	}
	 
}
