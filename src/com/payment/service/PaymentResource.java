package com.payment.service;

import java.sql.Date;
import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.CacheControl;

import com.payment.CardNetwork;
import com.payment.service.representation.PaymentRepresentation;
import com.payment.service.representation.PaymentRequest;
import com.payment.service.workflow.PaymentActivity;


@Path ("/paymentservice/")
public class PaymentResource implements PaymentService{

	//**************************************************************************************************************
	//*************************************************************************************************************
	//  											PaymentSimple 
	//*************************************************************************************************************
	//*************************************************************************************************************	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("customers/payments/{cId}")
	//@Cacheable(cc="public, maxAge=3600") example for caching
	public ArrayList<PaymentRepresentation> getPayments(@PathParam("cId") String cId) {
		System.out.println("GET METHOD Request for all payments for customer ID ............."+cId);
		PaymentActivity pmtActivity = new PaymentActivity();
		return pmtActivity.getPayments(cId);
	}
	

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/payments/{pmtId}")
	public PaymentRepresentation getPayment(@PathParam("pmtId") String pmtId) {
		System.out.println("GET METHOD Request for paymentRequest ID ............." + pmtId);
		PaymentActivity pmtActivity = new PaymentActivity();
		return pmtActivity.getPayment(pmtId);
	}
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/payments")
	public PaymentRepresentation createCustomerPayment(PaymentRequest  pmtRequest) throws Exception {
		System.out.println("POST METHOD Request from Client with Card Information............." + pmtRequest.getAcctNumber() + "  " + pmtRequest.getcName());
		PaymentActivity pmtActivity = new PaymentActivity();
		String cardName= pmtRequest.getcName();
		String cardNumber= pmtRequest.getAcctNumber();
		Date cardExpDate= pmtRequest.getExpDate();
		int code= pmtRequest.getAcctCode();
		CardNetwork cn= pmtRequest.getcNetwork();

		return pmtActivity.createPayment(pmtRequest.getCustomerId(), cn, cardNumber, cardName, cardExpDate, code);
	}

	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("/payments")
	public PaymentRepresentation updatePayment(PaymentRequest pmtRequest) throws Exception{
		System.out.println("PUT METHOD Request from Client to Update Card ID............." + pmtRequest.getPaymentId());
		PaymentRepresentation pmtRep= new PaymentRepresentation();
		PaymentActivity pmtActivity = new PaymentActivity();
		pmtRep= pmtActivity.updatePayment(pmtRequest);
		return pmtRep;
	}
	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/payments/{pmtId}")
	public Response deleteCustomer(@PathParam("pmtId") String pmtId) throws Exception {
		System.out.println("Delete METHOD Request from Client with paymentRequest ID ............." + pmtId);
		PaymentActivity pmtActivity = new PaymentActivity();
		boolean result = pmtActivity.deletePayment(pmtId);
		if (result) {
			return Response.status(Status.OK).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}
	
	
	
}
