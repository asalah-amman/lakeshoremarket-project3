package com.payment.service.workflow;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.customer.Customer;
import com.customer.CustomerManager;
import com.customer.dal.CustomerDao;
import com.customer.service.representation.CustomerRepresentation;
import com.payment.CardNetwork;
import com.payment.Payment;
import com.payment.PaymentManager;
import com.payment.service.representation.PaymentRepresentation;
import com.payment.service.representation.PaymentRequest;


/**
 * 
 * @author Alla Salah
 * Purpose: To provide a set of methods that can be used within the PaymentResource class
 * Available Methods: 
 * getPayments(): ArrayList<PaymentRepresentation>
 * getCustomerPayment(String customerId):PaymentRepresentation
 * createCustomerPayment(String fName, String lName, String email):PaymentRepresentation
 */
public class PaymentActivity {

private static CustomerManager cMngr = new CustomerManager();
private static PaymentManager pmtMngr= new PaymentManager();
	
	
	 
	
	public PaymentRepresentation createPayment(String cid,CardNetwork cn, String cNumber,String acctName, Date cardExp, int security) throws Exception{
		Payment newPmt= new Payment(cn,cNumber,acctName,cardExp,security);
		boolean result= pmtMngr.createCustomerPayment(cid,newPmt);
		
		PaymentRepresentation pmtRep = new PaymentRepresentation();
		if(result){
			pmtRep.setAcctNumber(newPmt.getCardNumber());
			pmtRep.setCustomerId(String.valueOf(newPmt.getcId()));
			pmtRep.setPmtURI("http://localhost:8081/paymentservice/payments");
		}
		
		//System.out.println("Completed the createCustomer in CustomerActivity class");
		return pmtRep;
	}
	
	/**
	 * 
	 * @return An ArrayList of CustomerRepresentation objects 
	 */
	/*
	       This methods uses the PaymentManager.getCustomerPayments() which then converts the resulting set by
	       
	 */
	public ArrayList<PaymentRepresentation> getPayments(String cid) {
		
		ArrayList<Payment> payments = new ArrayList<Payment>();
		ArrayList<PaymentRepresentation> pmtRepresentations = new ArrayList<PaymentRepresentation>();

		payments =pmtMngr.getCustomerPayments(cid);
		Iterator<Payment> it = payments.iterator();
		while(it.hasNext()) {
		  Payment pmt = (Payment)it.next();
		  PaymentRepresentation pmtRepresentation = new PaymentRepresentation();
		  pmtRepresentation.setCustomerId(String.valueOf(pmt.getcId()));
		  pmtRepresentation.setPaymentId(String.valueOf(pmt.getPaymentTypeId()));
		  pmtRepresentation.setAcctNumber(pmt.getCardNumber());
		  pmtRepresentation.setPmtURI("http://localhost:8081/paymentservice/payments/"+pmt.getPaymentTypeId());

		  //now add this representation in the list
		  pmtRepresentations.add(pmtRepresentation);
        }
		return pmtRepresentations;
	}
	
	 
	
	public PaymentRepresentation getPayment(String pmtId) {
		
		Payment pmt = pmtMngr.getCustomerPayment(pmtId);
		//System.out.println("CustomerID= "+cus.getCustomerId());
		
		PaymentRepresentation pmtRep = new PaymentRepresentation();
		pmtRep.setCustomerId(String.valueOf(pmt.getcId()));
		pmtRep.setPaymentId(String.valueOf(pmt.getPaymentTypeId()));
		pmtRep.setAcctNumber(pmt.getCardNumber());
		pmtRep.setPmtURI("http://localhost:8081/paymentservice/payments/"+pmt.getPaymentTypeId());
	
		return pmtRep;
	}
	
	public PaymentRepresentation updatePayment(PaymentRequest pmtRequest) throws Exception{
		 PaymentRepresentation pmtRep= new PaymentRepresentation();
		 String paymentId =pmtRequest.getPaymentId();
		 String customerId = pmtRequest.getCustomerId();
		 String cName = pmtRequest.getcName();
		 String acctNumber =pmtRequest.getAcctNumber();
		 int acctCode= pmtRequest.getAcctCode();
		 Date expDate =pmtRequest.getExpDate();
		 CardNetwork cNetwork= pmtRequest.getcNetwork();
		 
		 Payment updatedPmt= new Payment(cNetwork,acctNumber,cName,expDate,acctCode);
		 updatedPmt.setPaymentTypeId(Long.valueOf(paymentId));
		 updatedPmt.setcId(Long.valueOf(customerId));

		 pmtMngr.updateCustomerPayment(updatedPmt);
		
		return pmtRep;
	}
	
	public boolean deletePayment(String pmtId) throws Exception {
		boolean result=false;
		result= pmtMngr.deleteCustomerPayment(pmtId);
		
		return result;
	}
	
	
	//-------------------------------------
	// HELPER METHOD to build a string
	//-------------------------------------
	public String buildString(ArrayList<?> ar){
		String newStr="";
		for(int i=0; i < ar.size(); i++){
			if(i==0)
				newStr += ar.get(i).toString();
			else 
				newStr += " ; "+ar.get(i).toString();
		}
		return newStr;
	}
	
	//---------------------------------------------
	// HELPER METHOD to build a string array list
	//---------------------------------------------
	public ArrayList<String> buildStringArrayList(ArrayList<?> ar){
		ArrayList<String> strArrList= new ArrayList<String>();
		
		for(int i=0; i < ar.size(); i++){
			strArrList.add(ar.get(i).toString());
		}
		return strArrList;
	}

}
