package com.payment;
/**
 * @author Alla Salah
 */
public enum CardNetwork {
 
	AMERICAN_EXPRESS,
	DISCOVER,
	MASTER_CARD,
	VISA,
	PAYPAL
 

}
