package com.payment.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;

import com.address.*;
import com.payment.*;
/**
 * 
 * @author Alla Salah
 * This Class is used to handle CRUD functionality for a Payment object
 * The addCustomer method will result in adding records to the following tables:
 * 1. Customer_Payments
 */
public class PaymentDao {
        
  private Connection connect = null;
  private Statement statement = null;
  private PreparedStatement preparedStatement = null;
  private ResultSet resultSet = null;
  private AddressValidator av;
  
  final private String host = "localhost:3306";
  final private String user = "root";
  final private String passwd = "";
  
  
  
  public PaymentDao(){
	  this.av= new AddressValidator();
	  av.populateCities();
	  //av.populateZips();
	  av.populateStates();
	  av.populateCountries();
  }
  //----------------------------------------------------------------------------------------
  /**
   * Method is responsible for initializing a connection to the database
   * 
   * @return Connection object
   */
  public Connection initializeDB()throws Exception{
	  Connection dbConnection=null;
	 try{
		 Class.forName("com.mysql.jdbc.Driver");
		 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
	 } 
	 catch (SQLException e) {
      throw e;
     } 
	 finally {
      close();
    }
      
      return dbConnection;
  }//end method
  //----------------------------------------------------------------------------------------
  //----------------------------------------------------------------------------------------
   
  
  
  //Private method used by the addPayment method
  private boolean insertCustomerPaymentTypes(Connection dbConnection, Payment p, long customerId)throws Exception{
	  boolean result=false;
	  long newPmtTypeId=-1;
	  ArrayList<Integer> passList= new ArrayList<Integer>();
	  
	  try{
		   String insertQuery="INSERT INTO customer_payment_types(customer_id, network_type_id,acct_name,account_number,acct_exp_date,security_code,is_default)"+
					"SELECT ?,?,?,?,?,?,? "+
					" WHERE NOT EXISTS (SELECT 1 FROM customer_payment_types WHERE customer_id=? AND account_number=?)";

			preparedStatement = dbConnection.prepareStatement(insertQuery,Statement.RETURN_GENERATED_KEYS		);
			preparedStatement.setLong(1, customerId); //p.getPaymentCards().get(i).getPaymentNetwork().ordinal()+1
			preparedStatement.setInt(2, 1); //p.getPaymentCards().get(i).getPaymentNetwork().ordinal()+1
			preparedStatement.setString(3, p.getCardName());
			preparedStatement.setString(4,p.getCardNumber());
			preparedStatement.setDate(5, p.getCardExpiration());
			preparedStatement.setInt(6, p.getSecurityCode());
			preparedStatement.setInt(7, (p.isDefault())? 1:0);
			preparedStatement.setLong(8, customerId);
			preparedStatement.setString(9, p.getCardNumber());
			//System.out.print("Payment Insert Query: "+ preparedStatement );
			result= preparedStatement.execute();
			
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next()) {
				newPmtTypeId=rs.getLong(1);
				passList.add(0);
				p.setPaymentTypeId(newPmtTypeId);
				//System.out.println("New Payment ID: "+newPmtTypeId);
				passList.add(0);
			
			}
			 
	  }
	  catch (Exception e) {
	      throw e;
	    } 
	  finally {
	      close();
	    }
	  
	  return result;
	  
  }
	
  	//Private method use by updateCustomerPayment method
  	private boolean updatePayment(Connection updateCon,String cid, String paymentId,CardNetwork cn, String cName, String cNumber, Date expDate, int code) 
  			throws Exception{

  		boolean result=false;
  		try{
 	      preparedStatement = updateCon.prepareStatement("UPDATE customer_payment_types "
 	    		  										+"  SET network_type_id=1"//c.getBillingAddress().getTypeOfAddress()
 	    		  										+", acct_name=?"
 	    		  										+", account_number=?"
 	    		  										+", acct_exp_date=?"
 	    		  										+", security_code=?"
 	    		  										+" WHERE customer_id=?"
 	    		  										+" AND payment_type_id=?"
 	    		  										);
 
 	      preparedStatement.setString(1, cName);
 	      preparedStatement.setString(2, cNumber);
 	      preparedStatement.setDate(3, expDate);
 	      preparedStatement.setInt(4, code);
 	      preparedStatement.setLong(5, Long.valueOf(cid));
 	      preparedStatement.setLong(6, Long.valueOf(paymentId));
 	      //System.out.println("SQL for Updating Address table for Billing: "+ preparedStatement);
 	      int count=preparedStatement.executeUpdate();
 	      if(count > 0)
 	    	  result=true;
 	      else 
 	    	  result=false;
 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return result;
  	
  	} 
  	
  	//Private method used by the getCustomerPayments method
  	private ArrayList <Payment>  getCustomerPaymentInfo(Connection dbConnection,long customerId) throws Exception{
		ArrayList <Payment> pmtList= new ArrayList<Payment>();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getCustomerPaymentInfo, id= "+customerId);
  	  		String queryStr="SELECT payment_type_id,customer_id,network_type_id, acct_name,account_number, acct_exp_date, security_code, is_default"+
  	  				" FROM customer_payment_types"+
  	  				" WHERE customer_id=?";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, customerId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 //System.out.println("PaymentDao: = getCustomerPaymentInfo() customerID "+customerId);

  	        	 resultSet= preparedStatement.getResultSet();
  	        	
  	        	 while(resultSet.next()){
  	        		Payment newPayment= new Payment();
   	  	        	long pmtId= resultSet.getLong(1);				//payment_type_id
   	  	        	long cusId= resultSet.getLong(2);				//customer_id
   	  	        	int nType= resultSet.getInt(3); 				//network_type_id
   	  	        	String cardName= resultSet.getString(4); 		//acct_name
   	  	        	String acctNum= resultSet.getString(5); 		//account_number
   	  	        	java.sql.Date expDate= resultSet.getDate(6); 	//acct_exp_date
   	  	        	int sCode= resultSet.getInt(7);					//security_code
   	  	        	int isDefault =resultSet.getInt(8); 			//is_default
	
   	  	        	newPayment.setPaymentTypeId(pmtId);
   	  	        	newPayment.setcId(cusId);
   	  	        	newPayment.setPaymentNetwork(CardNetwork.AMERICAN_EXPRESS);
   	  	        	newPayment.setCardName(cardName);
   	  	        	newPayment.setCardNumber(acctNum);
   	  	        	newPayment.setCardExpiration(expDate);
   	  	        	newPayment.setSecurityCode(sCode);
   	  	        	newPayment.setDefault((isDefault==1)? true:false);
   	  	        	
   	  	        	pmtList.add(newPayment);
  	        	 }//end while
  	        	  
  	        }//end if
  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
       
     	 //System.out.println("End of PaymentDao: getCustomerPaymentInfo() size of pmt list"+pmtList.size());

        return pmtList;
  	}
  	
  	private Payment getPaymentInfo(Connection dbConnection,long pmtId) throws Exception{

		Payment pmt= new Payment();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getPaymentCardInfo, id= "+customerId);
  	  		String queryStr="SELECT payment_type_id,customer_id,network_type_id, acct_name,account_number, acct_exp_date, security_code, is_default"+
  	  				" FROM customer_payment_types"+
  	  				" WHERE payment_type_id=?";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, pmtId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 //System.out.println("PaymentDao: getPaymentInfo() payment ID= "+pmtId);
  	        	 resultSet= preparedStatement.getResultSet();
  	        	
  	        	 while(resultSet.next()){
   	  	        	long paymentId= resultSet.getLong(1);		    //payment_type_id
   	  	        	long cusId= resultSet.getLong(2);				//customer_id
   	  	        	int nType= resultSet.getInt(3); 				//network_type_id
   	  	        	String cardName= resultSet.getString(4); 		//acct_name
   	  	        	String acctNum= resultSet.getString(5); 		//account_number
   	  	        	java.sql.Date expDate= resultSet.getDate(6); 	//acct_exp_date
   	  	        	int sCode= resultSet.getInt(7);					//security_code
   	  	        	int isDefault =resultSet.getInt(8); 			//is_default

   	  	        	pmt.setPaymentTypeId(paymentId);
   	  	        	pmt.setcId(cusId);
   	  	       		pmt.setPaymentNetwork(CardNetwork.AMERICAN_EXPRESS);
   	  	  			pmt.setCardName(cardName);
   	  				pmt.setCardNumber(acctNum);
   	 				pmt.setCardExpiration(expDate);
   					pmt.setSecurityCode(sCode);
   					pmt.setDefault((isDefault==1)? true:false);
   	  	        	
  	        	 }//end while
  	        	  
  	        }//end if
  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
       
        return pmt;
  	
  	}
  	
  	//Private method used by the deleteCustomerPayment
  	private boolean deactivateCustomer(Connection dbConnection,long pmtId) throws Exception{
		
		boolean cResult=false;
  		try{
 	      
  			//System.out.println("customerId= "+ c.getCustomerId());
  			preparedStatement = dbConnection.prepareStatement("UPDATE customers "
 	    		  										+" SET is_active= 0"
 	    		  										+" WHERE payment_type_id=?"+pmtId );
 	      
  			preparedStatement.setLong(1, pmtId);
  			preparedStatement.executeUpdate();
 	      //System.out.println("PaymentDao: Deactivated Customer Payment Info");

 	      cResult=true;
 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return  cResult;
		
	}
  	/*
 	  *********************************************************************************************
 	  *********************************************************************************************
 	  										PUBLIC METHODS 
 	  *********************************************************************************************
 	  *********************************************************************************************/
    /**
     * 
     * @param p
     * @param customerId
     * @return
     * @throws Exception
     */
	public boolean addPayment(Payment p, long customerId) throws Exception{
		Connection insertCon=null;
		long paymentId=-1;
		boolean pmtResult=false;
		try{
	  		insertCon= this.initializeDB();
	  	   
	  	  pmtResult=this.insertCustomerPaymentTypes(insertCon, p, customerId);  
	  	  System.out.println("PaymentDao: Completed insert into payment table: "+pmtResult);

		}
		catch(Exception e){
			throw e;
		}
		finally{
			close();
		}
		
		return(paymentId>0);
	}

    /**
     * 
     * @param id
     * @param paymentId
     * @param cn -> card number
     * @param cName -> card name
     * @param cNumber -> card number
     * @param cExp
     * @param code
     * @return
     * @throws Exception
     */
  	public boolean updateCustomerPayment(Payment pmt) throws Exception{
  		Connection updateCon=null;
  		boolean result=false;
  		try{
  			updateCon= this.initializeDB();
  			String cId= String.valueOf(pmt.getcId());
  			String paymentId= String.valueOf(pmt.getPaymentTypeId());
  			CardNetwork cn= pmt.getPaymentNetwork();
  			String cName= pmt.getCardName();
  			String cNumber= pmt.getCardNumber();
  			Date expDate= pmt.getCardExpiration();
  			int code= pmt.getSecurityCode();
  			System.out.println("cId: "+cId+", pmtId:"+paymentId+", cn:"+cn+", cName:"+cName+", cNumber:"+cNumber+", expDate, code:  "+code);

  			result= this.updatePayment(updateCon, cId, paymentId ,cn, cName ,pmt.getCardNumber(), expDate, code);

  		}
  		catch (Exception e){
  			throw e;
  		}
  		finally{
  			close();
  		}
  		return result;
  	}

  	/**
  	 * 
  	 * @param cid
  	 * @return ArrayList<Payment>
  	 */
  	public ArrayList<Payment> getCustomerPayments(long cid){
  		ArrayList <Payment> pmtList= null;
  		Connection getConnection=null;
  		try {
  			getConnection= this.initializeDB();
  			pmtList= this.getCustomerPaymentInfo(getConnection, cid);
  			getConnection= this.initializeDB();
  			//System.out.println("In the getCustomerPayments for cid="+cid);
  			this.getCustomerPaymentInfo(getConnection, cid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return pmtList;
  	}
  	
	public Payment getCustomerPayment(long pid){
		Payment pmt= new Payment();
  		Connection getConnection=null;
  		try {
  			getConnection= this.initializeDB();
  			pmt= this.getPaymentInfo(getConnection, pid);
  			getConnection= this.initializeDB();
  			//System.out.println("In the getCustomerPayment for payment ID="+pid);
  		
		} catch (Exception e) {
			e.printStackTrace();
		}
  		return pmt;
  	}

  	/**
  	 * 
  	 * @param customerId
  	 * @return
  	 */
	public boolean deleteCustomerPayment(long pid){
  		Connection deleteConnection=null;
  		boolean result=false;
  		
  		try {
  			deleteConnection= this.initializeDB();
  			 result= this.deactivateCustomer(deleteConnection, pid);
  			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return (result);
  	}

  // You need to close the resultSet
  private void close() {
    try {
      if (resultSet != null) {
        resultSet.close();
      }

      if (statement != null) {
        statement.close();
      }

      if (connect != null) {
        connect.close();
      }
    } catch (Exception e) {

    }
  }

}