package com.payment;

import java.util.ArrayList;
import com.payment.dal.PaymentDao;

public class PaymentManager {
	private PaymentDao pmtDao = new PaymentDao();
	
	
	//Methods
	public boolean createCustomerPayment(String cid,Payment newPmt) throws Exception{
		boolean result=false;
		
		result= pmtDao.addPayment(newPmt, Long.valueOf(cid));

		return result;
	};

	public ArrayList<Payment> getCustomerPayments(String cid){
		ArrayList<Payment> pmts= new ArrayList<Payment>();
		
		pmts= pmtDao.getCustomerPayments(Long.valueOf(cid));
		
		return pmts;
	};
	
	public Payment getCustomerPayment( String pid){
		Payment cusPmt;
		cusPmt= pmtDao.getCustomerPayment(Long.valueOf(pid));
		
		return cusPmt;
	};
	
	
	public boolean updateCustomerPayment(Payment updatedPmt)throws Exception{
		boolean result=false;
		result= pmtDao.updateCustomerPayment(updatedPmt);
		return result;
	};
	
	public boolean deleteCustomerPayment( String pid)throws Exception{
		boolean result=false;
		pmtDao.deleteCustomerPayment(Long.valueOf(pid));
		return result;
	};

}
