package com.payment;
/**
 * @author Alla Salah
 */
import java.sql.Date;

public class Payment {
	
	//Class Attributes
	private long paymentTypeId;
	private long cId;
	private CardNetwork paymentNetwork;
	private String cardName;
	private String cardNumber;
	private Date cardExpiration;
	private int securityCode;
	private boolean isDefault;

	//Class Constructor
	public Payment(){
		this.cardNumber="";
	}
	public Payment (CardNetwork cn, String cNumber,String acctName, Date cardExp, int security){
		this.paymentNetwork=cn;
		this.cardNumber= cNumber;
		this.cardName=acctName;
		this.cardExpiration=cardExp;
		this.securityCode=security;
	}
	
	//Class Methods
	public long getPaymentTypeId() {
		return paymentTypeId;
	}
	
	public long getcId() {
		return cId;
	}
	
	public CardNetwork getPaymentNetwork() {
		return paymentNetwork;
	}

	public String getCardName() {
		return cardName;
	}

	public String getCardNumber() {
		return cardNumber;
	}
	
	public Date getCardExpiration() {
		return cardExpiration;
	}

	public int getSecurityCode() {
		return securityCode;
	}
	
	public boolean isDefault() {
		return isDefault;
	}

	public void setPaymentTypeId(long paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}
	
	public void setcId(long cId) {
		this.cId = cId;
	}
	
	public void setPaymentNetwork(CardNetwork paymentNetwork) {
		this.paymentNetwork = paymentNetwork;
	}
	
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public void setCardExpiration(Date cardExpiration) {
		this.cardExpiration = cardExpiration;
	}
	
	public void setSecurityCode(int securityCode) {
		this.securityCode = securityCode;
	}
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	
	
	@Override
	public String toString(){
		String output="";
		String delimiter="------------------------------------------";
		String cardType= "Card Network: "+ paymentNetwork ;
		String expInfo= "Expiration: "+ cardExpiration;
		boolean isEmpty=false;
		if(String.valueOf(paymentNetwork).length()==0 && cardNumber.length()==0 && securityCode <= 0)
			isEmpty=true;
		
		if(!isEmpty){
			output= this.buildWSOutput();
		}
		else{
			output="None";
		}
		
		return output;
	}
	
	public String buildWSOutput(){
		boolean isEmpty=false;
		String output="";
		if(String.valueOf(paymentNetwork).length()==0 && cardNumber.length()==0 && securityCode <= 0)
			isEmpty=true;
		
		if(!isEmpty){
			output+= "Card Number: "+cardNumber +", Code:"+securityCode +", Exp: "+ cardExpiration ;
		}
		else{
			output="None";
		}
		return output;
	} 
}
