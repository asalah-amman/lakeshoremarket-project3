package com.test;

import java.sql.Date;

import com.address.Address;
import com.customer.Customer;
import com.order.Order;
import com.order.dal.OrderDao;
import com.order.OrderStatus;
import com.payment.CardNetwork;
import com.payment.Payment;

public class testOrderDao {

	public testOrderDao() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		OrderDao dao = new OrderDao();
	
		//New order
		Order ord= new Order(new java.sql.Date(System.currentTimeMillis()), 100.00f, 10.00f, 8.00f, 100.00f, 1);
		ord.setQuantity(5);
		ord.setCustomerId(1);
		try {
			dao.addOrder(ord);
			System.out.println("Inserted a new order.");
		} catch (Exception e) {
			System.out.println("Failed to insert new order: "+e.toString());
		}
		
		//Update Order Status
		try {
			dao.updateOrderStatus(3, OrderStatus.Completed_Preparing_Order);
			System.out.println("Updated an existing order.");
		} catch (Exception e) {
			System.out.println("Failed to update order: "+e.toString());
		}
		
		try {
			dao.updateOrderTable(10, 500, 1);
			System.out.println("Updated an existing order.");
		} catch (Exception e) {
			System.out.println("Failed to update order: "+e.toString());
		}
		
		//Get Order
		try {
			dao.getOrder(17);
			System.out.println("Completed getting an existing order.");
		} catch (Exception e) {
			System.out.println("Failed to get order: "+e.toString());
		}
		
		//Delete Order
		try {
			dao.deleteOrder(2);
			System.out.println("Completed deleting an existing order.");
		} catch (Exception e) {
			System.out.println("Failed to delete order: "+e.toString());
		}
		
	}
}
