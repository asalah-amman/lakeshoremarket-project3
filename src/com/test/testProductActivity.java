package com.test;

/**
 * @author Kevin Morrissey
 * This is a JUnit test class to test ProductResource and Activity
 **/

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.product.service.representation.ProductRepresentation;
import com.product.service.workflow.ProductActivity;

public class testProductActivity {

	public static void main(String[] args) {
		
	}
	
	@Test
	//Test Representation and Activity classes to create resource and validate attributes
	public void testCreateBook() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr = new ProductRepresentation();
	double price = 15.95;
	pr = pa.createBook(1, "Test Book 7", "This is a test book", true, price, 100, 1000, "978-3-16-148410-0", "John Smith");
	assertTrue("Not equals", price - pr.getProductPrice() == 0);
	}
	
	@Test
	//Test Representation and Activity classes to create resource and validate attributes
	public void testCreateCellPhone() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr = new ProductRepresentation();
	int quantity = 995;
	pr = pa.createCellPhone(3, "iPhone 8s", "iPone 6 test 2", true, 500.00, quantity, 1010, "Apple", "iPhone6", "Verizon");
	assertTrue("Not equals", quantity - pr.getAvailableQuantity() == 0);
	}
	
	@Test
	//Test Representation and Activity classes to create resource and validate attributes
	public void testBook() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr = new ProductRepresentation();
	String author = "Kelly Author";
	pr = pa.createBook(1, "Test Book Author 5", "This is a test book", true, 24.95, 100, 1000, "978-3-16-148410-0", author);
	assertTrue(author.equals(pr.getAuthor()));
	}
	
	@Test
	//Test Representation and Activity classes  to create resource and validate attributes
	public void testCellPhone() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr = new ProductRepresentation();
	String manufacturer = "Samsung";
	pr = pa.createCellPhone(3, "Test Phone 5", "iPone 6 test 3", true, 500.00, 1000, 1010, manufacturer, "iPhone6", "Verizon");
	assertTrue(manufacturer.equals(pr.getManufacturer()));
	}
	
	@Test
	//Test Representation and Activity classes to update resource and validate attributes
	public void testUpdateBook() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr = new ProductRepresentation();
	pr = pa.createBook(1, "Test Book 9", "This is a test book", true, 15.95, 100, 1000, "978-3-16-148410-0", "John Smith");
	long productId = pr.getProduct_id();
	int quantity = 80;
	pr = pa.updateBook(productId, 1, "Test Book 9", "This is a test book", true, 15.95, quantity, 1000, "978-3-16-148410-0", "John Smith");
	assertTrue("Not equals", quantity - pr.getAvailableQuantity() == 0);
	}
	
	@Test
	//Test Representation and Activity classes to update resource and validate attributes
	public void testUpdateCellPhone() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr = new ProductRepresentation();
	pr = pa.createCellPhone(3, "Test Phone 6", "iPone 6 test 3", true, 500.00, 1000, 1010, "Apple", "iPhone6", "Verizon");
	long productId = pr.getProduct_id();
	boolean status = false;
	pr = pa.updateCellPhone(productId, 3, "Test Phone 6", "iPone 6 test 3", status, 500.00, 1000, 1010, "Apple", "iPhone6", "Verizon");
	assertEquals(status, pr.getIsActive());
	}
	
	@Test
	//Test Representation and Activity classes to get a Product
	public void testGetProduct() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr1 = new ProductRepresentation();
	ProductRepresentation pr2 = new ProductRepresentation();
	pr1 = pa.createBook(1, "Test Book 11", "This is a test book", true, 15.95, 100, 1000, "978-3-16-148410-0", "John Smith");
	String productId = Long.toString(pr1.getProduct_id());
	pr2 = pa.getProduct(productId);
	assertTrue(pr1.getIsbn().equals(pr2.getIsbn()));
	}
	
	@Test
	//Test Representation and Activity classes to get a Product
	public void testGetProduct4() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr1 = new ProductRepresentation();
	ProductRepresentation pr2 = new ProductRepresentation();
	pr1 = pa.createBook(1, "Test Book 11", "This is a test book", true, 15.95, 100, 1000, "978-3-16-148410-0", "John Smith");
	String product_id = Long.toString(pr1.getProduct_id());
	pr2 = pa.getProduct(product_id);
	String testIsbn = "978-3-16-148410-0";
	assertTrue(testIsbn.equals(pr2.getIsbn()));
	}
	
	@Test
	//Test Representation and Activity classes to delete a Product
	public void testDeleteProduct() {
	ProductActivity pa = new ProductActivity();
	ProductRepresentation pr1 = new ProductRepresentation();
	pr1 = pa.createCellPhone(3, "Test Phone 7", "iPone 6 test 3", true, 500.00, 1000, 1010, "Apple", "iPhone6", "Verizon");
	String productId = Long.toString(pr1.getProduct_id());
	String deleteStatus = pa.deleteProduct(productId);
	assertEquals(deleteStatus, "OK");
	}
	
	@Test
	//Test Representation and Activity search Product
	public void testSearchProduct() {
	ProductActivity pa = new ProductActivity();
	//ProductRepresentation pr1 = new ProductRepresentation();
	ArrayList <ProductRepresentation> productRepresentations = pa.searchProduct("y2");
	assertTrue("Not equals", 1 - productRepresentations.size() == 0);
	}



}
