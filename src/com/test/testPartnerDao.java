package com.test;
import java.util.ArrayList;
import java.sql.Date;

import com.Phone;
import com.PhoneType;
import com.address.Address;
/**
 * @author Alla Salah
 */
import com.partner.*;
import com.product.CellPhone;
import com.product.ProductSold;
import com.product.ProductType;

public class testPartnerDao {

	public static void main(String[] args) {
		PartnerDao pdao = new PartnerDao();
		
		//Partner need ContactPerson, Address, Settlement
		Phone phoneNumber= new Phone(PhoneType.Cell_Phone_Number,"321-111-4433");
		ContactPerson contactOne= new ContactPerson("Sam", "Jones","sam.jones@yahoo.com",phoneNumber);
		Address partnerAddress= new Address("7872 Oak Street","Orland Park","60125","Illinois","U.S.A");

		CellPhone phone = new CellPhone(ProductType.CELL_PHONE, "Test Phone", "This is a test phone", true, 499.95, 200, 1001, "Apple", "iPhone7", "Verizon");
		phone.setProductId(1);
		
		ProductSold sold= new ProductSold(phone.getProductId(),123.45,1);
		ArrayList<ProductSold> ps= new ArrayList<ProductSold>();
		ArrayList<PartnerSettlements> settlementsList= new ArrayList<PartnerSettlements>();
		
		ps.add(sold);
		Date d= new Date(System.currentTimeMillis());
		PartnerSettlements settlementOne= new PartnerSettlements(d, 100.23,ps);
		
		settlementsList.add(settlementOne); 
		//ProductSold pSold= ProductSold(Long.valueOf(String.valueOf(phone.getProductId())),100.23,2);
		//String fName, String lName, ContactPerson pContact, Address pAddress, boolean active, boolean pending, Date approval
		
		
		Partner bestDealsLLC= new Partner("Best Phone Deals LLC","LLC",contactOne,partnerAddress,true,true, new Date(System.currentTimeMillis()));
		bestDealsLLC.setSettelments(settlementsList);
		try {
			pdao.addPartner(bestDealsLLC);
			boolean a= pdao.addSettlement(settlementOne);
			
			System.out.println("PartnerSettlement add result: "+a);
			System.out.println("Finished adding a partner to database");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
 

}
