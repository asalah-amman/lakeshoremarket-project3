package com.test;

import java.sql.Date;

import com.phone.Phone;
import com.phone.PhoneType;
import com.address.Address;
import com.address.AddressValidator;
import com.customer.Customer;
import com.customer.dal.CustomerDao;
import com.payment.CardNetwork;
import com.payment.Payment;

public class testCustomerDao {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CustomerDao dao = new CustomerDao();

		
		
		//New customer
		Address shippingAddress= new Address("7712 W. 95th Ave","Oak Park","60455","Illinois","U.S.A");
		Address billingAddress= new Address("7872 Oak Street","Orland Park","60125","Illinois","U.S.A");
		Payment pmt= new Payment(CardNetwork.AMERICAN_EXPRESS,"555-4334-2190-1234","John Doe",new Date(System.currentTimeMillis()+10000),129);
		Phone number = new Phone(PhoneType.Cell_Phone_Number,"312-123-4567");
		Customer alla= new Customer("alla", "salah", billingAddress, shippingAddress, pmt, number, "asalah3@luc.edu",true);
		try {
			boolean addResult =dao.addCustomer(alla);
			System.out.println("Inserted a new customer "+addResult);
			 //Thread.sleep(8000);
			
			System.out.println("Before Updating the customer info ");
			System.out.println(alla.toString());

			
			alla.setLastName("Jones");
			billingAddress.setStreetAddress("7741 Clark St.");
			billingAddress.setCity("Chicago");
			shippingAddress.setStreetAddress("9822 N. Germantown Prkwy.");
			shippingAddress.setCity("Cordova");
			shippingAddress.setState("tennessee");
			pmt.setCardName("Alla Jones");
			number.setPhoneNumber("444-555-7777");
			boolean updateResult= dao.updateCustomer(alla);
			System.out.println("After Updating the customer info  "+updateResult);
			
			System.out.println("Getting customer info from the Database ");
			System.out.println(dao.getCustomer(alla.getCustomerId()).toString());
			


		} catch (Exception e) {
			System.out.println("Failed CustomerDao Test "+e.toString());
		}

		
	}

}
