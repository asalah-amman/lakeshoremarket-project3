package com.test;

import java.sql.Date;

/**
 * 
 * @author Kevin Morrissey - based on testCustomerDao by Alla Salah
 */






import java.util.ArrayList;

import com.product.*;
import com.product.dal.ProductDao;
/**
 * 
 * @author Kevin Morrissey - based on testCustomerDao by Alla Salah
 * This Class is used to test methods to manipulate a database in relation to a Product object. 
 *
 */

public class testProductDao {
	

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		ProductDao dao = new ProductDao();

		/*
		//New Book Product - update Test Book to the next number to create a new product
		Book book = new Book(ProductType.BOOK, "Test Book 1", "This is a description of test book", true, 15.95, 100, 1003, "978-3-16-148410-0", "John Smith"); 
		
		try {
			long result =dao.addProduct(book);
			System.out.println("Inserted a new product " + result);
			//Thread.sleep(8000);
			System.out.println("Updating Product ");
			book.setProductPrice(21.50);
			book.setAvailableQuantity(99);
			book.setBookIsbn("78-3-16-148410-1");
			book.setBookAuthor("Dave Johnson");
			
			boolean updateResult= dao.updateProduct(book);
			System.out.println("Update book " + updateResult);
			
			//this only works if you create a new product. If you re-run an existing product, the last getProduct will fail because the
			// id = 0
			
			System.out.println("Getting product information for product");
			System.out.println(dao.getProduct(book.getProductId()));
			
			long product_id = book.getProductId();
			boolean status = dao.deleteProduct(product_id);
			System.out.println("product deleted " + status + "\n\r");
			System.out.println("end product test " + "\n\r");

		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
		
		
		//New Cell Phone - update Test Phone to the next number to create a new product
		CellPhone phone = new CellPhone(ProductType.CELL_PHONE, "Test Phone 1", "This is a description of test phone", true, 500, 100, 1010, "Apple", "iPhone 7", "Verizon"); 
		try {
			long result = dao.addProduct(phone);
			System.out.println("Inserted a new product "+result);
			
			//Thread.sleep(8000);
			System.out.println("Updating Product ");
			phone.setManufacturer("Samsung");
			phone.setModelNumber("A2002");
			phone.setCarrier("TMobile");
					
			boolean updateResult= dao.updateProduct(phone);
			System.out.println("Update cell phone " + updateResult);
				
			//this only works if you create a new product. If you re-run an existing product, the last getProduct will fail because the
			// id = 0
			System.out.println("Getting product information for product id ");
			System.out.println(dao.getProduct(phone.getProductId()));
			
			long product_id = phone.getProductId();
			boolean status = dao.deleteProduct(product_id);
			System.out.println("product deleted " + status + "\n\r");
			System.out.println("end product test " + "\n\r");

		} catch (Exception e) {
			System.out.println("Failed ProductDao Test " + e.toString());
		}
		
		
		//New Laptop - update Test Laptop to the next number to create a new product
		Laptop laptop = new Laptop(ProductType.LAPTOP, "Test Laptop 1", "This is a description of test laptop", true, 900, 100, 1020, "HP", "Envy", false); 
		try {
			long result =dao.addProduct(laptop);
			System.out.println("Inserted a new product " + result);
			//Thread.sleep(8000);
			System.out.println("Updating Product ");
			laptop.setProductPrice(950);
			laptop.setAvailableQuantity(99);
			laptop.setManufacturer("Lenovo");
			laptop.setModelNumber("L2990");
			laptop.setTwoInOne(true);
							
			boolean updateResult= dao.updateProduct(laptop);
			System.out.println("Update laptop " + updateResult);
			
			//this only works if you create a new product. If you re-run an existing product, the last getProduct will fail because the
			// id = 0
			System.out.println("Getting product information for product id 12 ");
			System.out.println(dao.getProduct(laptop.getProductId()));
			
			long product_id = laptop.getProductId();
			boolean status = dao.deleteProduct(product_id);
			System.out.println("product deleted " + status + "\n\r");
			System.out.println("end product test " + "\n\r");
			
		} catch (Exception e) {
					System.out.println("Failed ProductDao Test "+e.toString());
		}
		*/
		
		ArrayList <Product> productList= new ArrayList<Product>();
		productList = dao.getAllProducts();
		
		
	}

}
