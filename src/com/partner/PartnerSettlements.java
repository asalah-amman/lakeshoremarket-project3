package com.partner;

import java.sql.Date;
import java.util.ArrayList;
import com.product.Product;
import com.product.ProductSold;

public class PartnerSettlements {
	//Class Attributes

	private long orderId;
	private Date settlementDate;
	private double settlementAmount;
	private ArrayList<ProductSold> productSold;
	
	
	//Constructor
	public PartnerSettlements(){
		this.settlementDate= new Date(System.currentTimeMillis());
		this.settlementAmount=0;
		this.productSold= new ArrayList<ProductSold>();
	}
	
	public PartnerSettlements(Date sDate, double amount, ArrayList<ProductSold> ps){
		this.productSold= new ArrayList<ProductSold>();
		this.productSold=ps;
		this.settlementDate= sDate;
		this.settlementAmount=amount;
	}
	
	//Methods
	public long getOrderId() {
		return orderId;
	}

	public Date getSettlementDate() {
		return settlementDate;
	}

	public double getSettlementAmount() {
		return settlementAmount;
	}

	public ArrayList<ProductSold> getProductSold() {
		return productSold;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	public void setSettlementAmount(double settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	public void setProductSold(ArrayList<ProductSold> productSold) {
		this.productSold = productSold;
	}

	
	@Override
	public String toString(){
		String output="";
		String delimiter="*********************************************";
		output+= "Partner Settlement Information \n\r";
		output+= delimiter+"\n\r";
		output+= "Partner Settlement Date: "+settlementDate+ "\n\r";
		output+= "Amount $ "+settlementAmount+"\n\r";
		output+= "Products Sold: \n\r";

		for(int i=0; i< productSold.size(); i++){
			output+= productSold.get(i).toString()+"\n\r";

		}
		output+= delimiter+"\n\r";
		return output;
	}
 
}
