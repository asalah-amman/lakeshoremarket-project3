package com.partner;
/**
 * @author Alla Salah
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.address.AddressValidator;
import com.customer.Customer;

public class PartnerDao {
	  private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;
	  private AddressValidator av;
	  
	  final private String host = "localhost:3306";
	  final private String user = "root";
	  final private String passwd = "";
	  
	  public PartnerDao(){
		  this.av= new AddressValidator();
		  av.populateCities();
		  //av.populateZips();
		  av.populateStates();
		  av.populateCountries();
	  }
	  
	  
	  
	  /**
	   * Method is responsible for initializing a connection to the database
	   * 
	   * @return Connection object
	   */
	  public Connection initializeDB()throws Exception{
		  Connection dbConnection=null;
		 try{
			 Class.forName("com.mysql.jdbc.Driver");
			 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
		 } 
		 catch (SQLException e) {
	      throw e;
	     } 
		 finally {
	      close();
	    }
	      
	      return dbConnection;
	  }//end method
	  
	  /*
	   * * *******************************************************************************************
	   										CREATE (INSERT) 
	  *********************************************************************************************/
	  private long insertPartner(Connection dbConnection, Partner c)throws Exception{
		  long newPartnerId=-1;
		  try{
		      String insertSql="INSERT INTO Partners(first_name, last_name,street_address, city_id, zip,state_id,country_id,is_pending_approval)"+
		    		  			"SELECT ?,?,?,?,?, ?,?,? "+
								"WHERE NOT EXISTS (SELECT 1 FROM Partners WHERE first_name=? AND street_address=?)";
		      
			  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
		      
		      preparedStatement.setString(1, c.getPartnerFirstName());
		      preparedStatement.setString(2, c.getPartnerLastName());
		      preparedStatement.setString(3, c.getPartnerAddress().getStreetAddress());
		      preparedStatement.setLong(4, av.getCityId(c.getPartnerAddress().getCity()));
		      preparedStatement.setString(5, c.getPartnerAddress().getZip());
		      preparedStatement.setLong(6, av.getStateId(c.getPartnerAddress().getState()));
		      preparedStatement.setLong(7, av.getCountryId(c.getPartnerAddress().getCountry()));
		      preparedStatement.setInt(8, (c.isPendingApproval())? 1:0);
		      preparedStatement.setString(9, c.getPartnerFirstName());
		      preparedStatement.setString(10, c.getPartnerAddress().getStreetAddress());
		      //System.out.println("Partner table Insert query: "+ preparedStatement);
		      boolean r= preparedStatement.execute();
		   
	    	  ResultSet rs = preparedStatement.getGeneratedKeys();
	    	  if (rs.next()) {
	    		  newPartnerId=rs.getLong(1);
	    	  }
	    	  //System.out.println("New Customer ID: "+newCustomerId);
		      
		      

		      if(newPartnerId > 0 ){
		    	  System.out.println("Updating Partner ID "+newPartnerId );
		    	  c.setPartnerId(newPartnerId);
		      }
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return newPartnerId;
		  
	  }
	  
	  
	  private boolean insertPartnerSettlement(Connection dbConnection, PartnerSettlements ps)throws Exception{
		  long newSettlementId=-1;
		  ArrayList<Integer> passList= new ArrayList<Integer>();
		  
		  try{
		      
			  for(int i=0; i < ps.getProductSold().size(); i++){

					
				  String insertSql="INSERT INTO Partner_Settlements(order_id, product_id,settlement_date, settlement_amount)"+
	    		  			"SELECT ?, ?, ? , ? "+
							"WHERE NOT EXISTS (SELECT 1 FROM Partner_Settlements WHERE order_id=? AND product_id=? )";
	      
				  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
				  preparedStatement.setLong(1,ps.getOrderId());
			      preparedStatement.setLong(2, ps.getProductSold().get(i).getProductId());
			      preparedStatement.setDate(3, ps.getSettlementDate());
			      preparedStatement.setDouble(4, ps.getSettlementAmount());
				  preparedStatement.setLong(5,ps.getOrderId());
			      preparedStatement.setLong(6, ps.getProductSold().get(i).getProductId());

			      //System.out.println("Partner table Insert query: "+ preparedStatement);
			      boolean r= preparedStatement.execute();
			   
			  	  ResultSet rs = preparedStatement.getGeneratedKeys();
			  	  if (rs.next()) {
			  		  newSettlementId=rs.getLong(1);
			  	  }
			  	  //System.out.println("New Customer ID: "+newCustomerId);
			      if(newSettlementId > 0 ){
			    	  //System.out.println("Updating customer ID " );
			    	  passList.add(1);
			      }

			  }
			 
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  if(passList.size()==ps.getProductSold().size())
			  return true;
		  else
			  return false;
		  
	  }
	  
	  
	  private boolean insertPartnerContact(Connection dbConnection, Partner c)throws Exception{
		  long newPartnerContactId=-1;
		  try{
			  	for(int i=0; i < c.getContacts().size(); i++){
			  		
				      String insertSql="INSERT INTO partner_contacts(partner_id, first_name,last_name, email, phone_number, is_active)"+
		    		  			"SELECT ?, ?, ? , ?, ?,? "+
								"WHERE NOT EXISTS (SELECT 1 FROM partner_contacts WHERE partner_id=? AND first_name=? AND last_name=? )";
		      
					  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
				      preparedStatement.setLong(1, c.getPartnerId());
				      preparedStatement.setString(2, c.getContacts().get(i).getFirstName());
				      preparedStatement.setString(3, c.getContacts().get(i).getLastName());
				      preparedStatement.setString(4, c.getContacts().get(i).getEmail());
				      preparedStatement.setString(5, c.getContacts().get(i).getPhoneNumber().getPhoneNumber());
				      preparedStatement.setInt(6, 1);
				      preparedStatement.setLong(7, c.getPartnerId());
				      preparedStatement.setString(8, c.getContacts().get(i).getFirstName());
				      preparedStatement.setString(9, c.getContacts().get(i).getLastName());
				      
				      boolean r= preparedStatement.execute();
				   
			    	  ResultSet rs = preparedStatement.getGeneratedKeys();
			    	  if (rs.next()) {
			    		  newPartnerContactId=rs.getLong(1);
			    	  }
			    	  //System.out.println("New Customer ID: "+newCustomerId);
				      
				      
		
				      if(newPartnerContactId > 0 ){
				    	  System.out.println("Updating contact person ID " +newPartnerContactId);
				    	  c.getContacts().get(i).setContactPersonId(newPartnerContactId);
				      }
			  	}
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return (newPartnerContactId>0);
		  
	  }
	  
	  public boolean addPartner(Partner p) throws Exception{
		  Connection insertCon=null;
	  		long newPartnerId=-1;
	  		try{
	  	  		insertCon= this.initializeDB();
	  	  		newPartnerId = this.insertPartner(insertCon, p);
	  	  		System.out.println("Completed insert into partner table. The Partner is: " + newPartnerId);

	  	  	    boolean pc= this.insertPartnerContact(insertCon, p);
	  	  		System.out.println("Completed insert into partner contact table. Result is " + pc);

	  		}
	  		catch(Exception e){
	  			throw e;
	  		}
	  		finally{
	  			close();
	  		}
	  		
	  		return (newPartnerId > 0);
	  }
	  
	  
	  public boolean addSettlement(PartnerSettlements ps) throws Exception{
		  Connection insertCon=null;
		  boolean result;
		  try{
			  	insertCon= this.initializeDB();
	  	  	    result= this.insertPartnerSettlement(insertCon, ps);
	  	  		System.out.println("Completed insert into partner settlement table. Result is: " + ps);

		  }
		  catch(Exception e){
			  throw e;
		  }
		  finally{
			  close();
		  }
		  return result;
	  }
	  
	  
	  
	  /*
	   * * *******************************************************************************************
	   										Update
	  *********************************************************************************************/
	  
	  
	  
	  
	  /*
	   * * *******************************************************************************************
	   										Read (GET) 
	  *********************************************************************************************/
	  
	  
	  
	  /*
	   * * *******************************************************************************************
	   										Delete   
	  *********************************************************************************************/
	  
	  // You need to close the resultSet
	  private void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
}
