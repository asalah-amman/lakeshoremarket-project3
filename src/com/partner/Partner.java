package com.partner;
/**
 * @author Alla Salah
 */
import java.util.ArrayList;
import java.util.Date;

import com.address.*;
import com.product.Product;

public class Partner {

	//Class Attributes
	private long partnerId;
	private String partnerFirstName;
	private String partnerLastName;
	private ArrayList <ContactPerson> contacts;
	private Address partnerAddress;
	private boolean isActive;
	private boolean isPendingApproval;
	private Date accApprovalDate;
	private ArrayList<PartnerSettlements> settelments;
	

	//Constructor
	public  Partner(String fName, String lName, ContactPerson pContact, Address pAddress, boolean active, boolean pending, Date approval){
		this.partnerFirstName=fName;
		this.contacts= new ArrayList <ContactPerson>();
		this.contacts.add(pContact);
		this.partnerAddress=pAddress;
		this.isActive=active;
		this.isPendingApproval=pending;
		this.accApprovalDate=approval;
	}
	
	//Methods

	public void setPartnerId(long partnerId) {
		this.partnerId = partnerId;
	}
	
	public void setPrimaryContact(ArrayList <ContactPerson>  c) {
		this.contacts=c;
	}

	public void setPartnerAddress(Address partnerAddress) {
		this.partnerAddress = partnerAddress;
	}
	
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public void setPendingApproval(boolean isPendingApproval) {
		this.isPendingApproval = isPendingApproval;
	}
	
	public void setAccApprovalDate(Date accApprovalDate) {
		this.accApprovalDate = accApprovalDate;
	}

	public void setSettelments(ArrayList<PartnerSettlements> settelments) {
		this.settelments = settelments;
	}

	public void setPartnerFirstName(String partnerFirstName) {
		this.partnerFirstName = partnerFirstName;
	}

	public void setPartnerLastName(String partnerLastName) {
		this.partnerLastName = partnerLastName;
	}
	

	

	public long getPartnerId() {
		return partnerId;
	}


	public String getPartnerFirstName() {
		return partnerFirstName;
	}

	public ArrayList <ContactPerson>  getContacts() {
		return contacts;
	}
	
	public Address getPartnerAddress() {
		return partnerAddress;
	}
	public boolean isActive() {
		return isActive;
	}
	public boolean isPendingApproval() {
		return isPendingApproval;
	}
	public Date getAccApprovalDate() {
		return accApprovalDate;
	}
	public ArrayList<PartnerSettlements> getSettelments() {
		return settelments;
	}
	public String getPartnerLastName() {
		return partnerLastName;
	}
	
	//Please see the customer class for complete implementation
	//As long as this method returns a boolean My Class LakeshoreSite will work. Same goes for the equals method.
	@Override 
	public int hashCode(){
		int num;
		num= Integer.valueOf(this.getPartnerFirstName().toUpperCase()) + Integer.valueOf(this.getPartnerLastName().toUpperCase());
		return num;
	}
	
	@Override 
	public boolean equals(Object obj){
		boolean result=false;
		Partner c= (Partner)obj;
		
		if(c.getPartnerFirstName().equals(this.getPartnerFirstName())){
			if(c.getPartnerLastName().equals(this.getPartnerLastName()))
					result=true;
		}		
		return result;
	}
	
	@Override
	public String toString(){
		String output="";
		String delimiter="-------------------------------";
		String name= partnerLastName+", "+partnerFirstName;
		
		output+="Product ID:" +partnerId+"\n\r";
		output+="Partner Name: "+name+"\n\r";
		output+="Partner Address: \n\r"+this.partnerAddress.toString();
		output+="Partner Contacts ("+getContacts().size()+"): \n\r";
		
		for(int i=0; i < getContacts().size(); i++){
			output+= getContacts().get(i).toString()+"\n\r";
		}
		output+="Partner Settlements ("+getSettelments().size()+"): \n\r";		
		for(int i=0; i < this.getSettelments().size(); i++){
			output+= getSettelments().get(i).toString()+"\n\r";
		}

		output+=delimiter;
		
		return output;
	}
	
}
