package com.partner;
/**
 * @author Alla Salah
 */
import com.Phone;

public class ContactPerson {

	//Class Attributes
	private long contactPersonId;
	private String firstName;
	private String lastName;
	private String email;
	private Phone phoneNumber;
	
	//Constructor
	public ContactPerson(){
		this.firstName="";
		this.lastName="";
		this.email="";
	}
	
	public ContactPerson(String fName, String lName, String email, Phone pNumber){
		this.firstName=fName;
		this.lastName=lName;
		this.email=email;
		this.phoneNumber=pNumber;
	}

	
	
	//Methods
	public long getContactPersonId() {
		return contactPersonId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public Phone getPhoneNumber() {
		return phoneNumber;
	}


	public void setContactPersonId(long contactPersonId) {
		this.contactPersonId = contactPersonId;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setPhoneNumber(Phone phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public String toString(){
		String output="";
		String delimiter="*********************************************";
		String cName= "Contact Person Name: "+ lastName + ", " + firstName ;
		output+= delimiter+"\n\r";
		output+= "Contact Person ID: "+contactPersonId +"\n\r";
		output+= cName+"\n\r";
		output+= "Email: "+email+"\n\r";
		output+= delimiter+"\n\r";
		return output;
	}


	
}
