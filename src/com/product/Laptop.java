package com.product;

/**
 * @author Kevin Morrissey
 * This Class extends Electronics and defines attributes and states of Laptop Objects.  
 */

public class Laptop extends Electronics {
	
	private boolean twoInOne;
	
	// Constructor
	public Laptop(ProductType productType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, boolean twoInOne) {
		super (productType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber);
		this.twoInOne = twoInOne;
	}
	
	public Laptop() {
		// TODO Auto-generated constructor stub
	}

	// setters and getters
	public void setTwoInOne(boolean twoInOne) {
		this.twoInOne = twoInOne;
	}
	
	public boolean getTwoInOne() {
		return this.twoInOne;
	}
	
	public String toString(){
		String delimiter="------------------------------------------";
		String output="";
		output+= super.toString();
		output+= twoInOne +"\n\r";
		output+= delimiter+"\n\r";
		
		return output;
	}

}

