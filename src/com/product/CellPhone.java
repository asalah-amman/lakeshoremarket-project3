package com.product;

/**
 * @author Kevin Morrissey
 * This Class extends Electronics and defines attributes and states of CellPhone Objects.  
 */

public class CellPhone extends Electronics {
	
	String carrier;

	
	// added override constructor for flexibility. Allows adding all CellPhone specific attributes at once
	public CellPhone(ProductType productType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, String carrier) {
		super (productType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber);
		this.carrier = carrier;
	}
	
	public CellPhone() {
		// TODO Auto-generated constructor stub
	}

	// setters and getters
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	
	public String getCarrier() {
		return this.carrier;
	}
	
	public String toString(){
		String delimiter="------------------------------------------";
		String output="";
		output+= super.toString();
		output+= carrier +"\n\r";
		output+= delimiter+"\n\r";
		
		return output;
	}

}

