package com.product;

public class ProductSold {
	
	//Class Attributes
	private long productId;
	private double price;
	private int quantitySold;
	
	//Constructor
	public ProductSold(long pId, double price, int quantitySold){
		this.productId= pId;
		this.price=price;
		this.quantitySold=quantitySold;
	}

	//Methods
	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantitySold() {
		return quantitySold;
	}

	public void setQuantitySold(int quantitySold) {
		this.quantitySold = quantitySold;
	}
	
	@Override
	public String toString(){
		String output="";
		String delimiter="-------------------------------";
		
		output+="Product ID:" +productId+"\n\r";
		output+="Price: "+price+"\n\r";
		output+="Quantity Sold: "+quantitySold+"\n\r";
		output+=delimiter;
		
		return output;
	}

}
