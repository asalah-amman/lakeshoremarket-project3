package com.product;

import java.io.Serializable;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Kevin Morrissey
 * This Class is an abstract class to define basic attributes and states of Product Objects.  
 */

@XmlRootElement
public abstract class Product implements Serializable {
	private static final long serialVersionUID = 1L;
	private String gid;
	protected ProductType productType;
	protected String productName;
	protected String productDescription;
	protected double productPrice;
	protected int availableQuantity;
	protected long product_id = 0;
	protected boolean isActive = false;
	protected long productPartner;
	private Set<String> privileges;
	
	//constructors
	public Product() {
	}
	
	public Product(ProductType productType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, long productPartner) {
		this.productType = productType;
		this.productName = productName;
		this.productDescription = productDescription;
		this.isActive = isActive;
		this.productPrice = productPrice;
		this.availableQuantity = availableQuantity;
		this.productPartner = productPartner;
		
	}

	// setter methods
	public void setProductType (ProductType productType) {
		this.productType = productType;
	}
	
	public void setProductName (String productName) {
		this.productName = productName;
	}
	
	public void setProductDescription (String productDescription) {
		this.productDescription = productDescription;
	}
	
	
	public void setProductStatus(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	public void setProductPrice (double productPrice) {
		this.productPrice = productPrice;
	}
	
	public void setAvailableQuantity (int availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
	
	public void setProdPartnerId(long productPartner) {
		this.productPartner = productPartner;
	}
	
	public void setProductId(long product_id) {
		this.product_id = product_id;
	}
	
	// getter methods
	public ProductType getProductType() {
		return productType;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public String getProductDescription() {
		return productDescription;
	}
	
	public boolean getProductStatus() {
		return this.isActive;
	}
	
	public double getProductPrice () {
		return this.productPrice;
	}
	
	public int getAvailableQuantity () {
		return this.availableQuantity;
	}
	
	public long getProdPartnerId() {
		return this.productPartner;
	}
	
	public long getProductId() {
		return this.product_id;
	}
	
	
	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}
	
	
	public boolean isUserInRole(String role) {
		if(privileges == null) { return false; }
		else { return privileges.contains(role); }
	}
	
 	
	
	@Override
	public String toString(){
		String output="";
		String delimiter="------------------------------------------";
		output+= delimiter+"\n\r";
		output+= productType+ "\n\r";
		output+= productType.getProdTypeValue()+ "\n\r";
		output+= productName+ "\n\r";
		output+= productDescription + "\n\r";
		output+= isActive + "\n\r";
		output+= productPrice + "\n\r";
		output+= availableQuantity + "\n\r";
		
		return output;
	}

	
}

