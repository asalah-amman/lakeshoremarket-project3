package com.product;

/**
 * @author Kevin Morrissey
 * This Class extends Product and defines attributes and states of Book Objects.  
 */

public class Book extends Product {
	
	private String isbn;
	private String author;
	
	// added override constructor for flexibility. Allows adding all Book specific attributes at once
	public Book(ProductType productType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String isbn, String author) {
		super(productType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner);
		this.isbn = isbn; 
		this.author = author;
	}
	
	public Book() {
		// TODO Auto-generated constructor stub
	}

	// setters and getters
	public void setBookIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public void setBookAuthor(String author) {
		this.author = author;
	}
	
	public String getBookIsbn() {
		return isbn;
	}
	
	public String getBookAuthor() {
		return author;
	}
	
	public String toString(){
		String delimiter="------------------------------------------";
		String output="";
		output+= super.toString();
		output+= isbn;
		output+= ", " + author +"\n\r";
		output+= delimiter+"\n\r";
		
		return output;
	}

}