package com.product;

/**
 * @author Kevin Morrissey
 * This is a manager class to communicate between the Activity and the DAO
 */

import java.util.ArrayList;

import com.product.dal.*;

public class ProductManager {
	
	private long productId;
	

	private static ProductDao pd = new ProductDao();
	
	// Add Book Product
	public Product addBook(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String isbn, String author) {
		
		Book book = new Book();
		book.setProductType(ProductType.BOOK);
		book.setBookIsbn(isbn);
		book.setBookAuthor(author);
		book.setProdPartnerId(productPartner);
		book.setProductName(productName);
		book.setProductDescription(productDescription);
		book.setProductPrice(productPrice);
		book.setAvailableQuantity(availableQuantity);
		book.setProductStatus(isActive);
		
		try {
			productId = pd.addProduct(book);
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
		
		if (productId > 0) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else
			return null;
	}
	
	// Add Electronics Product
	public Product addElectronics(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber) {
				
		Electronics elect = new Electronics();
		elect.setProductType(ProductType.ELECTRONICS);
		elect.setManufacturer(manufacturer);
		elect.setModelNumber(modelNumber);
		elect.setProdPartnerId(productPartner);
		elect.setProductName(productName);
		elect.setProductDescription(productDescription);
		elect.setProductPrice(productPrice);
		elect.setAvailableQuantity(availableQuantity);
		elect.setProductStatus(isActive);
				
		try {
			productId = pd.addProduct(elect);
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
				
		if (productId > 0) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else
			return null;
	}
	
	// Add CellPhone Product
	public Product addCellPhone(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, String carrier) {
			
		CellPhone phone = new CellPhone();
		phone.setProductType(ProductType.CELL_PHONE);
		phone.setManufacturer(manufacturer);
		phone.setModelNumber(modelNumber);
		phone.setCarrier(carrier);
		phone.setProdPartnerId(productPartner);
		phone.setProductName(productName);
		phone.setProductDescription(productDescription);
		phone.setProductPrice(productPrice);
		phone.setAvailableQuantity(availableQuantity);
		phone.setProductStatus(isActive);
			
		try {
			productId = pd.addProduct(phone);
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
			
		if (productId > 0) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else
			return null;
	}
	
	// Add Laptop Product
	public Product addLaptop(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, boolean twoInOne) {
				
		Laptop laptop = new Laptop();
		laptop.setProductType(ProductType.LAPTOP);
		laptop.setManufacturer(manufacturer);
		laptop.setModelNumber(modelNumber);
		laptop.setTwoInOne(twoInOne);
		laptop.setProdPartnerId(productPartner);
		laptop.setProductName(productName);
		laptop.setProductDescription(productDescription);
		laptop.setProductPrice(productPrice);
		laptop.setAvailableQuantity(availableQuantity);
		laptop.setProductStatus(isActive);
				
		try {
			productId = pd.addProduct(laptop);
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
				
		if (productId > 0) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else 
			return null;
	}
	
	// Update Book Product
	public Product updateBook(long productId, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String isbn, String author) {
		
		boolean update = false;
		
		Book book = (Book) pd.getProduct(productId);
		book.setProductType(ProductType.BOOK);
		book.setBookIsbn(isbn);
		book.setBookAuthor(author);
		book.setProdPartnerId(productPartner);
		book.setProductName(productName);
		book.setProductDescription(productDescription);
		book.setProductPrice(productPrice);
		book.setAvailableQuantity(availableQuantity);
		book.setProductStatus(isActive);
	
		
		
		try {
			 update = pd.updateProduct(book);
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
		
		if (update == true) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else
			return null;
	}
	
	// Update Electronics Product
	public Product updateElectronics(long productId, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber) {
					
		boolean update = false;
			
		
		Electronics elect = (Electronics) pd.getProduct(productId);
		elect.setProductType(ProductType.ELECTRONICS);
		elect.setManufacturer(manufacturer);
		elect.setModelNumber(modelNumber);
		elect.setProdPartnerId(productPartner);
		elect.setProductName(productName);
		elect.setProductDescription(productDescription);
		elect.setProductPrice(productPrice);
		elect.setAvailableQuantity(availableQuantity);
		elect.setProductStatus(isActive);
	
					
		try {
			update = pd.updateProduct(elect);
		} catch (Exception e) {
				System.out.println("Failed ProductDao Test "+e.toString());
		}
					
		if (update == true) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else
			return null;
	}
	
	// Update CellPhone Product
	public Product updateCellPhone(long productId, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, String carrier) {
				
		boolean update = false;
		
		CellPhone phone = (CellPhone) pd.getProduct(productId);
		phone.setProductType(ProductType.CELL_PHONE);
		phone.setManufacturer(manufacturer);
		phone.setModelNumber(modelNumber);
		phone.setCarrier(carrier);
		phone.setProdPartnerId(productPartner);
		phone.setProductName(productName);
		phone.setProductDescription(productDescription);
		phone.setProductPrice(productPrice);
		phone.setAvailableQuantity(availableQuantity);
		phone.setProductStatus(isActive);
	
				
		try {
			update = pd.updateProduct(phone);
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
				
		if (update == true) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else
			return null;
	}
	
	// Update Laptop Product
	public Product updateLaptop(long productId, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, boolean twoInOne) {
					
		boolean update = false;
			
		Laptop laptop = (Laptop) pd.getProduct(productId);
		laptop.setProductType(ProductType.LAPTOP);
		laptop.setManufacturer(manufacturer);
		laptop.setModelNumber(modelNumber);
		laptop.setTwoInOne(twoInOne);
		laptop.setProdPartnerId(productPartner);
		laptop.setProductName(productName);
		laptop.setProductDescription(productDescription);
		laptop.setProductPrice(productPrice);
		laptop.setAvailableQuantity(availableQuantity);
		
					
		try {
			update = pd.updateProduct(laptop);
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
					
		if (update == true) {
			Product prod = pd.getProduct(productId);
			return prod;
		}	
		else
			return null;
	}

	// Get a specific product
	public Product getProduct(long productId) {
		
		try {
			 Product prod = pd.getProduct(productId);
			 return prod;
		} catch (Exception e) {
			System.out.println("Failed ProductDao Test "+e.toString());
		}
		return null;
	}
	
	// Get All Products
	public ArrayList <Product> getAllProducts() {
		ArrayList <Product> productList = pd.getAllProducts();
		return productList;
	}
	
	//Search Product based on product name
	public ArrayList <Product> searchProduct(String productName) {
		ArrayList <Product> productList = pd.searchForProduct(productName);
		return productList;
	}
		
	// Delete a specific product
	public boolean deleteProduct(long productId) {
		//System.out.println("In deletePoduct method of ProductManager. Id is " + productId);
		boolean deleted = false;
		deleted = pd.deleteProduct(productId);
		return deleted;
	}
}
