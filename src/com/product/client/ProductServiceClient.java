package com.product.client;

/**
 * @author Kevin Morrissey
 * ProductServiceCleint class tests the CRUD actions of the service
 */

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Response;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

import com.product.service.representation.ProductRequest;

public final class ProductServiceClient {
	
	public static void main(String[] args) {
		
		List<Object> providers = new ArrayList<Object>();
        JacksonJsonProvider provider = new JacksonJsonProvider();
        provider.addUntouchable(Response.class);
        providers.add(provider);
        

        /*****************************************************************************************
         * GET METHOD invoke
         *****************************************************************************************/
        
        System.out.println("GET METHOD .........................................................");
        
   
        WebClient getClient = WebClient.create("http://localhost:8081", providers);
        
        //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
      //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to get in xml format
        getClient = getClient.accept("application/json").type("application/json").path("/productservice/products/41"); // 5 is an active product in my local db
        
        //The following lines are to show how to log messages without the CXF interceptors
        String getRequestURI = getClient.getCurrentURI().toString();
        System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
        String getRequestHeaders = getClient.getHeaders().toString();
        System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);
        
        //to see as raw XML/json
        System.out.println("First line of to see as raw XML/json");
        String response = getClient.get(String.class);
        System.out.println("GET METHOD Response: ...." + response);
        
        //to get the response as object of Product class
        //Product product = client.get(Product.class);
        //System.out.println("Prod Name:" + product.getProductName());
        
        
        /*****************************************************************************************
         * POST METHOD invoke - Book Product
        *****************************************************************************************/
        System.out.println("POST METHOD .........................................................");
        WebClient postClient = WebClient.create("http://localhost:8081", providers);
        WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
        WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        postClient = postClient.accept("application/json").type("application/xml").path("/productservice/products");
     	
        String postRequestURI = postClient.getCurrentURI().toString();
        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
        String postRequestHeaders = postClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
        ProductRequest productRequest = new ProductRequest();
        productRequest.setProdType(1);
        productRequest.setProductName("This is a test book - 10102"); // must update this to a new name each time a product is created
        productRequest.setProductDescription("Test book description");
        productRequest.setIsActive(true);
        productRequest.setProductPrice(19.99);
        productRequest.setAvailableQuantity(900);
        productRequest.setProductPartner(1002);
        productRequest.setIsbn("978-3-16-148410-2");
        productRequest.setAuthor("Dave Johnson Smith");
        
     	String responsePost =  postClient.post(productRequest, String.class);
        System.out.println("POST METHOD Response ........." + responsePost);
        System.out.println();
        
        /*****************************************************************************************
         * PUT METHOD invoke - Cell Product
        *****************************************************************************************/
        System.out.println("PUT METHOD .........................................................");
        WebClient putClient = WebClient.create("http://localhost:8081", providers);
        WebClient.getConfig(putClient).getOutInterceptors().add(new LoggingOutInterceptor());
        WebClient.getConfig(putClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        putClient = putClient.accept("application/xml").type("application/json").path("/productservice/products");
     	
        String putRequestURI = putClient.getCurrentURI().toString();
        System.out.println("Client PUT METHOD Request URI:  " + putRequestURI);
        String putRequestHeaders = putClient.getHeaders().toString();
        System.out.println("Client PUT METHOD Request Headers:  " + putRequestHeaders);
        ProductRequest productPutRequest = new ProductRequest();
        productPutRequest.setProduct_id(37);
        productPutRequest.setProdType(3);
        productPutRequest.setProductName("Cell Phone Test");
        productPutRequest.setProductDescription("This is a test of a CellPhone Product");
        productPutRequest.setIsActive(true);
        productPutRequest.setProductPrice(829.00);
        productPutRequest.setAvailableQuantity(200);
        productPutRequest.setProductPartner(1020);
        productPutRequest.setManufacturer("Samsung");
        productPutRequest.setModelNumber("Z5900");
        productPutRequest.setCarrier("Verizon");
        
     	String responsePut =  putClient.put(productPutRequest, String.class);
        System.out.println("PUT METHOD Response ........." + responsePut);
        System.out.println();
        
        /*****************************************************************************************
         * DELETE METHOD invoke
        *****************************************************************************************/
        
        System.out.println("DELETE METHOD .........................................................");
        WebClient deleteClient = WebClient.create("http://localhost:8081", providers);
        WebClient.getConfig(deleteClient).getOutInterceptors().add(new LoggingOutInterceptor());
        WebClient.getConfig(deleteClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to application/json get in json format
        deleteClient = deleteClient.accept("application/json").type("application/json").path("/productservice/products/34"); 
     	
        String deleteRequestURI = deleteClient.getCurrentURI().toString();
        System.out.println("Client DELETE METHOD Request URI:  " + deleteRequestURI);
        String deleteRequestHeaders = deleteClient.getHeaders().toString();
        System.out.println("Client DELETE METHOD Request Headers:  " + deleteRequestHeaders);
        
        deleteClient.delete();
        System.out.println("DELETE MEDTHOD Response ......... OK");
        
         
        System.exit(0);

	}

}
