package com.product;

public enum ProductType {
	 
	BOOK,
	ELECTRONICS,
	CELL_PHONE,
	LAPTOP;
	
	public int getProdTypeValue() {
		
		switch(this) {
		case BOOK:
			return 1;
		case ELECTRONICS:
			return 2;
		case CELL_PHONE:
			return 3;
		case LAPTOP:
			return 4;
		default:
			break;
		}
		return 0;
	}

}
