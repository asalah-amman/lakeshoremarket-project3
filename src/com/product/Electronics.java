package com.product;

/**
 * @author Kevin Morrissey
 * This Class extends Product and defines attributes and states of Electronics Objects.  
 */

public class Electronics extends Product {
	
	private String manufacturer;
	private String modelNumber;

	// added override constructor for flexibility. Defines Electronics specific attributes
	public Electronics(ProductType productType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber) {
		super(productType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner);
		this.manufacturer = manufacturer;
		this.modelNumber = modelNumber;
	}
	
	public Electronics() {
		// TODO Auto-generated constructor stub
	}

	// setters and getters
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	public String getManufacturer() {
		return this.manufacturer;
	}
	
	public String getModelNumber() {
		return this.modelNumber;
	}
	
	public String toString(){
		String delimiter="------------------------------------------";
		String output="";
		output+= super.toString();
		output+= manufacturer +"\n\r";
		output+= modelNumber +"\n\r";
		output+= delimiter+"\n\r";
		
		return output;
	}

}
