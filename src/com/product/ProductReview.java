package com.product;

import java.util.Date;

public class ProductReview {
	
	private long customer_id;
	private long product_id;
	private String customer_review;
	private Date review_date;
	private long review_id;
	
	//constructor
	public ProductReview() {
		
	}

	public ProductReview(long customer_id, long product_id, String customer_review, Date review_date) {
		this.customer_id = customer_id;
		this.product_id = product_id;
		this.customer_review = customer_review;
		this.review_date = review_date;
	}
	
	// setter methods
	public void setCustomerID(long customer_id) {
		this.customer_id = customer_id;
	}
	
	public void setProductID(long product_id) {
		this.product_id = product_id;
	}
	
	public void setReviewID(long review_id) {
		this.review_id = review_id;
	}
	
	public void setCustReview(String customer_review) {
		this.customer_review = customer_review;
	}
	
	public void setReviewDate(Date review_date) {
		this.review_date = review_date;
	}
	
	public void setReviewID(int review_id) {
		this.review_id = review_id;
	}
	
	// getter methods
	public long getCustomerID() {
		return this.customer_id;
	}
	
	public long getProductID() {
		return this.product_id;
	}
	
	public String getCustomerReview() {
		return customer_review;
	}
	
	public Date getReviewDate() {
		return review_date;
	}
	
	public long getReviewID() {
		return this.review_id;
	}

}
