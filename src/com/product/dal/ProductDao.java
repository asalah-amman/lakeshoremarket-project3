package com.product.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;




import com.customer.Customer;
//import com.address.*;
//import com.payment.*;
import com.product.*;
/**
 * 
 * @author Kevin Morrissey - based on CustomerDao by Alla Salah
 * This Class is used to encapsulate  methods to manipulate a database in relation to a Product object. 
 *
 */
public class ProductDao {
        
  private Connection connect = null;
  private Statement statement = null;
  private PreparedStatement preparedStatement = null;
  private ResultSet resultSet = null;
  private ProductType productType;

  
  final private String host = "localhost:3306";
  final private String user = "root";
  final private String passwd = "";
  
  
  
  public ProductDao(){

  }
  //----------------------------------------------------------------------------------------
  /**
   * Method is responsible for initializing a connection to the database
   * 
   * @return Connection object
   */
  public Connection initializeDB()throws Exception{
	  Connection dbConnection=null;
	 try{
		 Class.forName("com.mysql.jdbc.Driver");
		 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
	 } 
	 catch (SQLException e) {
      throw e;
     } 
	 finally {
      close();
    }
      
      return dbConnection;
  }//end method
  //----------------------------------------------------------------------------------------
  //----------------------------------------------------------------------------------------
  
  /*
   * * *******************************************************************************************
   										CREATE (INSERT) 
  *********************************************************************************************/
  /**
   * Method is responsible for inserting data into Products table. If insertion is successful it 
   * will return the ID of the product which would be > 0
   */
  private long insertProduct(Connection dbConnection, Product p)throws Exception{
	  long newProductId=-1;
	  productType = p.getProductType(); //get Product Type
	  
	  try{
		  
		  if (p instanceof Book) {
			  
			  String insertSql="INSERT INTO products(product_type_id, partner_id, product_name, product_desc, price, available_quantity, is_active, isbn, author)"+
	    		  			"SELECT ?, ?, ? , ?, ?,?,?,?,? "+
							"WHERE NOT EXISTS (SELECT 1 FROM products WHERE partner_id=? AND product_name=?)";
	      
			  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
	      
			  preparedStatement.setInt(1, productType.getProdTypeValue());
			  preparedStatement.setLong(2, p.getProdPartnerId());
			  preparedStatement.setString(3, p.getProductName());
			  preparedStatement.setString(4, p.getProductDescription());
			  preparedStatement.setDouble(5, p.getProductPrice());
			  preparedStatement.setInt(6, p.getAvailableQuantity());
			  preparedStatement.setInt(7, p.getProductStatus()? 1:0);
			  preparedStatement.setString(8, ((Book) p).getBookIsbn());
			  preparedStatement.setString(9, ((Book) p).getBookAuthor());
			  preparedStatement.setLong(10, p.getProdPartnerId());
			  preparedStatement.setString(11, p.getProductName());
	      
		  }
		  else if (p instanceof Electronics) {
			  
			  String insertSql="INSERT INTO products(product_type_id, partner_id, product_name, product_desc, price, available_quantity, is_active, manufacturer, model_number)"+
  		  			"SELECT ?, ?, ? , ?, ?,?,?,?,? "+
						"WHERE NOT EXISTS (SELECT 1 FROM products WHERE partner_id=? AND product_name=?)";
    
			  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
    
			  preparedStatement.setInt(1, productType.getProdTypeValue());
			  preparedStatement.setLong(2, p.getProdPartnerId());
			  preparedStatement.setString(3, p.getProductName());
			  preparedStatement.setString(4, p.getProductDescription());
			  preparedStatement.setDouble(5, p.getProductPrice());
			  preparedStatement.setInt(6, p.getAvailableQuantity());
			  preparedStatement.setInt(7, p.getProductStatus()? 1:0);
			  preparedStatement.setString(8, ((Electronics) p).getManufacturer());
			  preparedStatement.setString(9, ((Electronics) p).getModelNumber());
			  preparedStatement.setLong(10, p.getProdPartnerId());
			  preparedStatement.setString(11, p.getProductName());
		  }
		  else if (p instanceof CellPhone) {
			  
			  String insertSql="INSERT INTO products(product_type_id, partner_id, product_name, product_desc, price, available_quantity, is_active, manufacturer, model_number, carrier)"+
  		  			"SELECT ?, ?, ? , ?, ?,?,?,?,?,? "+
						"WHERE NOT EXISTS (SELECT 1 FROM products WHERE partner_id=? AND product_name=?)";
    
			  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
    
			  preparedStatement.setInt(1, productType.getProdTypeValue());
			  preparedStatement.setLong(2, p.getProdPartnerId());
			  preparedStatement.setString(3, p.getProductName());
			  preparedStatement.setString(4, p.getProductDescription());
			  preparedStatement.setDouble(5, p.getProductPrice());
			  preparedStatement.setInt(6, p.getAvailableQuantity());
			  preparedStatement.setInt(7, p.getProductStatus()? 1:0);
			  preparedStatement.setString(8, ((CellPhone) p).getManufacturer());
			  preparedStatement.setString(9, ((CellPhone) p).getModelNumber());
			  preparedStatement.setString(10, ((CellPhone) p).getCarrier());
			  preparedStatement.setLong(11, p.getProdPartnerId());
			  preparedStatement.setString(12, p.getProductName());
		  }
		  else if (p instanceof  Laptop) {
			  
			  String insertSql="INSERT INTO products(product_type_id, partner_id, product_name, product_desc, price, available_quantity, is_active, manufacturer, model_number, two_in_one)"+
  		  			"SELECT ?, ?, ? , ?, ?,?,?,?,?,? "+
						"WHERE NOT EXISTS (SELECT 1 FROM products WHERE partner_id=? AND product_name=?)";
    
			  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
    
			  preparedStatement.setInt(1, productType.getProdTypeValue());
			  preparedStatement.setLong(2, p.getProdPartnerId());
			  preparedStatement.setString(3, p.getProductName());
			  preparedStatement.setString(4, p.getProductDescription());
			  preparedStatement.setDouble(5, p.getProductPrice());
			  preparedStatement.setInt(6, p.getAvailableQuantity());
			  preparedStatement.setInt(7, p.getProductStatus()? 1:0);
			  preparedStatement.setString(8, ((Laptop) p).getManufacturer());
			  preparedStatement.setString(9, ((Laptop) p).getModelNumber());
			  preparedStatement.setInt(10, ((Laptop) p).getTwoInOne()? 1:0);
			  preparedStatement.setLong(11, p.getProdPartnerId());
			  preparedStatement.setString(12, p.getProductName());
		  }
	    
	      boolean r= preparedStatement.execute();
    	  ResultSet rs = preparedStatement.getGeneratedKeys();
    	  if (rs.next()) {
    		  newProductId=rs.getLong(1);
    	  }      

	      if(newProductId > 0 ){
	    	  p.setProductId(newProductId);
	      }
	  }
	  catch (Exception e) {
	      throw e;
	    } 
	  finally {
	      close();
	    }
	  
	  
	  return newProductId;
  }
  
 
  
  /**
   * Method is public method to call the private method insertIntoProducts()
   * 
   * @return void - update to return assigned ID to the ProductManager
   */
  	public long addProduct(Product p) throws Exception{
  		Connection insertCon=null;
  		long productId =-1;
  		
  		try{
  	  		insertCon = this.initializeDB();
  	  	    productId = this.insertProduct(insertCon, p);
  	  		
  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally{
  			close();
  		}
  		
  		return(productId); 
  	}
  	 


 /*
  * * *******************************************************************************************
  										UPDATE 
 *********************************************************************************************/

  	private boolean updateProductTable(Connection dbConnection, Product p)throws Exception{
  		boolean result=false;
  	
  		try{
 		 // PreparedStatements can use variables and are more efficient
  			if (p instanceof Book) {
  				preparedStatement = dbConnection.prepareStatement("UPDATE products "
 	    		  										+"SET product_name=? "
 	    		  										+" , product_desc=? "
 	    		  										+" , price=? "
 	    		  										+" , available_quantity=? "
 	    		  										+" , is_active=? "
 	    		  										+" , isbn=? "
 	    		  										+" , author=? "
 	    		  										+" WHERE product_id="+p.getProductId());
 	      
  				preparedStatement.setString(1, p.getProductName());
  				preparedStatement.setString(2, p.getProductDescription());
  				preparedStatement.setDouble(3, p.getProductPrice());
  				preparedStatement.setInt(4, p.getAvailableQuantity());
  				preparedStatement.setInt(5, p.getProductStatus()? 1:0);
  				preparedStatement.setString(6, ((Book) p).getBookIsbn());
  				preparedStatement.setString(7, ((Book) p).getBookAuthor());
  			}
  			
  			if (p instanceof Electronics) {
  				preparedStatement = dbConnection.prepareStatement("UPDATE products "
 	    		  										+"SET product_name=? "
 	    		  										+" , product_desc=? "
 	    		  										+" , price=? "
 	    		  										+" , available_quantity=? "
 	    		  										+" , is_active=? "
 	    		  										+" , manufacturer=? "
 	    		  										+" , model_number=? "
 	    		  										+" WHERE product_id="+p.getProductId());
 	      
  				preparedStatement.setString(1, p.getProductName());
  				preparedStatement.setString(2, p.getProductDescription());
  				preparedStatement.setDouble(3, p.getProductPrice());
  				preparedStatement.setInt(4, p.getAvailableQuantity());
  				preparedStatement.setInt(5, p.getProductStatus()? 1:0);
  				preparedStatement.setString(6, ((Electronics) p).getManufacturer());
  				preparedStatement.setString(7, ((Electronics) p).getModelNumber());
  			}
  			
  			if (p instanceof CellPhone) {
  				preparedStatement = dbConnection.prepareStatement("UPDATE products "
 	    		  										+"SET product_name=? "
 	    		  										+" , product_desc=? "
 	    		  										+" , price=? "
 	    		  										+" , available_quantity=? "
 	    		  										+" , is_active=? "
 	    		  										+" , manufacturer=? "
 	    		  										+" , model_number=? "
 	    		  										+" , carrier=? "
 	    		  										+" WHERE product_id="+p.getProductId());
 	      
  				preparedStatement.setString(1, p.getProductName());
  				preparedStatement.setString(2, p.getProductDescription());
  				preparedStatement.setDouble(3, p.getProductPrice());
  				preparedStatement.setInt(4, p.getAvailableQuantity());
  				preparedStatement.setInt(5, p.getProductStatus()? 1:0);
  				preparedStatement.setString(6, ((CellPhone) p).getManufacturer());
  				preparedStatement.setString(7, ((CellPhone) p).getModelNumber());
  				preparedStatement.setString(8, ((CellPhone) p).getCarrier());
  			}
  			
  			if (p instanceof Laptop) {
  				preparedStatement = dbConnection.prepareStatement("UPDATE products "
 	    		  										+"SET product_name=? "
 	    		  										+" , product_desc=? "
 	    		  										+" , price=? "
 	    		  										+" , available_quantity=? "
 	    		  										+" , is_active=? "
 	    		  										+" , manufacturer=? "
 	    		  										+" , model_number=? "
 	    		  										+" , two_in_one=? "
 	    		  										+" WHERE product_id="+p.getProductId());
 	      
  				preparedStatement.setString(1, p.getProductName());
  				preparedStatement.setString(2, p.getProductDescription());
  				preparedStatement.setDouble(3, p.getProductPrice());
  				preparedStatement.setInt(4, p.getAvailableQuantity());
  				preparedStatement.setInt(5, p.getProductStatus()? 1:0);
  				preparedStatement.setString(6, ((Laptop) p).getManufacturer());
  				preparedStatement.setString(7, ((Laptop) p).getModelNumber());
  				preparedStatement.setInt(8, ((Laptop) p).getTwoInOne()? 1:0);
  			}

  				
  			preparedStatement.executeUpdate();
  			result=true;
 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return result;
  	}
   	

  	
  	public boolean updateProduct(Product p) throws Exception{
  		Connection updateCon=null;
  		boolean result1=false; 
  
  		try{
  			updateCon= this.initializeDB();
  			result1=this.updateProductTable(updateCon, p);
  			
  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally {
  			close();
  		}
  		
  		return (result1);
  	}
 
 /*
  * *********************************************************************************************
  										GET 
 *********************************************************************************************/
  	
  	// Get product based on product ID
  	public Product getProduct(long id){
  		Product matchingProduct = null;
  		Connection getConnection=null;
  		
  		try {
  			getConnection= this.initializeDB();
  			
  			matchingProduct=this.getProductInfo(getConnection, id);
  			
		} catch (Exception e) {
			e.printStackTrace();
		}
  		return matchingProduct;
  	}
  	
  	// Get ArrayList of all products
  	public ArrayList <Product> getAllProducts() {
  		ArrayList <Product> productList= new ArrayList<Product>();
  		Connection getConnection=null;
  		
  		try {
  			getConnection= this.initializeDB();
  			
  			productList= this.getProducts(getConnection);
  			
		} catch (Exception e) {
			e.printStackTrace();
		}
  		return productList;
  	}
  	
  	// private method to return ArrayList of all products
  	private ArrayList <Product> getProducts(Connection dbConnection) throws Exception {
  		ArrayList <Product> productList= new ArrayList<Product>();
  		Product prod;
  		ResultSet resultSet;
  		
  		try {
  			dbConnection= this.initializeDB();
  			
  			String queryStr="SELECT product_id FROM products ORDER BY product_id";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
 			
 			boolean r = preparedStatement.execute();
 			if(r){
 				resultSet= preparedStatement.getResultSet();
  	        	while(resultSet.next()){
  	  	        	long productId= resultSet.getLong(1);
  	  	        	prod = this.getProduct(productId);
  	  	        	productList.add(prod);
  	        	}
  	        }
  			
		} catch (Exception e) {
			e.printStackTrace();
		}
  		return productList;
  	}
  	
  	// Get ArrayList of products from a search based on product name
  	public ArrayList <Product> searchForProduct(String productName) {
  		System.out.println("ProductDao: searchForProduct- searching for..."+productName);
  		ArrayList <Product> productList= new ArrayList<Product>();
  		Connection getConnection=null;

  		try {
  			getConnection= this.initializeDB();
  			productList= this.findProducts(getConnection,productName);
		} catch (Exception e) {
			e.printStackTrace();
		}
  		return productList;
  	}

  	//Private method to return an ArrayList of Products from a search based on product name
  	private ArrayList <Product> findProducts(Connection dbConnection, String productName) throws Exception {
  		ArrayList <Product> productList= new ArrayList<Product>();
  		Product prod;
  		ResultSet resultSet;

  		try {
  			dbConnection= this.initializeDB();
  			
  			String queryStr="SELECT product_id  FROM products WHERE product_name LIKE '%" + productName + "%'";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
 		
 			boolean r = preparedStatement.execute();
 			if(r){
 				resultSet= preparedStatement.getResultSet();
  	        	while(resultSet.next()){
  	  	        	long productId= resultSet.getLong(1);
  	  	        	prod = this.getProduct(productId);
  	  	        	if (prod.getProductStatus() == true) // check to active product
  	  	        		productList.add(prod);
  	        	}
  	        }
  			
		} catch (Exception e) {
			System.out.println("Exception caught" );
			e.printStackTrace();
		}
  		return productList;
  	}
  	
  	// Private method to get a product based on product ID
  	private Product getProductInfo(Connection dbConnection,long productId) throws Exception{
  		ResultSet resultSet;
  		Book book = new Book();
  		Electronics electronic = new Electronics();
  		CellPhone phone = new CellPhone(); 
  		Laptop laptop = new Laptop();
  		
  		try{
  			//System.out.println("Inside the getProductInfo, id= "+productId);
  			

  			String queryStr="SELECT product_id, product_type_id, partner_id, product_name, product_desc, price, available_quantity, is_active, isbn, author, manufacturer, model_number, carrier, two_in_one"+
  	  				" FROM products"+
  	  				" WHERE product_id=?";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, productId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 resultSet= preparedStatement.getResultSet();
  	        	if(resultSet.next()){
  	  	        	long pId= resultSet.getLong(1);
  	  	        	int prodTypeValue= resultSet.getInt(2); 	//product type
  	  	        	int partnerId= resultSet.getInt(3); 	//partner id
  	  	        	String prodName= resultSet.getString(4); 	//product name
  	  	        	String prodDescription= resultSet.getString(5);	//productDescription
  	  	        	double prodPrice= resultSet.getDouble(6); 		//product price
  	  	        	int availQty= resultSet.getInt(7); //available quantity
  	  	        	int prodStatus = resultSet.getInt(8);
  	  	        	String isbn = resultSet.getString(9); // isbn
  	  	        	String author = resultSet.getString(10); //author
  	  	        	String manufacturer = resultSet.getString(11); // maufacturer
  	  	        	String modelNumber = resultSet.getString(12); // model number
  	  	        	String carrier = resultSet.getString(13); //carrier
  	  	        	int twoInOne = resultSet.getInt(14); // twoInOne
  	  	        	
  	  	        	
  	  	        	if (prodTypeValue == 1) {
  	  	        		book.setProductType(ProductType.BOOK);
  	  	        		book.setBookIsbn(isbn);
  	  	        		book.setBookAuthor(author);
  	  	        		book.setProductId(pId);
  	  	        		book.setProdPartnerId(partnerId);
  	  	        		book.setProductName(prodName);
  	  	        		book.setProductDescription(prodDescription);
  	  	        		book.setProductPrice(prodPrice);
  	  	        		book.setAvailableQuantity(availQty);
  	  	        		book.setProductStatus((prodStatus==1)? true:false);
  	  	        		return book;
  	  	        	}
  	  	        	if (prodTypeValue == 2) {
  	  	        		electronic.setProductType(ProductType.ELECTRONICS);
  	  	        		electronic.setManufacturer(manufacturer);
  	  	        		electronic.setModelNumber(modelNumber);
  	  	        		electronic.setProductId(pId);
  	  	        		electronic.setProdPartnerId(partnerId);
  	  	        		electronic.setProductName(prodName);
  	  	        		electronic.setProductDescription(prodDescription);
  	  	        		electronic.setProductPrice(prodPrice);
  	  	        		electronic.setAvailableQuantity(availQty);
  	  	        		electronic.setProductStatus((prodStatus==1)? true:false);
  	  	        		return electronic;
  	  	        	}
  	  	        	if (prodTypeValue == 3) {
  	  	        		phone.setProductType(ProductType.CELL_PHONE);
  	  	        		phone.setManufacturer(manufacturer);
	  	        		phone.setModelNumber(modelNumber);
	  	        		phone.setCarrier(carrier);
	  	        		phone.setProductId(pId);
	  	  	        	phone.setProdPartnerId(partnerId);
	  	  	        	phone.setProductName(prodName);
	  	  	        	phone.setProductDescription(prodDescription);
	  	  	        	phone.setProductPrice(prodPrice);
	  	  	        	phone.setAvailableQuantity(availQty);
	  	  	        	phone.setProductStatus((prodStatus==1)? true:false);
	  	  	        	return phone;
  	  	        	}
  	  	        	if (prodTypeValue == 4) {
  	  	        		laptop.setProductType(ProductType.LAPTOP);
  	  	        		laptop.setManufacturer(manufacturer);
	  	        		laptop.setModelNumber(modelNumber);
	  	        		laptop.setTwoInOne((twoInOne==1)? true:false);
	  	        		laptop.setProductId(pId);
	  	  	        	laptop.setProdPartnerId(partnerId);
	  	  	        	laptop.setProductName(prodName);
	  	  	        	laptop.setProductDescription(prodDescription);
	  	  	        	laptop.setProductPrice(prodPrice);
	  	  	        	laptop.setAvailableQuantity(availQty);
	  	  	        	laptop.setProductStatus((prodStatus==1)? true:false);
	  	  	        	return laptop;
  	  	        	}
  	  	        	
  	  	        }
  	        }

  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
  		
		return null;
  	}
  	
  
  
  	 /*
  	  *********************************************************************************************
  	  										DELETE 
  	 *********************************************************************************************/
  	
  	public boolean deleteProduct(long id){
  		Product targetProduct = null;
  		Connection getConnection=null;
  		boolean deleted = false;
  		//System.out.println("In deleteProduct method of ProductDao.....id is " + id);
  		
  		try {
  			getConnection= this.initializeDB();
  			targetProduct=this.getProductInfo(getConnection, id);
  			targetProduct.setProductStatus(false);
  			deleted = this.updateProductTable(getConnection, targetProduct);
  			
		} catch (Exception e) {
			e.printStackTrace();
		}
  		finally {
  			close();
  		}
  		return deleted;
  	}
  
  

  // You need to close the resultSet
  private void close() {
    try {
      if (resultSet != null) {
        resultSet.close();
      }

      if (statement != null) {
        statement.close();
      }

      if (connect != null) {
        connect.close();
      }
    } catch (Exception e) {

    }
 
  }

}