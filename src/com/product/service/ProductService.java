package com.product.service;

/**
 * @author Kevin Morrissey
 * ProductService class defines required methods 
 */


import javax.jws.WebService;

import com.product.service.representation.ProductRepresentation;
import com.product.service.representation.ProductRequest;


@WebService
public interface ProductService {
	
	public ProductRepresentation getProduct(String product_id);
	public ProductRepresentation createProduct(ProductRequest productRequest); 
	public ProductRepresentation updateProduct(ProductRequest productRequest);  

}
