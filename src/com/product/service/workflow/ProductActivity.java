package com.product.service.workflow;

/**
 * @author Kevin Morrissey
 * ProductActivity class manages communication between ProductResource and ProductManager
 **/

import java.util.ArrayList;
import java.util.Iterator;

import com.product.*;
import com.product.service.representation.*;

public class ProductActivity {
	
	private static ProductManager pm = new ProductManager();
	
	// create Book product
	public ProductRepresentation createBook(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String isbn, String author) {
		
		Book prod = (Book) pm.addBook(prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, isbn, author);
		ProductRepresentation pr = createProdRep(prod);

		return pr;
	}
	
	// create Electronics product
	public ProductRepresentation createElectronics(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber) {
				
		Electronics prod = (Electronics) pm.addElectronics(prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber);
		ProductRepresentation pr = createProdRep(prod);

		return pr;	
	}
		
	// create CellPhone product
	public ProductRepresentation createCellPhone(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, String carrier) {
			
		CellPhone prod = (CellPhone) pm.addCellPhone(prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber, carrier);
		ProductRepresentation pr = createProdRep(prod);

		return pr;	
	}
	
	// create LapTop product
	public ProductRepresentation createLaptop(int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, boolean twoInOne) {
				
		Laptop prod = (Laptop) pm.addLaptop(prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber, twoInOne);
		ProductRepresentation pr = createProdRep(prod);

		return pr;	
	}
	
	// update Book product
	public ProductRepresentation updateBook(long product_id, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String isbn, String author) {
			
		Book prod = (Book) pm.updateBook(product_id, prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, isbn, author);
		ProductRepresentation pr = createProdRep(prod);
	
		return pr;
	}
	
	// update Electronics product
	public ProductRepresentation updateElectronics(long product_id, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber) {
						
		Electronics prod = (Electronics) pm.updateElectronics(product_id, prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber);
		ProductRepresentation pr = createProdRep(prod);
			
		return pr;		
	}
	
	// update CellPhone product
	public ProductRepresentation updateCellPhone(long product_id, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, String carrier) {
				
		CellPhone prod = (CellPhone) pm.updateCellPhone(product_id, prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber, carrier);
		ProductRepresentation pr = createProdRep(prod);
	
		return pr;		
	}
	
	// update Laptop product
	public ProductRepresentation updateLaptop(long product_id, int prodType, String productName, String productDescription, boolean isActive, double productPrice, int availableQuantity, 
			long productPartner, String manufacturer, String modelNumber, boolean twoInOne) {
					
		Laptop prod = (Laptop) pm.updateLaptop(product_id, prodType, productName, productDescription, isActive, productPrice, availableQuantity, productPartner, manufacturer, modelNumber, twoInOne);
		ProductRepresentation pr = createProdRep(prod);
		
		return pr;		
	}
	
	

	// get Product
	public ProductRepresentation getProduct(String productId) {
		long id = Long.valueOf(productId);	
		Product prod = pm.getProduct(id);
		ProductRepresentation pr = null;
		
		if (prod instanceof Book) {
			Book book = (Book) prod;
			pr = createProdRep(book);
		}
		if (prod instanceof CellPhone) {
			CellPhone phone = (CellPhone) prod;
			pr = createProdRep(phone);
		}
		if (prod instanceof Laptop) {
			Laptop laptop = (Laptop) prod;
			pr = createProdRep(laptop);
		}
		if (prod instanceof Electronics) {
			Electronics elect = (Electronics) prod;
			pr = createProdRep(elect);
		}
		// Add the links
		setLinks(pr, "Buy");
		
		return pr;
	}
	
	// Get all Products
	public ArrayList <ProductRepresentation> getProducts() {
		ArrayList <Product> productList = pm.getAllProducts();
		ArrayList <ProductRepresentation> productRepresentations = new ArrayList <ProductRepresentation> ();
		productList = pm.getAllProducts();
		
		Iterator<Product> it = productList.iterator();
		while(it.hasNext()) {
          Product prod = (Product)it.next();
          ProductRepresentation pr = new ProductRepresentation();
          pr = createProdRep(prod);
          
          // Add the links
          setLinks(pr, "View");
         
          //now add this representation in the list
          productRepresentations.add(pr);
		}
		return productRepresentations;
	}
	
	// Search for product based on product name field. Returns summary representation of products matching the search
	public ArrayList <ProductRepSummary> searchProduct(String productName) {
		ArrayList <Product> productList = pm.searchProduct(productName);
		ArrayList <ProductRepSummary> productRepSummaries = new ArrayList <ProductRepSummary> ();
		
		Iterator<Product> it = productList.iterator();
		while(it.hasNext()) {
          Product prod = (Product)it.next();
          ProductRepSummary pr = new ProductRepSummary();
          pr = createProdRepSummary(prod);
          
          // Add the links
          setLinks(pr, "View");
    
          //now add this representation in the list
          productRepSummaries.add(pr);
		}
				
		return productRepSummaries;
	}
	
	// delete Product
	public String deleteProduct(String productId) {
		//System.out.println("In the deleteProduct method of ProductActivity. ID is " + productId);
		long id = Long.valueOf(productId);	
		
		boolean deleted = false;
		deleted = pm.deleteProduct(id);
		if (deleted == true) {
			System.out.println("Returning OK");
			return "OK";
		}
		else {
			System.out.println("Returning failed");
			return "Failed";
		}
	}
	
	
	// create ProductRepresentation from Product Object
	private ProductRepresentation createProdRep(Product prod) {
		ProductRepresentation prodRep = new ProductRepresentation();
		
		prodRep.setProduct_id(prod.getProductId());
		prodRep.setProductName(prod.getProductName());
		prodRep.setProductDescription(prod.getProductDescription());
		prodRep.setIsActive(prod.getProductStatus());
		prodRep.setProductPrice(prod.getProductPrice());
		prodRep.setAvailableQuantity(prod.getAvailableQuantity());
		prodRep.setProductPartner(prod.getProdPartnerId());
		
		if (prod instanceof Book) {
			Book book = (Book) prod;
			int prodType = ProductType.BOOK.getProdTypeValue();
			prodRep.setProdType(prodType);
			prodRep.setIsbn(book.getBookIsbn());
			prodRep.setAuthor(book.getBookAuthor());
			return prodRep;
		}
		if (prod instanceof CellPhone) {
			CellPhone phone = (CellPhone) prod;
			int prodType = ProductType.CELL_PHONE.getProdTypeValue();
			prodRep.setProdType(prodType);
			prodRep.setManufacturer(phone.getManufacturer());
			prodRep.setModelNumber(phone.getModelNumber());
			prodRep.setCarrier(phone.getCarrier());
			return prodRep;
		}
		if (prod instanceof Laptop) {
			Laptop laptop = (Laptop) prod;
			int prodType = ProductType.LAPTOP.getProdTypeValue();
			prodRep.setProdType(prodType);
			prodRep.setManufacturer(laptop.getManufacturer());
			prodRep.setModelNumber(laptop.getModelNumber());
			prodRep.setTwoInOne(laptop.getTwoInOne());
			return prodRep;
		}
		if (prod instanceof Electronics) {
			Electronics elect = (Electronics) prod;
			int prodType = ProductType.ELECTRONICS.getProdTypeValue();
			prodRep.setProdType(prodType);
			prodRep.setManufacturer(elect.getManufacturer());
			prodRep.setModelNumber(elect.getModelNumber());
			return prodRep;
		}
		
		return null;
		
	}
	
	// create ProductRepresentation Summary from Product Object
	private ProductRepSummary createProdRepSummary(Product prod) {
		ProductRepSummary prodRep = new ProductRepSummary();
			
		prodRep.setProduct_id(prod.getProductId());
		prodRep.setProductName(prod.getProductName());
		prodRep.setProductDescription(prod.getProductDescription());
		prodRep.setProductPrice(prod.getProductPrice());
		
		return prodRep;
			
	}
	

	/**
	 * Sets all the links appropriately for ProductRepresentation object
	 * @param orderRep
	 */
	private void setLinks(ProductRepresentation prodRep, String state) {
		// Set up the activities that can be performed on orders
		String action = state;
		ProductRepresentation pRep = prodRep;
		
		if (action == "View") {
			String url = "http://localhost:8081/productservice/products/" + pRep.getProduct_id();
			Link view = new Link("View", url);
			view.setType("GET");
			pRep.setLinks(view);
		}
		
		if (action == "Buy") {
			String url = "http://localhost:8081/orderservice/orders";
			Link buy = new Link("buy", url);
			buy.setType("POST");
			pRep.setLinks(buy);
		}
		
		/*
		 *  nextState functionality that we can use if we want to. Matches Order. Original implementation above
		Link newLink= new Link();
		Link newLink= new Link();
		
		if (action == "View") {
			String url = "http://localhost:8081/productservice/products/" + pRep.getProduct_id();
			newLink.setAction("view");  
			newLink.setType("GET"); 
			newLink.setUrl(url);;
		}
		
		if (action == "Buy") {
			String url = "http://localhost:8081/orderservice/orders";
			newLink.setAction("buy");  
			newLink.setType("POST"); 
			newLink.setUrl(url);
		}
		*/
		
	}
	
	// Set links for ProductRepSummary object
	private void setLinks(ProductRepSummary prodRep, String state) {
		// Set up the activities that can be performed on orders
		String action = state;
		ProductRepSummary pRep = prodRep;
		
		if (action == "View") {
			String url = "http://localhost:8081/productservice/products/" + pRep.getProduct_id();
			Link view = new Link("view", url);
			view.setType("GET");
			pRep.setLinks(view);
		}
		
		if (action == "Buy") {
			String url = "http://localhost:8081/customerservice/customer/";
			Link buy = new Link("buy", url);
			buy.setType("POST");
			pRep.setLinks(buy);
		}
		
		
		
		/*
		 * nextState functionality that we can use if we want to. Matches order. Original implementation above
		Link newLink= new Link();
		
		if (action == "View") {
			String url = "http://localhost:8081/productservice/products/" + pRep.getProduct_id();
			newLink.setAction("view");  
			newLink.setType("GET"); 
			newLink.setUrl(url);
		}
		
		if (action == "Buy") {
			String url = "http://localhost:8081/orderservice/orders";
			newLink.setAction("buy");  
			newLink.setType("POST"); 
			newLink.setUrl(url);
		}
		*/
		
	}

}
