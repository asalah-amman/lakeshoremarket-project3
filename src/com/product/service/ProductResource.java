package com.product.service;

/**
 * @author Kevin Morrissey
 * ProductResource class defines CRUD actions, URIs, and media types
 */

import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.product.service.representation.ProductRepresentation;
import com.product.service.representation.ProductRepSummary;
import com.product.service.representation.ProductRequest;
import com.product.service.workflow.ProductActivity;


@Path("/productservice/")
public class ProductResource implements ProductService {
	
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/products")
	public ProductRepresentation createProduct(ProductRequest productRequest) {
			
		ProductActivity prodActivity = new ProductActivity();
		ProductRepresentation prodRep; 
		
		if (productRequest.getProdType() == 1) {
			// Create Book Product
			prodRep =  prodActivity.createBook(productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
				productRequest.getProductPartner(), productRequest.getIsbn(), productRequest.getAuthor());
		}
		else if (productRequest.getProdType() == 2) {
			// Create Electronics Product
			prodRep = prodActivity.createElectronics(productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
				productRequest.getProductPartner(), productRequest.getManufacturer(), productRequest.getModelNumber());
		}
		else if (productRequest.getProdType() == 3) {
			// Create Cell Phone Product
			prodRep = prodActivity.createCellPhone(productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
				productRequest.getProductPartner(), productRequest.getManufacturer(), productRequest.getModelNumber(), productRequest.getCarrier());
		}
		else if (productRequest.getProdType() == 4) {
			// Create Laptop Product
			prodRep = prodActivity.createLaptop(productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
				productRequest.getProductPartner(), productRequest.getManufacturer(), productRequest.getModelNumber(), productRequest.getTwoInOne());
		}
		else 
			prodRep = null;
		
		return prodRep;
	}
	
	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("/products")
	public ProductRepresentation updateProduct(ProductRequest productRequest) {
			
		ProductActivity prodActivity = new ProductActivity();
		ProductRepresentation prodRep; 
		
		if (productRequest.getProdType() == 1) {
			// Update Book Product
			prodRep =  prodActivity.updateBook(productRequest.getProduct_id(), productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
					productRequest.getProductPartner(), productRequest.getIsbn(), productRequest.getAuthor());
		}
		else if (productRequest.getProdType() == 2) {
			// Update Electronics Product
			prodRep = prodActivity.updateElectronics(productRequest.getProduct_id(), productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
					productRequest.getProductPartner(), productRequest.getManufacturer(), productRequest.getModelNumber());
		}
		else if (productRequest.getProdType() == 3) {
			// Update Cell Phone Product
			prodRep = prodActivity.updateCellPhone(productRequest.getProduct_id(), productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
					productRequest.getProductPartner(), productRequest.getManufacturer(), productRequest.getModelNumber(), productRequest.getCarrier());
		}
		else if (productRequest.getProdType() == 4) {
			// Update Laptop Product
			prodRep = prodActivity.updateLaptop(productRequest.getProduct_id(), productRequest.getProdType(), productRequest.getProductName(), productRequest.getProductDescription(), productRequest.getIsActive(), productRequest.getProductPrice(), productRequest.getAvailableQuantity(), 
					productRequest.getProductPartner(), productRequest.getManufacturer(), productRequest.getModelNumber(), productRequest.getTwoInOne());
		}
		else 
			prodRep = null;
		
		return prodRep;
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/products/{productId}")
	public ProductRepresentation getProduct(@PathParam("productId") String id) {
		//System.out.println("GET METHOD Request from Client with productRequest String ............." + id);
		ProductActivity prodActivity = new ProductActivity();
		return prodActivity.getProduct(id);
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/products")
	//@Cacheable(cc="public, maxAge=3600") example for caching
	public ArrayList<ProductRepresentation> getProducts() {
		ProductActivity prodActivity = new ProductActivity();
		return prodActivity.getProducts();	
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/products/search/{productName}")
	public ArrayList<ProductRepSummary> searchProduct(@PathParam("productName") String productName) {
		//System.out.println("GET METHOD Request from Client with productRequest String ............." + id);
		ProductActivity prodActivity = new ProductActivity();
		return prodActivity.searchProduct(productName); //searches for product
	}
	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/products/{productId}")
	public Response deleteProduct(@PathParam("productId") String id) {
		//System.out.println("Delete METHOD Request from Client with productRequest String ....." + id);
		ProductActivity prodActivity = new ProductActivity();
		String res = prodActivity.deleteProduct(id);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		if (res.equals("Failed")) {
			return Response.status(Status.NOT_MODIFIED).build();
		}
		else return null;
	}
	


	

}
