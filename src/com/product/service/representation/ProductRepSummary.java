package com.product.service.representation;

/**
 * @author Kevin Morrissey
 * ProductRepresentation class defines setters and getters for Product attributes
 **/

import com.product.service.workflow.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")

public class ProductRepSummary extends AbstractRepresentation {
	
	//Product Variables
	private long product_id;
	private String productName;
	private String productDescription;
	private double productPrice;
	private String gid;
	//private Link nextState;

	
		
	public ProductRepSummary() {}
	
	// Product setters
	public void setProduct_id(long product_id) {
		this.product_id = product_id;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}
	
	/*public void setNextState(Link newNextState) {
		this.nextState = newNextState;
	}
	*/
	
	
	// Product getters
	public String getGid() {
		return gid;
	}
	
	public long getProduct_id() {
		return product_id;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public String getProductDescription() {
		return productDescription;
	}

	public double getProductPrice() {
		return productPrice;
	}
		
}

