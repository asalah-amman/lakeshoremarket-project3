package com.product.service.representation;

/**
 * @author Kevin Morrissey
 * ProductRepresentation class defines setters and getters for Product attributes
 **/

import com.product.service.workflow.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")

public class ProductRepresentation extends AbstractRepresentation {
	
	//Product Variables
	private int prodType;
	private String productName;
	private String productDescription;
	private boolean isActive;
	private double productPrice;
	private int availableQuantity;
	private long productPartner;
	private long product_id;
	private String gid;
	//private Link nextState;

	
	// Book variables
	private String isbn;
	private String author;
	
	// Electronics variables
	private String manufacturer;
	private String modelNumber;
	
	//CellPhone variables
	private String carrier;
	
	//LapTop variables
	private boolean twoInOne;

	
	public ProductRepresentation() {}
	
	// Product setters
	public void setProdType(int prodType) {
		this.prodType = prodType;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	
	public void setAvailableQuantity (int availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
	
	public void setProductPartner(long productPartner) {
		this.productPartner = productPartner;
	}
	
	public void setProduct_id(long product_id) {
		this.product_id = product_id;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}
	
	/*public void setNextState(Link newNextState) {
		this.nextState = newNextState;
	}
	*/
	
	
	// Product getters
	public String getGid() {
		return gid;
	}
	
	public int getProdType() {
		return prodType;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public String getProductDescription() {
		return productDescription;
	}
	
	public boolean getIsActive() {
		return isActive;
	}
	
	public double getProductPrice() {
		return productPrice;
	}
		
	public int getAvailableQuantity () {
		return this.availableQuantity;
	}
	
	public long getProductPartner() {
		return this.productPartner;
	}
	
	public long getProduct_id() {
		return this.product_id;
	}
	
	/*public Link getNextState() {
	return nextState;
	}
	 */
	
	// Book Setters and Getters
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getIsbn() {
		return isbn;
	}
	
	public String getAuthor() {
		return author;
	}
	
	
	//Electronics Setters and Getters
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public String getModelNumber() {
		return modelNumber;
	}
	
	//Cell Phone Setters and Getters
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
		
	public String getCarrier() {
		return carrier;
	}
	
	//Laptop Setters and Getters
	public void setTwoInOne(boolean twoInOne) {
		this.twoInOne = twoInOne;
	}
				
	public boolean getTwoInOne() {
		return twoInOne;
	}
		

}
