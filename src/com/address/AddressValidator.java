package com.address;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class AddressValidator {
	 //Class Attributes
	  private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;
	  private ArrayList<Location> cityList;
	  private ArrayList<Location> zipList;
	  private ArrayList<Location> stateList;
	  private ArrayList<Location> countryList;
	  
	  final private String host = "localhost:3306";
	  final private String user = "root";
	  final private String passwd = "";
	  

	  public AddressValidator (){
		  this.cityList= new ArrayList<Location>();
		  this.zipList= new ArrayList<Location>();
		  this.stateList= new ArrayList<Location>();
		  this.countryList= new ArrayList<Location>();
		  
	  }
	//----------------------------------------------------------------------------------------
	  /**
	   * Method is responsible for initializing a connection to the database
	   * 
	   * @return Connection object
	   */
	  public Connection initializeDB()throws Exception{
		  Connection dbConnection=null;
		 try{
			 Class.forName("com.mysql.jdbc.Driver");
			 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
			 
		 } 
		 catch (SQLException e) {
	      throw e;
	     } 
		 finally {
	      this.close();
	    }
	      
	      return dbConnection;
	  }//end method
	  
	  
	  
	  private boolean getCities(Connection dbConnection)throws Exception{
		  cityList= new ArrayList<Location>();
		  boolean result=false;
		  try{
		      preparedStatement = dbConnection.prepareStatement("SELECT geography_id,geography_name FROM  geographies WHERE level_id=1");
		      preparedStatement.executeQuery();
		      ResultSet rs=preparedStatement.getResultSet();
		      while(rs.next()){
		    	  int id= rs.getInt(1);
		    	  String name=rs.getString(2);
		    	  
		    	  Location newLocation= new Location(id,name,"");
		    	  cityList.add(newLocation);
		      }
		      
		      if(cityList.size() > 0)
		    	  result=true;
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      this.close();
		    }
 
		  return result;
	  }
	  
	  private boolean getZips(Connection dbConnection)throws Exception{
		  zipList= new ArrayList<Location>();
		  boolean result=false;

		  try{
		      preparedStatement = dbConnection.prepareStatement("SELECT geography_id,geography_name FROM  geographies WHERE level_id=2");
		      preparedStatement.executeQuery();
		      ResultSet rs=preparedStatement.getResultSet();
		      while(rs.next()){
		    	  int id= rs.getInt(1);
		    	  String name=rs.getString(2);
		    	  
		    	  Location newLocation= new Location(id,name,"");
		    	  zipList.add(newLocation);
		      }
		      
		      if(zipList.size() > 0)
		    	  result=true;

		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      this.close();
		    }
		  return result;

	  }
	  

	  private boolean getStates(Connection dbConnection)throws Exception{
		  stateList= new ArrayList<Location>();
		  boolean result=false;

		  try{
		      preparedStatement = dbConnection.prepareStatement("SELECT geography_id,geography_name FROM  geographies WHERE level_id=3");
		      preparedStatement.executeQuery();
		      ResultSet rs=preparedStatement.getResultSet();
		      while(rs.next()){
		    	  int id= rs.getInt(1);
		    	  String name=rs.getString(2);
		    	  
		    	  Location newLocation= new Location(id,name,"");
		    	  stateList.add(newLocation);
		      }
		      
		      if(stateList.size() > 0)
		    	  result=true;

		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      this.close();
		    }
		  return result;

	  }
	  
	  private boolean getCountries(Connection dbConnection)throws Exception{
		  countryList= new ArrayList<Location>();
		  boolean result=false;

		  try{
		      preparedStatement = dbConnection.prepareStatement("SELECT geography_id,geography_name FROM  geographies WHERE level_id=4");
		      preparedStatement.executeQuery();
		      ResultSet rs=preparedStatement.getResultSet();
		      while(rs.next()){
		    	  int id= rs.getInt(1);
		    	  String name=rs.getString(2);
		    	  
		    	  Location newLocation= new Location(id,name,"");
		    	  countryList.add(newLocation);
		      }
		      
		      if(countryList.size() > 0)
		    	  result=true;

		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      this.close();
		    }
		  return result;

	  }
	  
	  public boolean populateCities(){
		  boolean result=false;
		  try {
			  connect= this.initializeDB();
			  result=this.getCities(connect);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	  }
	  public boolean populateZips(){
		  boolean result=false;
		  try {
			  connect= this.initializeDB();
			  result=this.getZips(connect);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	  }
	  
	  public boolean populateStates(){
		  boolean result=false;
		  try {
			  connect= this.initializeDB();
			  result=this.getStates(connect);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	  }
	  
	 
	  
	  public boolean populateCountries(){
		  boolean result=false;
		  try {
			  connect= this.initializeDB();
			  result=this.getCountries(connect);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	  }
	  
	  
	  public long getCityId(String city){
		  long cityId=-1;
		  
		  for(int i=0; i< cityList.size(); i++){
			  if(cityList.get(i).getLocationName().toUpperCase().equals(city.toUpperCase()))
				  cityId=cityList.get(i).getLocationId();
		  }
		  return cityId;
	  }
	  
	  public String getCityName(long cityId){
		  String cName="";
		  
		  for(int i=0; i< cityList.size(); i++){
			  if(cityList.get(i).getLocationId() == cityId){
				  cName= cityList.get(i).getLocationName();
			  }
			  
		  }
		  
		  return cName;
	  }
	  
	  public long getZipId(String zip){
		  long zipId=-1;
		  
		  for(int i=0; i< zipList.size(); i++){
			  if(zipList.get(i).getLocationName().toUpperCase().equals(zip.toUpperCase()))
				  zipId=zipList.get(i).getLocationId();
		  }
		  return zipId;
	  }
	  
	  public long getStateId(String state){
		  long stateId=-1;
		  
		  for(int i=0; i< stateList.size(); i++){
			  if(stateList.get(i).getLocationName().toUpperCase().equals(state.toUpperCase()))
				  stateId=stateList.get(i).getLocationId();
		  }
		  return stateId;
	  }
	  
	  public String getStateName(long stateId){
		  String sName="";
		  
		  for(int i=0; i< stateList.size(); i++){
			  if(stateList.get(i).getLocationId() == stateId){
				  sName= stateList.get(i).getLocationName();
			  }
			  
		  }
		  
		  return sName;
	  }
	  
	  public long getCountryId(String country){
		  long countryId=-1;
		  
		  for(int i=0; i< countryList.size(); i++){
			  if(countryList.get(i).getLocationName().toUpperCase().equals(country.toUpperCase()))
				  countryId=countryList.get(i).getLocationId();
		  }
		  return countryId;
	  }
	  
	  
	  public String getCountryName(long cId){
		  String cName="";
		  
		  for(int i=0; i< countryList.size(); i++){
			  if(countryList.get(i).getLocationId() == cId){
				  cName= countryList.get(i).getLocationName();
			  }
			  
		  }
		  
		  return cName;
	  }
	  
	  
	  public ArrayList<Location> getCityList() {
		  return cityList;
	  }



	  public ArrayList<Location> getZipList() {
		  return zipList;
	  }



	  public ArrayList<Location> getStateList() {
		  return stateList;
	  }



	  public ArrayList<Location> getCountryList() {
		  return countryList;
	  }

	  // You need to close the resultSet
	  private void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
	  
	  
	  @Override 
	  public String toString(){
		  String output="";
		  String delimiter="----------------------------------------";
		  output+=delimiter+ "\n\r";
		  output+="Cities ("+cityList.size()+") :\n\r";
		  for(int i=0; i < this.cityList.size(); i++){
			  output+= cityList.get(i).getLocationId() + " - "+ cityList.get(i).getLocationName()+ "\n\r";;
		  }
		  output+=delimiter+ "\n\r";
		  output+="States ("+stateList.size()+") :\n\r";
		  output+=delimiter+ "\n\r";
		  for(int i=0; i < this.stateList.size(); i++){
			  output+= stateList.get(i).getLocationId() + " - "+ stateList.get(i).getLocationName()+ "\n\r";;
		  }
		  
		  
		  output+=delimiter+ "\n\r";
		  output+="Countries ("+countryList.size()+") :\n\r";
		  output+=delimiter+ "\n\r";
		  for(int i=0; i < this.countryList.size(); i++){
			  output+= countryList.get(i).getLocationId() + " - "+ countryList.get(i).getLocationName()+ "\n\r";;
		  }
		  return output;
	  }
	  

}
