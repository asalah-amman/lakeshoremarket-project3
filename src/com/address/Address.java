package com.address;


public class Address {
	
	//Class Attributes
	private long addressId;
	private AddressType typeOfAddress;
	private String streetAddress;
	private String apartmentUnitNum;
	private String city;
	private String zip;
	private String state;
	private String country;
	private boolean isActive;


	//Constructors
	public Address (){
		this.streetAddress="";
		this.apartmentUnitNum="";
		this.city="";
		this.zip="";
		this.state="";
		this.country="";
	}
 
	public Address (String street, String city, String zip, String state, String country){
		this.streetAddress=street;
		this.apartmentUnitNum="";
		this.city=city;
		this.zip=zip;
		this.state=state;
		this.country=country;
	}
	
	public Address (String street, String apartUnitNum, String city, String zip, String state, String country){
		this.streetAddress=street;
		this.apartmentUnitNum= apartUnitNum;
		this.city=city;
		this.zip=zip;
		this.state=state;
		this.country=country;
	}

	
	//Methods
	public long getAddressId() {
		return addressId;
	}
	public AddressType getTypeOfAddress() {
		return typeOfAddress;
	}
	public String getStreetAddress() {return streetAddress;}
	
	public String getApartmentUnitNum() {return apartmentUnitNum;}

	public String getCity() {return city;}

	public String getZip() {return zip;}

	public String getState() {return state;}

	public String getCountry() {return country;}
	
	public boolean isActive() {
		return isActive;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
	
	public void setTypeOfAddress(AddressType typeOfAddress) {
		this.typeOfAddress = typeOfAddress;
	}
	
	public void setZip(String zip) {this.zip = zip;
	}
	
	public void setState(String state) {
		this.state = state;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public void setApartmentUnitNum(String apartmentUnitNum) {
		this.apartmentUnitNum = apartmentUnitNum;
	}
	
	public void setCity(String city) {
		this.city = city;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	@Override 
	public int hashCode(){
		int num;
		num= Integer.valueOf(this.getStreetAddress().toUpperCase()) + Integer.valueOf( this.getApartmentUnitNum().toUpperCase()) + Integer.valueOf( this.getZip().toUpperCase())
			+ Integer.valueOf( this.getCity().toUpperCase()) + Integer.valueOf( this.getState().toUpperCase()) + Integer.valueOf( this.getCountry().toUpperCase());
		return num;
	}
	
	@Override 
	public boolean equals(Object obj){
		boolean result=false;
		Address a= (Address)obj;
		
		boolean matchingStreet= (a.getStreetAddress().equals(this.getStreetAddress()))? true:false;
		boolean matchingAptNum= (a.getApartmentUnitNum().equals(this.getApartmentUnitNum()))? true:false;
		boolean matchingCity= (a.getCity().equals(this.getCity()))? true:false;
		boolean matchingState= (a.getState().equals(this.getState()))? true:false;
		boolean matchingCountry= (a.getCountry().equals(this.getCountry()))? true:false;
		
		return (matchingStreet && matchingAptNum && matchingCity && matchingState && matchingCountry);
	}
	
	@Override
	public String toString(){
		boolean isEmpty=false;
		String output="";
		String delimiter="------------------------------------------";
		output= this.buildWSOutput();
		return output;
	}
	
	public String buildWSOutput(){
		boolean isEmpty=false;
		String output="";

		if(city.length()==0 && state.length()== 0 && zip.length()== 0){
			isEmpty=true;
		}
		
		if(!isEmpty){
			output+= city + ", "+state;
		}
		else{
			output="None";
		}
		return output;
	} 
	
}
