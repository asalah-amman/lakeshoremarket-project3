package com.address.dal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
 
import com.address.*;
 
/**
 * 
 * @author Alla Salah
 * This Class is used to handle CRUD functionality for a Address object
 * The addCustomer method will result in adding records to the following tables:
 * 1. Customers
 * 2. Customer_Addresses
 * 3. Customer_Payments
 * 4. Customer_Phones
 */
public class AddressDao {
        
  private Connection connect = null;
  private Statement statement = null;
  private PreparedStatement preparedStatement = null;
  private ResultSet resultSet = null;
  private AddressValidator av;
  
  final private String host = "localhost:3306";
  final private String user = "root";
  final private String passwd = "";
  
  
  
  public AddressDao(){
	  this.av= new AddressValidator();
	  av.populateCities();
	  //av.populateZips();
	  av.populateStates();
	  av.populateCountries();
  }
  //----------------------------------------------------------------------------------------
  /**
   * Method is responsible for initializing a connection to the database
   * 
   * @return Connection object
   */
  public Connection initializeDB()throws Exception{
	  Connection dbConnection=null;
	 try{
		 Class.forName("com.mysql.jdbc.Driver");
		 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
	 } 
	 catch (SQLException e) {
      throw e;
     } 
	 finally {
      close();
    }
      
      return dbConnection;
  }//end method
  //----------------------------------------------------------------------------------------
  //----------------------------------------------------------------------------------------
  
  //Private method used by addAddress method
  private long insertAddress(Connection dbConnection, Address a,long customerId, AddressValidator av)throws Exception{
	  long newBillAddrId=-1;
	  try{
		  String insertSql="INSERT INTO customer_addresses(customer_id, address_type_id, street_address, city_id, zip,state_id,country_id,is_active)"+
				  "SELECT ?,?,?,?,?,?,?,?"+
				  " WHERE NOT EXISTS (SELECT 1 FROM customer_addresses WHERE customer_id=? AND street_address=?)";
					
	      String city= a.getCity();
	      String zip= a.getZip();
	      String state= a.getState();
	      String country= a.getCountry();
		 
	      preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
		  preparedStatement.setLong(1, customerId);
	      preparedStatement.setInt(2, 	 1);//
 	      preparedStatement.setString(3, a.getStreetAddress());
 	      preparedStatement.setLong(4, Long.valueOf(city));
 	      preparedStatement.setString(5, zip);
 	      preparedStatement.setLong(6, Long.valueOf(state));
 	      preparedStatement.setLong(7, av.getCountryId(country));
 	      preparedStatement.setInt(8, (a.isActive())? 1:0);
		  preparedStatement.setLong(9, customerId);
 	      preparedStatement.setString(10, a.getStreetAddress());
	      boolean r= preparedStatement.execute();

 	     ResultSet rs = preparedStatement.getGeneratedKeys();
	   	  if (rs.next()) {
	   		newBillAddrId=rs.getLong(1);
	   	  }
 
	  }
	  catch (Exception e) {
	      throw e;
	    } 
	  finally {
	      close();
	    }
	  
	  return newBillAddrId;
	  
  }
    

  	
  	//Private method used by _____ method
  	private boolean updateCustomerAddress(Connection dbConnection, Address a, long customerId)throws Exception{
  		boolean result=false;
  		try{
 	      preparedStatement = dbConnection.prepareStatement("UPDATE customer_addresses "
 	    		  										+"  SET address_type_id=1"//c.getBillingAddress().getTypeOfAddress()
 	    		  										+", street_address=?"
 	    		  										+", city_id=?"
 	    		  										+", zip=?"
 	    		  										+", state_id=?"
 	    		  										+", country_id=?"
 	    		  										+", is_active=?"
 	    		  										+" WHERE customer_id="+customerId
 	    		  										+" AND customer_address_id="+a.getAddressId()
 	    		  										);
 	      String city= a.getCity();
	      String zip= a.getZip();
	      String state= a.getState();
	      String country= a.getCountry();
	      
 	      preparedStatement.setString(1, a.getStreetAddress());
 	      preparedStatement.setLong(2, av.getCityId(city));
 	      preparedStatement.setString(3, a.getZip());
 	      preparedStatement.setLong(4, av.getStateId(state));
 	      preparedStatement.setLong(5, av.getCountryId(country));
 	      preparedStatement.setInt(6, (a.isActive())? 1:0);
 	      preparedStatement.executeUpdate();
 	      result=true;
 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return result;
  	}

  	//Private method used by the getCustomerAddresses method
  	private ArrayList<Address> getAddresses(Connection dbConnection, long customerId) throws Exception{
  		ArrayList<Address> addrList=null; 
  		ResultSet resultSet;
  		
  		try{
  	  		String queryStr="SELECT customer_address_id,address_type_id, street_address, city_id, zip, state_id, country_id, is_active"+
  	  				" FROM customer_addresses WHERE customer_id= ?";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
 			preparedStatement.setLong(1, customerId);
  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	
  	        	 addrList = new ArrayList<Address>();
  	        	 resultSet= preparedStatement.getResultSet();
  	        	while(resultSet.next()){
  	  	        	 Address newAddr= new Address();

  	  	        	 
  	  	        	long cId= resultSet.getLong(1);
  	  	        	int addrType= resultSet.getInt(2); 			//address_type_id
  	  	        	String steetName= resultSet.getString(3); 	//street_address
  	  	        	long cityId= resultSet.getLong(4); 			//city_id
  	  	        	String zip= resultSet.getString(5);			//zip
  	  	        	long stateId =resultSet.getLong(6); 		//state_id
  	  	        	long countryId= resultSet.getLong(7); 		//country_id
  	  	        	int activeStatus= resultSet.getInt(8);		//is_active
  	  	        	
  	    		

  	    			newAddr.setAddressId(cId);
  	    			newAddr.setTypeOfAddress((addrType==1)? AddressType.Billing_Address:AddressType.Shipping_Address);
  	    			newAddr.setStreetAddress(steetName);
  	  	            newAddr.setCity(av.getCityName(cityId));
  	    			//System.out.println("Checkpoint #2 ");

  	  	            newAddr.setState(av.getStateName(stateId));
  	    			//System.out.println("Checkpoint #3 ");

  	  	            newAddr.setCountry(av.getCountryName(countryId));
  	    			//System.out.println("Checkpoint #4 ");

  	  	            newAddr.setActive((activeStatus==1)? true:false);
  	  	            
  	  	            addrList.add(newAddr);
  	  	        }//end while
  	        }//end if
  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
        return addrList;
  	}
  	
  	//Private method used by ___ method
  	private ArrayList<Address> getAllCustomerAddresses(Connection dbConnection) throws Exception{
  		ArrayList<Address> addrList=null; 

  		try{
  	  		String queryStr="SELECT customer_address_id,address_type_id, street_address, city_id, zip, state_id, country_id, is_active"+
  	  				" FROM customer_addresses ORDER BY customer_id ";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  			//System.out.println("Inside the AddressDao.getAllCustomerAddresses, CHECKPOINT #1");

  	        boolean r = preparedStatement.execute();
  			//System.out.println("Inside the AddressDao.getAllCustomerAddresses, CHECKPOINT #2");

  	        if(r){
  	        	
  	        	 addrList = new ArrayList<Address>();
  	        	 resultSet= preparedStatement.getResultSet();
  	        	while(resultSet.next()){
  	        		Address newAddr= new Address();
  	        		
  	  	        	long cId= resultSet.getLong(1);
  	  	        	int addrType= resultSet.getInt(2); 			//address_type_id
  	  	        	String steetName= resultSet.getString(3); 	//street_address
  	  	        	long cityId= resultSet.getLong(4); 			//city_id
  	  	        	String zip= resultSet.getString(5);			//zip
  	  	        	long stateId =resultSet.getLong(6); 		//state_id
  	  	        	long countryId= resultSet.getLong(7); 		//country_id
  	  	        	int activeStatus= resultSet.getInt(8);		//is_active
  	  	        	

  	    			newAddr.setAddressId(cId);
  	    			newAddr.setTypeOfAddress((addrType==1)? AddressType.Billing_Address:AddressType.Shipping_Address);
  	    			newAddr.setStreetAddress(steetName);
  	  	            newAddr.setCity(av.getCityName(cityId));
  	  	            newAddr.setState(av.getStateName(stateId));
  	  	            newAddr.setCountry(av.getCountryName(countryId));

  	  	            boolean addActStatus;
  	  	            if(activeStatus > 0)
  	  	            	addActStatus=true;
  	  	            else
  	  	            	addActStatus=false;
  	  	            
  	  	            newAddr.setActive(addActStatus);


  	  	            addrList.add(newAddr);
  	  	        }//end while
  	        }//end if
  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		} 
			

  		return addrList;
  	}
  

	//Private method used by ___ method
	private boolean deactivateAddress(Connection dbConnection, long customerId)throws Exception{
		boolean result=false;
		try{
			preparedStatement= dbConnection.prepareStatement("UPDATE customer_addresses"+
															" SET is_active=0"+
															" WHERE  customer_id=?");
			preparedStatement.setLong(1, customerId);
			preparedStatement.executeUpdate();

			result=true;
		}
		  catch (Exception e) {
	 	      throw e;
	 	    } 
	 	  finally {
	 	      close();
	 	    }
		
		return result;
	}
	/*
	  *********************************************************************************************
	  *********************************************************************************************
	  										PUBLIC METHODS 
	  *********************************************************************************************
	  *********************************************************************************************/
	 /**
     * 
     * @param ca
     * @param customerId
     * @return
     * @throws Exception
     */
    	public long addAddress(Address ca, long customerId) throws Exception{
    		Connection insertCon=null;
    		long addressId=-1;
    		try{
    	  		insertCon= this.initializeDB();
    	  		addressId=this.insertAddress(insertCon, ca,customerId, av);
    	  		
    		}
    		catch(Exception e){
    			throw e;
    		}
    		finally{
    			close();
    		}
    		
    		return addressId;
    	}
 
    /**
     * 
     * @param cid
     * @return
     */
  	public ArrayList<Address> getCustomerAddresses(long customerId ){
  		ArrayList <Address> addrList=null;
  		Connection getConnection=null;
  		try {
  			getConnection= this.initializeDB();

  			addrList= new ArrayList<Address>();
  			addrList= this.getAddresses(getConnection,customerId);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return addrList;
  	}
  	
  	/**
  	 * 
  	 * @return
  	 */
  	public ArrayList<Address> getAllAddresses(){
  		ArrayList<Address> addrList= new ArrayList<Address>();
  		Connection getConnection=null;
  		try {
  			getConnection= this.initializeDB();

			addrList= this.getAllCustomerAddresses(getConnection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return addrList;
  	}
  	
  	/**
  	 * 
  	 * @param cid
  	 * @param addrId
  	 * @return
  	 */
  	public boolean deleteCustomerAddress(  long addrId){
  		Connection deleteConnection=null;
  		boolean deleteResult=false;
  		
  		try{
  			deleteConnection= this.initializeDB();
  			deleteResult= this.deactivateAddress(deleteConnection,  addrId);
  		}
  		catch (Exception e){
  			e.printStackTrace();
  		}
  		
  		return deleteResult;
  	}

  	/**
  	 * 
  	 * @param customerId
  	 * @param updatedAddress
  	 * @return
  	 * @throws Exception
  	 */
	public boolean updateCustomerAddress (long customerId, Address updatedAddress)throws Exception{
  		Connection updateCon=null;
  		boolean updateResult=false;
  		try{
  			updateCon= this.initializeDB();
  			updateResult= this.updateCustomerAddress(updateCon, updatedAddress, customerId);
  		}
  		catch(Exception e){
  			e.printStackTrace();
  		}
  		
  		return updateResult;
  		
	}
	
	
  // You need to close the resultSet
  private void close() {
    try {
      if (resultSet != null) {
        resultSet.close();
      }

      if (statement != null) {
        statement.close();
      }

      if (connect != null) {
        connect.close();
      }
    } catch (Exception e) {

    }
  }

}