package com.address;

public class Location {
	
	//Class Attributes
	private long locationId;
	private String locationName;
	private String locationCode;
	
	
	public Location (long id, String name, String code){
		this.locationId=id;
		this.locationName=name;
		this.locationCode=code;
	}


	
	//Methods
	public long getLocationId() {
		return locationId;
	}

	public String getLocationName() {
		return locationName;
	}
	
	public String getLocationCode() {
		return locationCode;
	}


	
	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	
	
	
}
