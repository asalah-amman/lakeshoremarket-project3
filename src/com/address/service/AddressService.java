package com.address.service;

import java.util.ArrayList;

import com.address.service.representation.AddressRepresentation;
import com.address.service.representation.AddressRequest;
 

public interface AddressService {
	public ArrayList<AddressRepresentation> getAddresses();
	public ArrayList<AddressRepresentation> getAddress(String customerId);
	public AddressRepresentation createAddress(AddressRequest cusAddrRequest);
 
}

