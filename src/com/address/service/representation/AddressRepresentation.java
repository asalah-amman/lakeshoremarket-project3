package com.address.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Address")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class AddressRepresentation {

	private String addrId;
	private String cusId;
	private String street;
	private String aptNum;
	private String city;
	private String zip;
	private String state;
	private String country;
	private String isActive;

	public AddressRepresentation() {}

	public AddressRepresentation(String addrId, String street, String aptNumber, String city, String zip, String state, String country){
		this.addrId=addrId;
		this.street=street;
		this.aptNum=aptNumber;
		this.city=city;
		this.zip=zip;
		this.state=state;
		this.country=country;
	}

	//Methods
	public String getAddrId() {
		return addrId;
	}

	public String getCustomerId(){
		return cusId;
	}
	
	public String getStreet() {
		return street;
	}

	public String getAptNumber(){
		return aptNum;
	}
	
	public String getCity() {
		return city;
	}

	public String getZip() {
		return zip;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public String getActiveStatus(){
		return isActive;
	}
	
	
	
	public void setAddrId(String addrId) {
		this.addrId = addrId;
	}

	public void setCustomerId(String cid){
		this.cusId=cid;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setAptNumber(String aptNumber){
		this.aptNum=aptNumber;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public void setState(String state) {
		this.state = state;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public void setActiveStatus(String newStatus){
		this.isActive=newStatus;
	}
}
