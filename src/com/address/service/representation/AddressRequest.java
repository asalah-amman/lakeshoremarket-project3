package com.address.service.representation;

import com.address.Address;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class AddressRequest {
	
	//Class Attributes
	private String customerID;
	//private String customerfName;
	//private String customerlName;
	private String addressId;
	private String addressTypeID;
	private String streetAddress;
	private String aptNum;
	private String city;
	private	String zip;
	private String state;
	private String country;
	private String isActive;
	
	public AddressRequest(){}
	
	public AddressRequest (String cusId, String addrId, String addrType, Address newAddr){
		this.customerID=cusId;
		//this.customerfName=cusFName;
		//this.customerlName=cusLName;
		this.addressId=addrId;
		this.addressTypeID=addrType;
		this.streetAddress= newAddr.getStreetAddress();
		this.city=newAddr.getCity();
		this.zip=newAddr.getZip();
		this.state=newAddr.getState();
		this.country=newAddr.getCountry();
	}
	//Methods
	public String getAddressId(){
		return addressId;
	}
	
	public String getCustomerID() {
		return customerID;
	}
	
	public String getAddressTypeID() {
		return addressTypeID;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	
	public String getAptNumber(){
		return aptNum;
	}
	
	public String getCity() {
		return city;
	}
	public String getZip() {
		return zip;
	}
	public String getState() {
		return state;
	}
	public String getCountry() {
		return country;
	}
	public String getIsActive() {
		return isActive;
	}


	
	//Setters
	public void setAddressId(String addressId){
		this.addressId=addressId;
	}
	
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public void setAddressTypeID(String addressTypeID) {
		this.addressTypeID = addressTypeID;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public void setApartmentNumber(String aptNumber){
		this.aptNum=aptNumber;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	
	
}
