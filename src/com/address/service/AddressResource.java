package com.address.service;

import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.CacheControl;

import com.address.service.representation.AddressRepresentation;
import com.address.service.representation.AddressRequest;
import com.address.service.workflow.AddressActivity;

@Path ("/address-service/")
public class AddressResource implements AddressService{


	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/address")
	//@Cacheable(cc="public, maxAge=3600") example for caching
	public ArrayList<AddressRepresentation> getAddresses() {
		System.out.println("GET METHOD Request for all customer addresses .............");
		AddressActivity addrActivity = new AddressActivity();
		return addrActivity.getCustomerAddresses();
	}
	

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/address/{customerId}")
	public ArrayList<AddressRepresentation> getAddress(@PathParam("customerId") String cid) {
		System.out.println("GET METHOD Request from Client with customerAddressRequest String ............." + cid);
		AddressActivity addrActivity = new AddressActivity();
		return addrActivity.getCustomerAddress(cid);
	}
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/address")
	public AddressRepresentation createAddress(AddressRequest  cusAddrRequest) {
		System.out.println("POST METHOD Request from Client with city/state............." + cusAddrRequest.getCity()+", "+cusAddrRequest.getState());
		AddressActivity addrActivity = new AddressActivity();
		return addrActivity.createAddress(cusAddrRequest);
	}
	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/address/{addrId}")
	public Response deleteAddress(@PathParam("addrId") String addrId) {
		System.out.println("Delete METHOD Request from Client with customerRequest address ID ............." + addrId);
		AddressActivity addrActivity = new AddressActivity();
		String res = addrActivity.deleteAddress(addrId);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}
	
	 
	
}
