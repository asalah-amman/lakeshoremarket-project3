package com.address.service;
import java.util.ArrayList;

import com.address.Address;
import com.address.AddressType;
/**
 * @author Alla Salah
 */
import com.address.dal.AddressDao;
 

public class AddressManager {
		private AddressDao addrDao= new AddressDao();
		
		
		//Methods
		public ArrayList<Address> getAllAddresses(){
			//System.out.println("AddressManager: getAllAddresses() Complete");
			return addrDao.getAllAddresses();
			
		}
		
		public ArrayList<Address> getCustomerAddresses( long customerId){
			//System.out.println("AddressManager: getCustomerAddresses() Complete");
			return addrDao.getCustomerAddresses(customerId );
			
		}
		
		public Address addCustomerAddress(String customerId, String streetAddr, String aprtNum, String city, String state, String zip, String country, AddressType atype ) {
			long newAddrId=-1;
			Address newCustomerAddress= new Address();
			
			newCustomerAddress.setStreetAddress(streetAddr);
			newCustomerAddress.setApartmentUnitNum(aprtNum);
			newCustomerAddress.setCity(city);
			newCustomerAddress.setState(state);
			newCustomerAddress.setCountry(country);
			newCustomerAddress.setZip(zip);
			newCustomerAddress.setTypeOfAddress(atype);
			newCustomerAddress.setActive(true);
			System.out.println("AddressManager.addCustomerAddress, city= "+city + "state= "+state );
			try {
				newAddrId = addrDao.addAddress(newCustomerAddress, Long.valueOf(customerId));
			} catch (Exception e) {
				e.printStackTrace();
			}
 
			if(newAddrId > 0){
				newCustomerAddress.setAddressId(newAddrId);
			}
			return newCustomerAddress;
		}
		
		public boolean updateCustomerAddress(long customerId, String steetAddr, String aprtNum, String city, String state, String zip, String country, AddressType atype){
			boolean updateResult=false;
			Address updatedCustomerAddress= new Address();
			
			updatedCustomerAddress.setStreetAddress(steetAddr);
			updatedCustomerAddress.setApartmentUnitNum(aprtNum);
			updatedCustomerAddress.setCity(city);
			updatedCustomerAddress.setState(state);
			updatedCustomerAddress.setCountry(country);
			updatedCustomerAddress.setZip(zip);
			updatedCustomerAddress.setTypeOfAddress(atype);
			updatedCustomerAddress.setActive(true);
			
			try {
				updateResult = addrDao.updateCustomerAddress(customerId, updatedCustomerAddress);
			} catch (Exception e) {
				e.printStackTrace();
			}
 
			return updateResult;
		}
		
		 public boolean deleteCustomerAddress( long cAddressId){
			 boolean deleteResult= false;
			 
			 try{
				 deleteResult= addrDao.deleteCustomerAddress( cAddressId);
			 }
			 catch (Exception e){
				 e.printStackTrace();
			 }
			 return deleteResult;
		 }
		
		
		 
	
	
}
