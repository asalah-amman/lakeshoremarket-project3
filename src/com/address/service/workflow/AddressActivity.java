package com.address.service.workflow;

import java.util.ArrayList;
import java.util.Iterator;

import com.address.service.AddressManager;
import com.address.service.representation.AddressRepresentation;
import com.address.service.representation.AddressRequest;
import com.customer.Customer;
import com.customer.service.representation.CustomerRepresentation;
import com.address.Address;
import com.address.AddressType;

public class AddressActivity {
	
	AddressManager addrManager= new AddressManager();
	
	
	
	//Methods
	public ArrayList<AddressRepresentation> getCustomerAddresses() {
			
			ArrayList<Address> customerAddressList = new ArrayList<Address>();
			ArrayList<AddressRepresentation> customerAddrRepresentations = new ArrayList<AddressRepresentation>();
			customerAddressList = addrManager.getAllAddresses();
			System.out.println(customerAddressList.size() +" Addresses found");
			Iterator<Address> it = customerAddressList.iterator();
			while(it.hasNext()) {
	          Address addr = (Address)it.next();
	          AddressRepresentation caddrRep = new AddressRepresentation();
	          caddrRep.setAddrId(String.valueOf(addr.getAddressId()));
	          caddrRep.setStreet(addr.getStreetAddress());
	          caddrRep.setCity(addr.getCity());
	          caddrRep.setZip(caddrRep.getZip());
	          caddrRep.setState(addr.getState());
	          caddrRep.setCountry(addr.getCountry());

	          //now add this representation in the list
	          customerAddrRepresentations.add(caddrRep);
	        }
			return customerAddrRepresentations;
		}
	
	public ArrayList<AddressRepresentation> getCustomerAddress(String cid) {
		
		ArrayList<Address> customerAddressList = new ArrayList<Address>();
		ArrayList<AddressRepresentation> customerAddrRepresentations = new ArrayList<AddressRepresentation>();
		customerAddressList = addrManager.getCustomerAddresses(Long.valueOf(cid));
		
		System.out.println(customerAddressList.size() +" Addresses found");
		Iterator<Address> it = customerAddressList.iterator();
		while(it.hasNext()) {
          Address addr = (Address)it.next();
          AddressRepresentation caddrRep = new AddressRepresentation();
          caddrRep.setAddrId(String.valueOf(addr.getAddressId()));
          caddrRep.setStreet(addr.getStreetAddress());
          caddrRep.setCity(addr.getCity());
          caddrRep.setZip(caddrRep.getZip());
          caddrRep.setState(addr.getState());
          caddrRep.setCountry(addr.getCountry());

          //now add this representation in the list
          customerAddrRepresentations.add(caddrRep);
        }
		return customerAddrRepresentations;
	}
	
	public AddressRepresentation createAddress(AddressRequest cusAddReq) {
		String addrId= cusAddReq.getAddressId();
		String street= cusAddReq.getStreetAddress();
		String aptNum= cusAddReq.getAptNumber();
		String city= cusAddReq.getCity();
		String zip= cusAddReq.getZip();
		String state= cusAddReq.getState();
		String country= cusAddReq.getCountry();
		String customerId= cusAddReq.getCustomerID();
		AddressType aType= (cusAddReq.getAddressTypeID().equals("1")? AddressType.Billing_Address:AddressType.Shipping_Address);
		
		Address newAddress = addrManager.addCustomerAddress(customerId, street, aptNum, city, state, zip, country, aType);

		
		AddressRepresentation addrRep = new AddressRepresentation();
		addrRep.setAddrId(String.valueOf(newAddress.getAddressId()));
		addrRep.setStreet(newAddress.getStreetAddress());
		addrRep.setAptNumber(newAddress.getApartmentUnitNum());
		addrRep.setCity(newAddress.getCity());
		addrRep.setState(newAddress.getState());
		addrRep.setZip(zip);
		addrRep.setCountry(newAddress.getCountry());
		addrRep.setActiveStatus((newAddress.isActive())? "Active":"Not Active");
		//System.out.println("Completed the createCustomer in CustomerActivity class");
		return addrRep;
	}
	
	public String deleteAddress (String addrId) {		
		addrManager.deleteCustomerAddress(Long.valueOf(addrId));
		return "OK";
	}
	
	//-------------------------------------
	// HELPER METHOD to build a string
	//-------------------------------------
	public String buildString(ArrayList<?> ar){
		String newStr="";
		for(int i=0; i < ar.size(); i++){
			if(i==0)
				newStr += ar.get(i).toString();
			else 
				newStr += " ; "+ar.get(i).toString();
		}
		return newStr;
	}
	

}
