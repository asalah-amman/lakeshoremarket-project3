package com.customer.service;

import java.util.ArrayList;

import com.customer.service.representation.CustomerRepresentation;
import com.customer.service.representation.CustomerRequest;


public interface CustomerService {
	public ArrayList<CustomerRepresentation> getCustomers();
	public CustomerRepresentation getCustomer(String customerId);
	public CustomerRepresentation createCustomer(CustomerRequest customerRequest);
    //public Response updateEmployee(EmployeeRequest employeeRequest);
    //public Response deleteEmployee(String employeeId);
	
	
}
