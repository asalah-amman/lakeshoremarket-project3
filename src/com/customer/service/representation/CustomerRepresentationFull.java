package com.customer.service.representation;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CustomerRepresentationFull {

	private CustomerRepresentation simpleCusRep;
	private String billingAddress;
	private String shippingAddresses;
	private String phones;
	private String payments;
	
//	public CustomerRepresentationFull() {
//		shippingAddresses= new String();
//		phones= new String();
//		payments= new String();
//	}
	public CustomerRepresentationFull(){}

	//Methods
	public CustomerRepresentation getCustomerRepresentation(){
		return simpleCusRep;
	}
	public String getBillingAddress() {
		return billingAddress;
	}
	public String getShippingAddresses() {
		return shippingAddresses;
	}
	public String getPhones() {
		return phones;
	}
	public String getPayments(){
		return payments;
	}
	
	
	public void setCustomerRepresentation(CustomerRepresentation cRep){
		this.simpleCusRep=cRep;
	}
	
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public void setShippingAddresses(String shippingAddresses) {
		this.shippingAddresses = shippingAddresses;
	}

	public void setPhones(String phones) {
		this.phones = phones;
	}

	public void setPayments(String payments) {
		this.payments = payments;
	}

}
