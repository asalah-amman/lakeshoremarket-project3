package com.customer.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.product.service.workflow.Link;

@XmlRootElement(name = "Customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CustomerRepresentation {

	private String cid;
	private String lastName;
	private String firstName;
	private String email;
	private String status;
	private String passcode;
	private Link nextState;

	public CustomerRepresentation() {}

	public CustomerRepresentation(String cusId, String lName, String fName, String email, boolean cusStatus){
		this.cid=cusId;
		this.lastName=lName;
		this.firstName=fName;
		this.email=email;
		nextState= new Link();
		String statusStr="";
		
		if(cusStatus)
			statusStr="Active";
		else
			statusStr="Not Active";
		this.status= statusStr;
	}


	//Methods
	public String getPasscode() {
		return passcode;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public Link getNextState() {
		return nextState;
	}

	public void setNextState(Link newNextState) {
		this.nextState = newNextState;
	}

	public String getCustomerId() {
		return cid;
	}

	public String getLastName() {
		return lastName;
	}
 
	public String getEmail() {
		return email;
	}
 
	public String getStatus(){
		return status;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setCustomerId(String cid) {
		this.cid = cid;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public void setStatus(boolean newStatus){
		String statusStr="";
			
		if(newStatus)
			statusStr="Active";
		else
			statusStr="Not Active";
		this.status= statusStr;
	}
	
}
