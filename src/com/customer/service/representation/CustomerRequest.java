package com.customer.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class CustomerRequest {
	
	private String firstName;
	private String lastName;
	private String email;
	private String passcode;
	private String status;
	
	//Methods
	public String getFirstName() {
		return firstName;
	}
	
	public String getPasscode() {
		return passcode;
	}


	public String getLastName() {
		return lastName;
	}

	public String getEmail(){
		return email;
	}
	
	public String getStatus(){
		return status;
	}
	
	
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}
	
	public void setEmail(String email){
		this.email=email;
	}
	
	public void setStatus(boolean newStatus){
		this.status= (newStatus)? "Active":"Not Active";
	}

}
