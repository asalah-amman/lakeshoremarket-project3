package com.customer.service;

import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.CacheControl;

import com.customer.service.representation.CustomerRepresentation;
import com.customer.service.representation.CustomerRequest;
import com.customer.service.representation.CustomerRepresentationFull;
import com.customer.service.workflow.CustomerActivity;


@Path ("/customerservice/")
public class CustomerResource implements CustomerService{

	//**************************************************************************************************************
	//*************************************************************************************************************
	//  											CustomerSimple 
	//*************************************************************************************************************
	//*************************************************************************************************************	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/customers")
	//@Cacheable(cc="public, maxAge=3600") example for caching
	public ArrayList<CustomerRepresentation> getCustomers() {
		System.out.println("GET METHOD Request for all customers .............");
		CustomerActivity customerActivity = new CustomerActivity();
		return customerActivity.getCustomers();	
	}
	

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/customers/{customerId}")
	public CustomerRepresentation getCustomer(@PathParam("customerId") String cid) {
		System.out.println("GET METHOD Request from Client with customerRequest String ............." + cid);
		CustomerActivity customerActivity = new CustomerActivity();
		return customerActivity.getCustomer(cid);
	}
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/customers")
	public CustomerRepresentation createCustomer(CustomerRequest  customerRequest) {
		System.out.println("POST METHOD Request from Client with ............." + customerRequest.getFirstName() + "  " + customerRequest.getLastName());
		CustomerActivity customerActivity = new CustomerActivity();
		return customerActivity.createCustomer(customerRequest.getFirstName(), customerRequest.getLastName(), customerRequest.getEmail(), customerRequest.getPasscode());
	}

	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/customers/{customerId}")
	public Response deleteCustomer(@PathParam("customerId") String cid) {
		System.out.println("Delete METHOD Request from Client with customerRequest String ............." + cid);
		CustomerActivity customerActivity = new CustomerActivity();
		String res = customerActivity.deleteCustomer(cid);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}
	
	//**************************************************************************************************************
	//*************************************************************************************************************
	// 													CustomerFull
	//*************************************************************************************************************
	//*************************************************************************************************************
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/customers-full")
	//@Cacheable(cc="public, maxAge=3600") example for caching
	public ArrayList<CustomerRepresentationFull> getCustomersFull() {
		System.out.println("GET METHOD Request for all customers FULL .............");
		CustomerActivity customerActivity = new CustomerActivity();
		return customerActivity.getCustomersFull();
	}	
	
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/customer-full/{customerId}")
	public CustomerRepresentationFull getCustomerFull(@PathParam("customerId") String cid) {
		System.out.println("GET METHOD Request from Client with customerRequest String ............." + cid);
		CustomerActivity customerActivity = new CustomerActivity();
		return customerActivity.getCustomerFull(cid);
	}
	
}
