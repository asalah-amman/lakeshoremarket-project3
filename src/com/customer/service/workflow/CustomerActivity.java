package com.customer.service.workflow;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.phone.*;
import com.address.Address;
import com.address.service.AddressManager;
import com.customer.Customer;
import com.customer.CustomerManager;
import com.customer.dal.CustomerDao;
import com.customer.service.representation.CustomerRepresentation;
import com.customer.service.representation.CustomerRepresentationFull;
import com.payment.Payment;
import com.payment.PaymentManager;
import com.phone.PhoneManager;
import com.product.service.workflow.Link;

/**
 * 
 * @author Alla Salah
 * Purpose: To provide a set of methods that can be used within the CustomerResource class
 * Available Methods: 
 * getCustomers(): ArrayList<CustomerRepresentation>
 * getCustomer(String customerId):CustomerRepresentation
 * createCustomer(String fName, String lName, String email):CustomerRepresentation
 */
public class CustomerActivity {

private static CustomerManager cMngr = new CustomerManager();
private static AddressManager addrMngr= new AddressManager();
private static PhoneManager phoneMngr= new PhoneManager();
private static PaymentManager pmtMngr= new PaymentManager();

	/**
	 * 
	 * @return An ArrayList of CustomerRepresentation objects 
	 */
	/*
	       This methods uses the CustomerManager.getAllCustomers() which then converts the resulting set by
	       
	 */
	public ArrayList<CustomerRepresentation> getCustomers() {
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		ArrayList<CustomerRepresentation> customerRepresentations = new ArrayList<CustomerRepresentation>();
		customers = cMngr.getAllCustomers();
		
		Iterator<Customer> it = customers.iterator();
		while(it.hasNext()) {
          Customer cus = (Customer)it.next();
          CustomerRepresentation customerRepresentation = new CustomerRepresentation();
          customerRepresentation.setCustomerId(String.valueOf(cus.getCustomerId()));
          customerRepresentation.setFirstName(cus.getFirstName());
          customerRepresentation.setLastName(cus.getLastName());
          customerRepresentation.setEmail(cus.getEmail());
          customerRepresentation.setStatus(cus.isActive());
          //now add this representation in the list
          customerRepresentations.add(customerRepresentation);
        }
		return customerRepresentations;
	}
	
	public ArrayList<CustomerRepresentationFull> getCustomersFull() {
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		ArrayList<Address> addresses = new ArrayList<Address>();
		ArrayList<Phone> phones = new ArrayList<Phone>();
		ArrayList<Payment> payments = new ArrayList<Payment>();
		ArrayList<CustomerRepresentationFull> cusRepFullList = new ArrayList<CustomerRepresentationFull>();
		customers = cMngr.getAllCustomers();

		Iterator<Customer> it = customers.iterator();
		while(it.hasNext()) {
          Customer cus = (Customer)it.next();

          CustomerRepresentationFull cusRepFull = new CustomerRepresentationFull();
         
          //--------------------------------------------------------------------------------
          // Collecting various customer information to build customer representation full list
          //--------------------------------------------------------------------------------
          String cusFName, cusLName, cusId,cusEmail;
          boolean isActive=false;
          cusFName= cus.getFirstName();
          cusLName= cus.getLastName();
          cusId= String.valueOf(cus.getCustomerId());
          cusEmail= cus.getEmail();
          isActive=cus.isActive();
          CustomerRepresentation cusRep= new CustomerRepresentation(cusFName, cusLName, cusId, cusEmail,isActive);
          cusRepFull.setCustomerRepresentation(cusRep);
          
          String  billingAddr= cus.getBillingAddress().toString();
          
          addresses= cus.getShippingAddresses();
          String shippingAddresses= buildString(addresses);
          
          phones= cus.getPhoneNumbers();
          String phoneList= buildString(phones);

          payments= cus.getPaymentCards();
          String pmtList= buildString(payments);

          cusRepFull.setBillingAddress(billingAddr);
          cusRepFull.setShippingAddresses(shippingAddresses);
          cusRepFull.setPhones(phoneList);
          cusRepFull.setPayments(pmtList);

          cusRepFullList.add(cusRepFull);
        }
		return cusRepFullList;
	}
	
	public CustomerRepresentation getCustomer(String id) {
		
		Customer cus = cMngr.getCustomer(id);
		//System.out.println("CustomerID= "+cus.getCustomerId());
		
		CustomerRepresentation cusRep = new CustomerRepresentation();
		cusRep.setFirstName(cus.getFirstName());
		cusRep.setLastName(cus.getLastName());
		cusRep.setEmail(cus.getEmail());
		cusRep.setStatus(cus.isActive());
		cusRep.setCustomerId(String.valueOf(cus.getCustomerId()));
	
		return cusRep;
	}
	
	public CustomerRepresentationFull getCustomerFull(String id) {
		Customer cus = cMngr.getCustomer(id);
		CustomerRepresentationFull cusRepFull = new CustomerRepresentationFull();
		
		CustomerRepresentation cRep=  new CustomerRepresentation();
		cRep.setFirstName(cus.getFirstName());
		cRep.setLastName(cus.getLastName());
		cRep.setEmail(cus.getEmail());
		cRep.setCustomerId(String.valueOf(cus.getCustomerId()));
		
		cusRepFull.setCustomerRepresentation(cRep);
		cusRepFull.setBillingAddress(cus.getBillingAddress().buildWSOutput());
		
		String shipAddresses= this.buildString(cus.getShippingAddresses());
		cusRepFull.setShippingAddresses(shipAddresses);
		
		String phoneList= this.buildString(cus.getPhoneNumbers());
		cusRepFull.setPhones(phoneList);
		
		String pmtList= this.buildString(cus.getPaymentCards());
		cusRepFull.setPayments(pmtList);
	
		return cusRepFull;
	}
	
	public CustomerRepresentation createCustomer(String firstName, String lastName, String email, String password) {
		System.out.println("Start: Customer Activity Create Customer -------");

		Customer cus = cMngr.addCustomer(firstName, lastName, email, password);
		
		CustomerRepresentation cusRep = new CustomerRepresentation();
		cusRep.setFirstName(cus.getFirstName());
		cusRep.setLastName(cus.getLastName());
		cusRep.setEmail(email);
		cusRep.setPasscode(password);
		cusRep.setCustomerId(String.valueOf(cus.getCustomerId()));

		 //cusRep.setSearchProductsLink("http://localhost:8081/productservice/products");
		 
		 cusRep.setNextState(this.setLinks(cusRep,"Buy"));
		//System.out.println("Completed the createCustomer in CustomerActivity class");
		
		return cusRep;
	}
	
	private Link setLinks( CustomerRepresentation cusRep, String state) {
		// Set up the activities that can be performed on orders
		Link newLink= new Link();
		if ("View".equals(state)) {
			String url = "http://localhost:8081/productservice/products";
			newLink.setAction("view");  
			newLink.setUrl(url);
		}
		
		if ("Buy".equals(state)) {
			String url = "http://localhost:8081/customerservice/customer/"+ cusRep.getCustomerId();
			Link buy = new Link("buy", url);
			newLink.setAction("buy");  
			newLink.setUrl(url);
		
		}
		return newLink;
	}
	
	public String deleteCustomer(String cid) {
		
		cMngr.deleteCustomer(cid);;
		
		return "OK";
	}
	
	
	//-------------------------------------
	// HELPER METHOD to build a string
	//-------------------------------------
	public String buildString(ArrayList<?> ar){
		String newStr="";
		for(int i=0; i < ar.size(); i++){
			if(i==0)
				newStr += ar.get(i).toString();
			else 
				newStr += " ; "+ar.get(i).toString();
		}
		return newStr;
	}
	
	//---------------------------------------------
	// HELPER METHOD to build a string array list
	//---------------------------------------------
	public ArrayList<String> buildStringArrayList(ArrayList<?> ar){
		ArrayList<String> strArrList= new ArrayList<String>();
		
		for(int i=0; i < ar.size(); i++){
			strArrList.add(ar.get(i).toString());
		}
		return strArrList;
	}

}
