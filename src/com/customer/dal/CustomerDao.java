package com.customer.dal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;

import com.phone.*;
import com.phone.dal.PhoneDao;
import com.address.*;
import com.address.dal.AddressDao;
import com.payment.*;
import com.payment.dal.PaymentDao;
import com.customer.*;
/**
 * 
 * @author Alla Salah
 * This Class is used to handle CRUD functionality for a Customer object
 * The addCustomer method will result in adding records to the following tables:
 * 1. Customers
 * 2. Customer_Addresses
 * 3. Customer_Payments
 * 4. Customer_Phones
 */
public class CustomerDao {

  final private String host = "localhost:3306";
  final private String user = "root";
  final private String passwd = "";

  
  private Connection connect = null;
  private Statement statement = null;
  private PreparedStatement preparedStatement = null;
  private ResultSet resultSet = null;
  private AddressValidator av;
  private AddressDao addrDao;
  private PhoneDao phoneDao;
  private PaymentDao pmtDao;
  
  
  public CustomerDao(){
	  this.av= new AddressValidator();
	  av.populateCities();
	  //av.populateZips();
	  av.populateStates();
	  av.populateCountries();
	  
	  //Initialize DAOs
	  addrDao= new AddressDao();
	  phoneDao= new PhoneDao();
	  pmtDao= new PaymentDao();
  }
  //----------------------------------------------------------------------------------------
  /**
   * Method is responsible for initializing a connection to the database
   * 
   * @return Connection object
   */
  public Connection initializeDB()throws Exception{
	  Connection dbConnection=null;
	 try{
		 Class.forName("com.mysql.jdbc.Driver");
		 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
	 } 
	 catch (SQLException e) {
      throw e;
     } 
	 finally {
      close();
    }
      
      return dbConnection;
  }//end method
  //----------------------------------------------------------------------------------------
  //----------------------------------------------------------------------------------------
  

 //Private method used by the addCustomer() method
  private long insertCustomer(Connection dbConnection, Customer c)throws Exception{
	  long newCustomerId=-1;
	  try{
	      String insertSql="INSERT INTO customers(first_name, last_name,email, is_active, is_pending_approval,acct_activation_date)"+
	    		  			"SELECT ?, ?, ? , ?, ?,? "+
							"WHERE NOT EXISTS (SELECT 1 FROM customers WHERE first_name=? AND last_name=? AND email=?)";
	      
		  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
	      
	      preparedStatement.setString(1, c.getFirstName());
	      preparedStatement.setString(2, c.getLastName());
	      preparedStatement.setString(3, c.getEmail());
	      preparedStatement.setInt(4, (c.isActive())? 1:0);
	      preparedStatement.setInt(5, (c.isPendingApproval())? 1:0);
	      preparedStatement.setDate(6,new java.sql.Date(System.currentTimeMillis()));
	      preparedStatement.setString(7, c.getFirstName());
	      preparedStatement.setString(8, c.getLastName());	
	      preparedStatement.setString(9, c.getEmail());
	      //System.out.println("Customer table Insert query: "+ preparedStatement);
	      boolean r= preparedStatement.execute();
	      //System.out.println("Result alla: " + r);
    	  ResultSet rs = preparedStatement.getGeneratedKeys();
    	  if (rs.next()) {
    		  newCustomerId=rs.getLong(1);
    	  }
    	  //System.out.println("New Customer ID: "+newCustomerId);
	      
	      

	      if(newCustomerId > 0 ){
	    	  //System.out.println("Updating customer ID " );
	    	  c.setCustomerId(newCustomerId);
	      }
	  }
	  catch (Exception e) {
	      throw e;
	    } 
	  finally {
	      close();
	    }
	  
	  return newCustomerId;
	  
  }
  
  //Private method used by ____ method
  private long insertCustomer(Connection dbConnection, String fName, String lName, String email, String password)throws Exception{
	  long newCustomerId=-1;
	  try{
	      String insertSql="INSERT INTO customers(first_name, last_name, email, password, is_active, is_pending_approval)"+
	    		  			" SELECT ?, ?, ? ,?, ?, ? "+
							" WHERE NOT EXISTS (SELECT 1 FROM customers WHERE first_name=? AND last_name=? AND email=?)";

	      System.out.println("Inside the insertCustomer method "+ insertSql);
		  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
	      
	      preparedStatement.setString(1, fName);
	      preparedStatement.setString(2, lName);
	      preparedStatement.setString(3, email);
	      preparedStatement.setString(4, password);
	      preparedStatement.setInt(5, 1);
	      preparedStatement.setInt(6, 1);
	      preparedStatement.setString(7, fName);
	      preparedStatement.setString(8, lName);
	      preparedStatement.setString(9, email);
	      System.out.println("Customer table Insert query: "+ preparedStatement);

	      boolean r= preparedStatement.execute();
	      System.out.println("Result of insert in the method insertCustomer: " + r);
    	  ResultSet rs = preparedStatement.getGeneratedKeys();
    	  if (rs.next()) {
    		  newCustomerId=rs.getLong(1);
    	  }
    	  //System.out.println("New Customer ID: "+newCustomerId);
	  }
	  catch (Exception e) {
	      throw e;
	    } 
	  finally {
	      close();
	    }
	  
	  return newCustomerId;
	  
  }
  
//Private Method used by the updateCustomer() v1 method
	private boolean updateCustomerTable(Connection dbConnection, Customer c)throws Exception{
		boolean result=false;
		try{
		 // PreparedStatements can use variables and are more efficient
	      
			//System.out.println("customerId= "+ c.getCustomerId());
			preparedStatement = dbConnection.prepareStatement("UPDATE customers "
	    		  										+"SET first_name= ?"
	    		  										+" , last_name= ?"
	    		  										+" , is_active=? "
	    		  										+" , is_pending_approval=? "
	    		  										+" WHERE customer_id="+c.getCustomerId());
	      
	      preparedStatement.setString(1, c.getFirstName());
	      preparedStatement.setString(2, c.getLastName());
	      preparedStatement.setInt(3, 1);
	      preparedStatement.setInt(4, (c.isPendingApproval())? 1:0);
	      //System.out.println("SQL for Updating Customer Table: "+ preparedStatement);

	      preparedStatement.executeUpdate();
	      result=true;
	  }
	  catch (Exception e) {
	      throw e;
	    } 
	  finally {
	      close();
	    }
		
		return result;
	}
	
	//Private method used by the updateCustomer() v2  method
	private boolean updateCustomerTable(Connection updateCon, String cid,String fName, String lName, String email) throws Exception{
		
		boolean result=false;
		try{
		 // PreparedStatements can use variables and are more efficient
	      
			//System.out.println("customerId= "+ c.getCustomerId());
			preparedStatement = updateCon.prepareStatement("UPDATE customers "
	    		  										+"SET first_name= ?"
	    		  										+" , last_name= ?"
	    		  										+" , email=? "
	    		  										+" WHERE customer_id=?");
	      
	      preparedStatement.setString(1, fName);
	      preparedStatement.setString(2, lName);
	      preparedStatement.setString(3, email);
	      preparedStatement.setLong(4, Long.valueOf(cid));
	      //System.out.println("SQL for Updating Customer Table: "+ preparedStatement);

	      preparedStatement.executeUpdate();
	      result=true;
	  }
	  catch (Exception e) {
	      throw e;
	    } 
	  finally {
	      close();
	    }
		
		return result;
	}

	//Private method used by the getCustomer() method
	private ArrayList <Payment>  getPaymentCardInfo(Connection dbConnection,long customerId) throws Exception{
		ArrayList <Payment> pmtList= new ArrayList<Payment>();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getPaymentCardInfo, id= "+customerId);
  	  		String queryStr="SELECT payment_type_id,network_type_id, acct_name,account_number, acct_exp_date, security_code, is_default"+
  	  				" FROM customer_payment_types"+
  	  				" WHERE customer_id=?";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, customerId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 resultSet= preparedStatement.getResultSet();
  	        	
  	        	 while(resultSet.next()){
  	        		Payment newPayment= new Payment();
   	  	        	long pmtId= resultSet.getLong(1);				//payment_type_id
   	  	        	int nType= resultSet.getInt(2); 				//network_type_id
   	  	        	String cardName= resultSet.getString(3); 		//acct_name
   	  	        	String acctNum= resultSet.getString(4); 		//account_number
   	  	        	java.sql.Date expDate= resultSet.getDate(5); 			//acct_exp_date
   	  	        	int sCode= resultSet.getInt(6);					//security_code
   	  	        	int isDefault =resultSet.getInt(7); 			//is_default

   	  
   	  	        	
   	  	        	newPayment.setPaymentTypeId(pmtId);
   	  	        	newPayment.setPaymentNetwork(CardNetwork.AMERICAN_EXPRESS);
   	  	        	newPayment.setCardName(cardName);
   	  	        	newPayment.setCardNumber(acctNum);
   	  	        	newPayment.setCardExpiration(expDate);
   	  	        	newPayment.setSecurityCode(sCode);
   	  	        	newPayment.setDefault((isDefault==1)? true:false);
   	  	        	
   	  	        	pmtList.add(newPayment);
  	        	 }//end while
  	        	  
  	        }//end if
  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
       
        return pmtList;
  	}
	
	//Private method used by the getCustomer() method
	private ArrayList <Customer> getCustomers(Connection dbConnection) throws Exception{
  		Customer newCustomer;
  		ArrayList <Customer> customerList= new ArrayList<Customer>();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getCustomerInfo, id= "+customerId);
  	  		String queryStr="SELECT customer_id, first_name, last_name, email, is_active, acct_activation_date, is_pending_approval FROM customers ORDER BY customer_id";
 			preparedStatement = dbConnection.prepareStatement(queryStr );

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 resultSet= preparedStatement.getResultSet();
  	        	while(resultSet.next()){
  	        		newCustomer= new Customer();
  	  	        	long cId= resultSet.getLong(1);
  	  	        	String fName= resultSet.getString(2); 	//fName
  	  	        	String lName= resultSet.getString(3); 	//lName
  	  	        	String email= resultSet.getString(4); 	//email
  	  	        	int activeStatus= resultSet.getInt(5);	//is_active
  	  	        	Date aDate =resultSet.getDate(6); 		//acct_activation_date
  	  	        	int pendingStatus= resultSet.getInt(7); //is_pending_approval
  	  	        	
  	  	        	
  	  	        	boolean aStatus= (activeStatus > 0)? true:false;
  	  	        	boolean pStatus= (pendingStatus > 0)? true:false;
  	  	        
  	  	        	newCustomer.setCustomerId(cId);
  	  	        	newCustomer.setFirstName(fName);
  	  	        	newCustomer.setLastName(lName);
  	  	        	newCustomer.setEmail(email);
  	  	        	newCustomer.setActive(aStatus);
  	  	        	newCustomer.setAcctActivationDate(aDate);
  	  	        	newCustomer.setPendingApproval(pStatus);
  	  	        	//System.out.println("Customer: "+ newCustomer.getFirstName()+ ", status= "+newCustomer.isActive() +", activeStatus: "+newCustomer.isActive());
  	  	        	customerList.add(newCustomer);
  	  	        }
  	        	
  	        }//end if (r)

  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
       
        return customerList;
  	}
	
	//Private method used by the getCustomer() method
  	private Customer getCustomerInfo(Connection dbConnection,long customerId) throws Exception{
  		Customer matchingCustomer= new Customer();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getCustomerInfo, id= "+customerId);
  	  		String queryStr="SELECT customer_id, first_name, last_name, email, is_active, acct_activation_date, is_pending_approval"+
  	  				" FROM customers"+
  	  				" WHERE customer_id=?";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, customerId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 resultSet= preparedStatement.getResultSet();
  	        	if(resultSet.next()){
  	  	        	long cId= resultSet.getLong(1);
  	  	        	String fName= resultSet.getString(2); 	//fName
  	  	        	String lName= resultSet.getString(3); 	//lName
  	  	        	String email= resultSet.getString(4); 	//email
  	  	        	int activeStatus= resultSet.getInt(5);	//is_active
  	  	        	Date aDate =resultSet.getDate(6); 		//acct_activation_date
  	  	        	int pendingStatus= resultSet.getInt(7); //is_pending_approval
  	  	        	
  	  	        	matchingCustomer.setCustomerId(cId);
  	  	        	matchingCustomer.setFirstName(fName);
  	  	        	matchingCustomer.setLastName(lName);
  	  	        	matchingCustomer.setEmail(email);
  	  	        	matchingCustomer.setActive((activeStatus==1)? true:false);
  	  	        	matchingCustomer.setAcctActivationDate(aDate);
  	  	        	matchingCustomer.setActive((pendingStatus==1)? true:false);
  	  	        }
  	        }

  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
        return matchingCustomer;
  	}
  
  	//Private method used by the getCustomer() method
  	private Address getBillingAddressInfo(Connection dbConnection,long customerId) throws Exception{
  		Address billingAddr= new Address();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getBillingAddressInfo, id= "+customerId);
  	  		String queryStr="SELECT customer_address_id,address_type_id, street_address, city_id, zip, state_id, country_id, is_active"+
  	  				" FROM customer_addresses"+
  	  				" WHERE customer_id=? AND address_type_id=1";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, customerId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 resultSet= preparedStatement.getResultSet();
  	        	if(resultSet.next()){
  	  	        	long cId= resultSet.getLong(1);
  	  	        	int addrType= resultSet.getInt(2); 			//address_type_id
  	  	        	String steetName= resultSet.getString(3); 	//street_address
  	  	        	long cityId= resultSet.getLong(4); 			//city_id
  	  	        	String zip= resultSet.getString(5);			//zip
  	  	        	long stateId =resultSet.getLong(6); 		//state_id
  	  	        	long countryId= resultSet.getLong(7); 		//country_id
  	  	        	int activeStatus= resultSet.getInt(8);		//is_active
  	  	        	

  	  	        	billingAddr.setAddressId(cId);
  	  	            billingAddr.setTypeOfAddress((addrType==1)? AddressType.Billing_Address:AddressType.Shipping_Address);
  	  	        	billingAddr.setStreetAddress(steetName);
  	  	        	billingAddr.setCity(av.getCityName(cityId));
  	    			//System.out.println("Checkpoint #2 ");
  	  	        	billingAddr.setZip(zip);
  	  	        	billingAddr.setState(av.getStateName(stateId));
  	    			//System.out.println("Checkpoint #3 ");

  	  	        	billingAddr.setCountry(av.getCountryName(countryId));
  	    			//System.out.println("Checkpoint #4 ");

  	  	        	billingAddr.setActive((activeStatus==1)? true:false);
  	  	        }//end if
  	        }//end if
  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
        return billingAddr;
  	}
  	
 	//Private method used by the getCustomer() method
	private ArrayList <Address>  getShippingAddressesInfo(Connection dbConnection,long customerId) throws Exception{
		ArrayList <Address> sAddrList= new ArrayList<Address>();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getShippingAddressesInfo, id= "+customerId);
  	  		String queryStr="SELECT customer_address_id,address_type_id, street_address, city_id, zip, state_id, country_id, is_active"+
  	  				" FROM customer_addresses"+
  	  				" WHERE customer_id=? AND address_type_id=2";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, customerId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 resultSet= preparedStatement.getResultSet();
  	        	
  	        	 while(resultSet.next()){
  	        		Address shippingAddr= new Address();
   	  	        	long cId= resultSet.getLong(1);
   	  	        	int addrType= resultSet.getInt(2); 			//address_type_id
   	  	        	String steetName= resultSet.getString(3); 	//street_address
   	  	        	long cityId= resultSet.getLong(4); 			//city_id
   	  	        	String zip= resultSet.getString(5);			//zip
   	  	        	long stateId =resultSet.getLong(6); 		//state_id
   	  	        	long countryId= resultSet.getLong(7); 		//country_id
   	  	        	int activeStatus= resultSet.getInt(8);		//is_active
   	  	        	
    	  	        	
   	  	        	shippingAddr.setAddressId(cId);
   	  	        	shippingAddr.setTypeOfAddress((addrType==1)? AddressType.Billing_Address:AddressType.Shipping_Address);
   	  	        	shippingAddr.setStreetAddress(steetName);
   	  	        	shippingAddr.setCity(av.getCityName(cityId));
   	  	        	shippingAddr.setState(av.getStateName(stateId));
   	  	        	shippingAddr.setCountry(av.getCountryName(countryId));
   	  	        	shippingAddr.setActive((activeStatus==1)? true:false);
   	  	        	
   	  	        	sAddrList.add(shippingAddr);
  	        	 }//end while
  	        	  
  	        }//end if

  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
       
        return sAddrList;
  	}
	
	//Private method used by the getCustomer() method
	private ArrayList <Phone>  getPhoneInfo(Connection dbConnection,long customerId) throws Exception{
		ArrayList <Phone> phoneList= new ArrayList<Phone>();
  		ResultSet resultSet;
  		
  		try{
  			//System.out.println("Inside the getPhoneInfo, id= "+customerId);
  	  		String queryStr="SELECT phone_id,phone_type_id, phone_number"+
  	  						" FROM customer_phone"+
  	  						" WHERE customer_id=?";
 			preparedStatement = dbConnection.prepareStatement(queryStr );
  	  		preparedStatement.setLong(1, customerId);

  	        boolean r = preparedStatement.execute();
  	        if(r){
  	        	 resultSet= preparedStatement.getResultSet();
  	        	
  	        	 while(resultSet.next()){
  	        		Phone newPhone;
   	  	        	long phoneId= resultSet.getLong(1);
   	  	        	int pType= resultSet.getInt(2); 			//phone_type_id
   	  	        	String phone= resultSet.getString(3); 		//phone_number

   	  	        	//int activeStatus= resultSet.getInt(8);		//is_active
   	  	        	
   	  	        	
   	  	        	newPhone= new Phone(PhoneType.Cell_Phone_Number,phone);
   	  	        	newPhone.setPhoneId(phoneId);
   	  	        	
   	  	        	phoneList.add(newPhone);
   	  	        	//shippingAddr.setActive((activeStatus==1)? true:false);
  	        	 }//end while
  	        	  
  	        }//end if
  		}
  		catch (Exception e) {
  		      throw e;
  		} 
  	    finally {
  		      close();
  		}
       
        return phoneList;
  	}
	
private boolean deactivateCustomer(Connection dbConnection,long customerId) throws Exception{
		
		boolean cResult=false;
  		try{
  			//System.out.println("customerId= "+ c.getCustomerId());
  			preparedStatement = dbConnection.prepareStatement("UPDATE customers "
 	    		  										+" SET is_active= 0"
 	    		  										+" WHERE customer_id="+customerId );
 	      preparedStatement.executeUpdate();
 	      System.out.println("Finished deactivating customer:");
 	      cResult=true;
 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return  cResult;
		
	}
	
	private boolean deactivateCustomerAddress(Connection dbConnection, long customerId)throws Exception{
		boolean result=false;
		try{
			preparedStatement= dbConnection.prepareStatement("UPDATE customer_addresses"+
															" SET is_active=?"+
															" WHERE customer_id=?");
			preparedStatement.setInt(1, 0);
			preparedStatement.setLong(2, customerId);
			preparedStatement.executeUpdate();
			result=true;
		}
		  catch (Exception e) {
	 	      throw e;
	 	    } 
	 	  finally {
	 	      close();
	 	    }
		
		return result;
	}
	
  // You need to close the resultSet
  private void close() {
    try {
      if (resultSet != null) {
        resultSet.close();
      }

      if (statement != null) {
        statement.close();
      }

      if (connect != null) {
        connect.close();
      }
    } catch (Exception e) {

    }
  }
  
  	//Private method used by updateCustomerPhone()
	private boolean updatePhone(Connection dbConnection, Phone c, long cid)throws Exception{
  		boolean result=false;
  		try{

  			preparedStatement = dbConnection.prepareStatement("UPDATE customer_phone"
						+"  SET phone_type_id=1"//+c.getPhoneNumbers().get(i).getTypeOfPhone()
						+", phone_number=?"
						+" WHERE customer_id="+cid
						+" AND phone_id="+c.getPhoneId()
						);

			preparedStatement.setString(1, c.getPhoneNumber());
			//System.out.println("SQL for Updating Phone table: "+ preparedStatement);
			
			int updateResult= preparedStatement.executeUpdate();
			
  			
  			if(updateResult> 0)
  	 	    	result= true;
 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return result;
  	}
	
	//Private method used by the updateCustomerPayment() method
	private boolean updatePayment(Connection dbConnection, Payment p, long cid)throws Exception{
 		ArrayList<Integer> passList = new ArrayList<Integer>();
  		ArrayList<Integer> failList = new ArrayList<Integer>();
  		boolean result=false;
  		try{

  			 preparedStatement = dbConnection.prepareStatement("UPDATE customer_payment_types "
						+"  SET payment_type_id=1"//+c.getPaymentCards().get(i).getPaymentNetwork()
						+", acct_name=?"
						+", account_number=?"
						+", acct_exp_date=?"
						+", is_default=?"
						+", security_code=?"
						+" WHERE customer_id="+cid
						+" AND payment_type_id="+p.getPaymentTypeId()
						);

			preparedStatement.setString(1, p.getCardName());
			preparedStatement.setString(2, p.getCardNumber());
			preparedStatement.setDate(3, p.getCardExpiration());
			preparedStatement.setInt(4, (p.isDefault())? 1:0);
			preparedStatement.setInt(5, p.getSecurityCode());
			int updateResult= preparedStatement.executeUpdate();
			//System.out.println("SQL for Updating Payment table: "+ preparedStatement);

 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return result;
  	}
  /*
   **********************************************************************************************
   **********************************************************************************************
   										Public Methods
   **********************************************************************************************
   **********************************************************************************************/
 
  /**
   * Method is public method to call the private method insertCustomer()
   * 
   * @return void
   */
  	public boolean addCustomer(Customer c) throws Exception{
  		Connection insertCon=null;
  		long customerId=-1;
  		boolean shipResult=false, billingAddreResult=false,phoneResult=false,pmtResult=false;
  		try{
  	  		insertCon= this.initializeDB();
  	  	    customerId= this.insertCustomer(insertCon, c);

  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally{
  			close();
  		}
  		
  		return(customerId>0) &&(shipResult && phoneResult && pmtResult);
  	}
  	
  	
  	public Customer addCustomer(String fName, String lName, String email, String password) throws Exception{
  		Connection insertCon=null;
  		long customerId=-1;
  		boolean addResult=false;
  		Customer newCustomer= new Customer();
  		try{
  	  		insertCon= this.initializeDB();
  	  	    customerId= this.insertCustomer(insertCon, fName, lName, email, password);
  	  	    if(customerId> 0){
  	  	    	System.out.println("customerId: "+customerId);
  	  	    	 newCustomer= this.getCustomer(customerId);
  	  	    }
  	  		System.out.println("Completed insert into customer table, new customer ID="+newCustomer.getCustomerId());

  	  	
  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally{
  			close();
  		}
  		
  		return newCustomer ;
  	}


  	
  	/**
  	 * 
  	 * @param updatedCustomer
  	 * @return
  	 * @throws Exception
  	 */
  	public boolean updateCustomer(Customer updatedCustomer) throws Exception{
  		Connection updateCon=null;
  		boolean result1=false,result2=false,result3=false,result4=false,result5=false;
  		try{
  			updateCon= this.initializeDB();

  			result1=this.updateCustomerTable(updateCon, updatedCustomer);
  			System.out.println("CustomerDao: Completed Update of Customer -v2: "+result1);


  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally {
  			close();
  		}
  		
  		return (result1 && result2 && result3 && result4);
  	}
  	
  	/**
  	 * 
  	 * @param cid
  	 * @param fName
  	 * @param lName
  	 * @param email
  	 * @return
  	 * @throws Exception
  	 */
  	public boolean updateCustomer( String cid,String fName, String lName, String email) throws  Exception{
  		
  		Connection updateCon=null;
  		boolean result=false;
  		try{
  			updateCon= this.initializeDB();

  			result=this.updateCustomerTable(updateCon, cid, fName, lName, email);
  			System.out.println("CustomerDao: Completed Update of Customer -v1: "+result);


  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally {
  			close();
  		}
  		return result;
  	}
  	
  	/**
  	 * 
  	 * @param updatePhone
  	 * @param customerId
  	 * @return
  	 * @throws Exception
  	 */
  	public boolean updateCustomerPhone(Phone updatePhone, String customerId) throws Exception{
  		Connection updateCon=null;
  		boolean result=false;
  		
  		try{
  			result=this.updatePhone(updateCon, updatePhone, Long.valueOf(customerId));
  			System.out.println("CustomerDao: Completed Update of Customer PhoneNumber via updateCustomerPhone(): "+result);
  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally{
  			close();
  		}
  		return result;
  	}
  	
  	/**
  	 * 
  	 * @param updatedPmt
  	 * @param customerId
  	 * @return
  	 * @throws Exception
  	 */
  	public boolean updateCustomerPayment(Payment updatedPmt, String customerId) throws Exception{
  		Connection updateCon= null;
  		boolean result=false;
  		try{
  			result= this.updatePayment(updateCon, updatedPmt, Long.valueOf(customerId));
  		}
  		catch(Exception e){
  			throw e;
  		}
  		finally{
  			close();
  		}
  		return result;
  	}

  	/**
  	 * 
  	 * @param cid
  	 * @return
  	 */
  	public Customer getCustomer(long cid){
  		Customer matchingCustomer= new Customer();
  		
  		Connection getConnection=null;
  		try {
  			getConnection= this.initializeDB();
  			System.out.println("In the getCustomer cid="+cid);
  			matchingCustomer=this.getCustomerInfo(getConnection, cid);
  			Address bAddr= this.getBillingAddressInfo(getConnection, cid);
  			//System.out.println("Completed the billing address list");
  			
  			ArrayList <Address> sAddrList=this.getShippingAddressesInfo(getConnection, cid);
  			ArrayList <Phone> phoneList= getPhoneInfo(getConnection, cid);
  			ArrayList<Payment> pmtList= getPaymentCardInfo(getConnection, cid);

  			
  			matchingCustomer.setBillingAddress(bAddr);
  			matchingCustomer.setShippingAddresses(sAddrList);
  			matchingCustomer.setPhoneNumber(phoneList);
  			matchingCustomer.setPaymentCards(pmtList);

  			System.out.println("End of the getCustomer method");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return matchingCustomer;
  	}

	public ArrayList<Customer> getAllCustomers(){
  		ArrayList <Customer> customerList= new ArrayList<Customer>();
  		Connection getConnection=null;
  		try {
  			getConnection= this.initializeDB();

  			customerList= this.getCustomers(getConnection);
	  		System.out.println("CustomerDAO: getAllCustomers found "+customerList.size()+ " customers");
	  		

  			//For each customer returned, get shipping, billing address info + payment & phone information
  			for(int i=0; i < customerList.size(); i++){
  				
  				long currentCustomerId= customerList.get(i).getCustomerId();
  		  		System.out.println("Customer DAO,  CustomerId ="+currentCustomerId +", Customer Name: "+customerList.get(i).getLastName()+ ", "+customerList.get(i).getFirstName() +", Status= "+customerList.get(i).isActive());

  				//Get billing address
  	  			Address bAddr= this.getBillingAddressInfo(getConnection, currentCustomerId);
  	  			//System.out.println("Customer DAO,billingAddress= "+bAddr.toString());
  	  			customerList.get(i).setBillingAddress(bAddr);

  	  			//Get shipping address
  	  			ArrayList <Address> sAddrList=this.getShippingAddressesInfo(getConnection, currentCustomerId);
  	  			customerList.get(i).setShippingAddresses(sAddrList);
  	  			//System.out.println("Customer DAO, shipping address= "+sAddrList.size());
  	  			
  	  			//Get Phone List
  	  			ArrayList <Phone> phoneList= this.getPhoneInfo(getConnection, currentCustomerId);
  	  			customerList.get(i).setPhoneNumber(phoneList);
  	  			//System.out.println("Customer DAO, phone list= "+phoneList.size());

  	  			ArrayList <Payment> pmtList= this.getPaymentCardInfo(getConnection, currentCustomerId);
  	  			customerList.get(i).setPaymentCards(pmtList);
  	  			//System.out.println("Customer DAO, payment list= "+pmtList.size());	
  			}
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return customerList;
  	}

	
	public boolean deleteCustomer(long customerId){
  		Connection deleteConnection=null;
  		boolean result=false,caResult=false;
  		
  		try {
  			deleteConnection= this.initializeDB();
  			 result= this.deactivateCustomer(deleteConnection, customerId);
  		     caResult= this.deactivateCustomerAddress(deleteConnection, customerId);
  			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  		return (result && caResult);
  	}

	

}