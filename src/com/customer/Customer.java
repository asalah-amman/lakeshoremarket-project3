package com.customer;



import java.util.ArrayList;
import java.util.Date;

import com.address.*;
import com.payment.*;
import com.phone.*;
import com.order.Order;
import com.product.*;

public class Customer {
	
	
	//Attributes
	private long customerId;
	private String firstName;
	private String lastName;
	private Address billingAddress;
	private ArrayList<Address> shippingAddresses;
	private ArrayList<Product> shoppingCart;
	private ArrayList<Order> pastOrders;
	private ArrayList<Product> returnedProducts;
	private ArrayList<Payment> paymentCards;
	private ArrayList<Phone> phoneNumbers;
	private String email;
	private boolean isActive;
	private boolean isPendingApproval;
	private Date acctActivationDate;
	private ArrayList<ProductReview> reviews;
	
	//Constructors
	public Customer(){
		this.shippingAddresses= new ArrayList<Address>();
		this.pastOrders= new ArrayList<Order>();
		this.returnedProducts= new ArrayList<Product>();
		this.phoneNumbers = new ArrayList<Phone>();
		this.paymentCards= new ArrayList<Payment>();
		this.shoppingCart= new ArrayList<Product>();
		this.billingAddress= new Address();
	}
	
	public Customer(String fName, String lName, Address billingAdr,Address shippingAdr, Payment actvPmt, Phone phone, String email, boolean active ){
		this.shippingAddresses= new ArrayList<Address>();
		this.pastOrders= new ArrayList<Order>();
		this.returnedProducts= new ArrayList<Product>();
		this.phoneNumbers = new ArrayList<Phone>();
		this.paymentCards= new ArrayList<Payment>();
		this.shoppingCart= new ArrayList<Product>();
		
		this.firstName=fName;
		this.lastName=lName;
		this.billingAddress= billingAdr;
		this.shippingAddresses.add(shippingAdr);
		this.paymentCards.add(actvPmt);
		this.phoneNumbers.add(phone);
		this.email=email;
		if(active)
			this.acctActivationDate= new Date();
		else
			this.isPendingApproval=true;
			
	}
	
	//Methods
	public long getCustomerId() {
		return customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public Address getBillingAddress() {
		return billingAddress;
	}
	
	public ArrayList<Address> getShippingAddresses() {
		return shippingAddresses;
	}
	
	public 	 ArrayList<Product>  getShoppingCart() {
		return shoppingCart;
	}
	
	public ArrayList<Order> getPastOrders() {
		return pastOrders;
	}

	public ArrayList<Product> getReturnedProducts() {
		return returnedProducts;
	}
	
	public ArrayList<Payment> getPaymentCards() {
		return paymentCards;
	}

	public ArrayList<Phone> getPhoneNumbers() {
		return phoneNumbers;
	}
	
	public String getEmail() {
		return email;
	}
	public boolean isPendingApproval() {
		return isPendingApproval;
	}
	public boolean isActive() {
		return isActive;
	}
	public Date getAcctActivationDate() {
		return acctActivationDate;
	}
	public ArrayList<ProductReview> getReviews() {
		return reviews;
	}

	

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setReviews(ArrayList<ProductReview> reviews) {
		this.reviews = reviews;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}
	public void setShippingAddresses(ArrayList<Address> shippingAddresses) {
		this.shippingAddresses = shippingAddresses;
	}
	public void setShoppingCart(ArrayList<Product> shoppingCart) {
		this.shoppingCart = shoppingCart;
	}
	public void setPastOrders(ArrayList<Order> pastOrders) {
		this.pastOrders = pastOrders;
	}
	public void setReturnedProducts(ArrayList<Product> returnedProducts) {
		this.returnedProducts = returnedProducts;
	}
	public void setPaymentCards(ArrayList<Payment> activePayment) {
		this.paymentCards= activePayment;
	}
	public void setPhoneNumber(ArrayList<Phone> phoneNumbers) {
		this.phoneNumbers=phoneNumbers;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public void setPendingApproval(boolean isPendingApproval) {
		this.isPendingApproval = isPendingApproval;
	}
	public void setAcctActivationDate(Date acctActivationDate) {
		this.acctActivationDate = acctActivationDate;
	}
	
	@Override 
	public int hashCode(){
		int num;
		num= Integer.valueOf(this.getFirstName().toUpperCase()) + Integer.valueOf( this.getLastName().toUpperCase()) ;
		return num;
	}
	
	@Override 
	public boolean equals(Object obj){
		boolean result=false;
		Customer c= (Customer)obj;
		
		if(c.getFirstName().equals(this.firstName)){
			if(c.getLastName().equals(this.lastName)){
				if(c.getEmail().equals(this.email)){
					if(c.getBillingAddress().equals(this.billingAddress))
						result=true;
				}
			}
		}		
		return result;
	}

	@Override
	public String toString(){
		String output="";
		String delimiter="*********************************************";
		String customerName= "Customer Name: "+ lastName + ", " + firstName ;
		String aStatus= (isActive)? "Active":"Inactive";
		String pStatus= (isPendingApproval)? "Pending Approval":"Account Approved";
		output+= delimiter+"\n\r";
		output+= "Customer ID: "+customerId +"\n\r";
		output+= customerName+"\n\r";
		output+= "Billing Address: "+"\n\r"+billingAddress.toString()+"\n\r";

		for(int i=0; i< shippingAddresses.size(); i++)
			output+= "Shipping Address  "+(i+1)+ " :"+"\n\r"+shippingAddresses.get(i).toString()+"\n\r";

		for(int i=0; i < paymentCards.size();i++)
			output+= "Payment Info: "+(i+1)+":"+"\n\r"+ paymentCards.get(i).toString()+"\n\r";
		
		for(int i=0; i < paymentCards.size();i++)
			output+= "Phone Info: "+(i+1)+":"+"\n\r"+ phoneNumbers.get(i).toString()+"\n\r";
		
		output+= "Email: "+email+"\n\r";
		output+= "Customer Status: "+aStatus+"\n\r";
		output+= "Customer Account Status: "+pStatus+"\n\r";
		if(!isPendingApproval)
			output+= "Appoved Date: "+acctActivationDate+"\n\r";
	
		output+= delimiter+"\n\r";
		return output;
	}

	
}
