package com.customer;

import java.sql.Date;
import java.util.ArrayList;

import com.customer.dal.CustomerDao;
import com.payment.*;
import com.phone.*;
import com.address.*;
import com.address.service.AddressManager;
import com.phone.service.*;
import com.customer.Customer;

public class CustomerManager {

	private static CustomerDao dao = new CustomerDao();
	private AddressManager addrManager = new AddressManager();
	private PhoneManager phoneManager= new PhoneManager();
	
	//Methods
	public ArrayList<Customer> getAllCustomers(){
		return dao.getAllCustomers();
	}
	
	public Customer getCustomer(String id) {
		return dao.getCustomer(Long.valueOf(id));
	}
	

	/* ************************************************************************************************ */
	//Add methods
	/* ************************************************************************************************ */
	public Customer addCustomer(String firstName, String lastName, String email, String password) {
		
		Customer newCustomer= new Customer();
		try {
			newCustomer = dao.addCustomer(firstName, lastName, email, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Completed the createCustomer in CustomerManager class");
		System.out.println(newCustomer.toString());


		return newCustomer;
	}

	
	/* ************************************************************************************************ */
	//Update methods
	/* ************************************************************************************************ */
	public void updateCustomer(String cid, String newFirstName, String newLastName, String newEmail) throws Exception{
		try {
			dao.updateCustomer(cid, newFirstName, newLastName, newEmail);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void updateCustomerAddress(String id, String billingAddrId,AddressType at, String street,String apartmentUnitNum,  String city, String zip, String state)
	throws Exception{
		try {
			this.addrManager.updateCustomerAddress(Long.valueOf(id), street, apartmentUnitNum, city, state, zip, state, at);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
 

	public void updateCustomerPhone(Phone updatedPhone, String cid){
		try {
			dao.updateCustomerPhone(updatedPhone, cid);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void updateCustomerPayment(Payment updatedPayment, String cid){
		try {
			dao.updateCustomerPayment(updatedPayment, cid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	/* ************************************************************************************************ */
	//Delete methods
	/* ************************************************************************************************ */
	public void deleteCustomer(String cId) {
		dao.deleteCustomer(Long.valueOf(cId));
	}

}
