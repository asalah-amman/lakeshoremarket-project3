package com.phone.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.payment.Payment;
import com.phone.*;

import java.sql.Date;

public class PhoneDao {
	        
	  private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;
	  private ArrayList<Payment> pmtList;
	  
	  final private String host = "localhost:3306";
	  final private String user = "root";
	  final private String passwd = "";
	
	 
	  //----------------------------------------------------------------------------------------
	  /**
	   * Method is responsible for initializing a connection to the database
	   * 
	   * @return Connection object
	   */
	  public Connection initializeDB()throws Exception{
		  Connection dbConnection=null;
		 try{
			 Class.forName("com.mysql.jdbc.Driver");
			 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
		 } 
		 catch (SQLException e) {
	      throw e;
	     } 
		 finally {
	      close();
	    }
	      
	      return dbConnection;
	  }//end method
	  //----------------------------------------------------------------------------------------
	  //----------------------------------------------------------------------------------------
	  

	  //Private method used by the ____ method
	  private boolean insertCustomerPhones(Connection dbConnection, Phone p, long customerId)throws Exception{
		  boolean result=false;
		  long newPhoneId=-1;
		  
		  try{
				  String insertSql="INSERT INTO customer_phone(phone_type_id, customer_id, phone_number)"+
							"SELECT ?, ?, ?"+
							" WHERE NOT EXISTS (SELECT 1 FROM customer_phone WHERE customer_id=? AND phone_number=?)";
			    
				  preparedStatement = dbConnection.prepareStatement(insertSql,Statement.RETURN_GENERATED_KEYS);
				  preparedStatement.setInt(1, 1); //c.getPhoneNumbers().get(i).getTypeOfPhone().ordinal()+1
			      preparedStatement.setLong(2, customerId);
			      preparedStatement.setString(3, p.getPhoneNumber());
			      preparedStatement.setLong(4, customerId);
			      preparedStatement.setString(5, p.getPhoneNumber());
	
			      boolean r= preparedStatement.execute();
	
			      ResultSet rs = preparedStatement.getGeneratedKeys();
			   	  if (rs.next()) {
			   		newPhoneId=rs.getLong(1);
			    	p.setPhoneId(newPhoneId);
				   	  //System.out.println("New Phone ID: "+newPhoneId);

			   	  }
			 
			 result=true;
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;
		  
	  }
	  
	  
	//Private method used by the ___ method	
  	private boolean updateCustomerPhones(Connection dbConnection, Phone p, long customerId)throws Exception{
  		boolean result=false;
  		try{

  			preparedStatement = dbConnection.prepareStatement("UPDATE customer_phone"
						+"  SET phone_type_id=1"//+c.getPhoneNumbers().get(i).getTypeOfPhone()
						+", phone_number=?"
						+" WHERE customer_id="+customerId
						+" AND phone_id="+p.getPhoneId()
						);

			preparedStatement.setString(1, p.getPhoneNumber());
			//System.out.println("SQL for Updating Phone table: "+ preparedStatement);
			
			int updateResult= preparedStatement.executeUpdate();
	
  			if(updateResult >0)
  	 	    	result= true;
 	  }
 	  catch (Exception e) {
 	      throw e;
 	    } 
 	  finally {
 	      close();
 	    }
  		
  		return result;
  	}
	  		
	  //Private Method used by getCustomerPhone() method
	private ArrayList <Phone>  getPhoneInfo(Connection dbConnection,long customerId) throws Exception{
		ArrayList <Phone> phoneList= new ArrayList<Phone>();
		ResultSet resultSet;
		
		try{
			//System.out.println("Inside the getPhoneInfo, id= "+customerId);
	  		String queryStr="SELECT phone_id,phone_type_id, phone_number"+
	  						" FROM customer_phone"+
	  						" WHERE customer_id=?";
			preparedStatement = dbConnection.prepareStatement(queryStr );
	  		preparedStatement.setLong(1, customerId);
	
	        boolean r = preparedStatement.execute();
	        if(r){
	        	 resultSet= preparedStatement.getResultSet();
	        	
	        	 while(resultSet.next()){
	        		Phone newPhone;
	  	        	long phoneId= resultSet.getLong(1);
	  	        	int pType= resultSet.getInt(2); 			//phone_type_id
	  	        	String phone= resultSet.getString(3); 		//phone_number
	
	  	        	//int activeStatus= resultSet.getInt(8);		//is_active
	  	        	
	  	        	
	  	        	newPhone= new Phone(PhoneType.Cell_Phone_Number,phone);
	  	        	newPhone.setPhoneId(phoneId);
	  	        	
	  	        	phoneList.add(newPhone);
	  	        	//shippingAddr.setActive((activeStatus==1)? true:false);
	        	 }//end while
	        	  
	        }//end if
		}
		catch (Exception e) {
		      throw e;
		} 
	    finally {
		      close();
		}
	   
	    return phoneList;
	}

	  /*
	  *********************************************************************************************
	  *********************************************************************************************
	   										Public Methods
	  *********************************************************************************************
	  *********************************************************************************************/
	  	
		/**
		 * 
		 * @param cid
		 * @param newPhone
		 * @return
		 */
		public boolean addCustomerPhone(long cid, Phone newPhone){
	  		Connection insertConnection=null;
	  		boolean insertResult=false;
	  		try {
	  			insertConnection= this.initializeDB();
	
	  			insertResult= this.insertCustomerPhones(insertConnection, newPhone, cid);
	  			System.out.println("PhoneDao: Completed the addCustomerPhone()");
	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  		return insertResult;
	  	}
		
		public boolean updateCustomerPhone(long cid, Phone updatedPhone){
	  		Connection updateConnection=null;
	  		boolean updateResult=false;
	  		try {
	  			updateConnection= this.initializeDB();
	
	  			updateResult= this.updateCustomerPhones(updateConnection, updatedPhone, cid);
	  			System.out.println("PhoneDao: Completed the updateCustomerPhone()");
	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  		return updateResult;
	  	}
	
	
		/**
		 * 
		 * @param cid
		 * @return
		 */
	  	public ArrayList<Phone> getCustomerPhone(long cid){
	  		ArrayList<Phone> phoneList= null;
	  		Connection getConnection=null;
	  		try {
	  			getConnection= this.initializeDB();


	  			phoneList= this.getPhoneInfo(getConnection, cid);
	  			System.out.println("PhoneDao: Completed the getCustomerPhone()");
	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  		return phoneList;
	  	}

  	
		
		
	  // You need to close the resultSet
	  private void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }

	}//end class