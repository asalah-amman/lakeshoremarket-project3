package com.phone;

public class Phone {
	//Class Attributes
	private long phoneId;
	private PhoneType typeOfPhone;
	private String phoneNumber;
	
	//Constructor
	public Phone(PhoneType pt, String number){
		this.typeOfPhone=pt;
		this.phoneNumber=number;
	}


	//Methods
	public long getPhoneId() {
		return phoneId;
	}
	
	public PhoneType getTypeOfPhone() {
		return typeOfPhone;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneId(long phoneId) {
		this.phoneId = phoneId;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public void setTypeOfPhone(PhoneType typeOfPhone) {
		this.typeOfPhone = typeOfPhone;
	}
	
	@Override 
	public int hashCode(){
		int num;
		num= Integer.valueOf(this.getPhoneNumber().toUpperCase()) + this.getTypeOfPhone().ordinal()+1;
		return num;
	}
	
	@Override 
	public boolean equals(Object obj){
		boolean result=false;
		Phone a= (Phone)obj;
		
		boolean matchingSPhone= (a.getPhoneNumber().equals(this.getPhoneNumber()))? true:false;
		boolean matchingType= (a.getTypeOfPhone().equals(this.typeOfPhone))? true:false;
		
		return (matchingSPhone && matchingType);
	}
	
	@Override
	public String toString(){
		String output="";
		boolean isEmpty=false;
		
		if(phoneNumber.length()==0)
			isEmpty=true;
		
		output= this.buildWSOutput();
	
		return output;
	}
	
	public String buildWSOutput(){
		boolean isEmpty=false;
		String output="";
		if(String.valueOf(phoneNumber).length()==0 )
			isEmpty=true;
		
		if(!isEmpty){
			output+= "Phone Number: "+phoneNumber ;
		}
		else{
			output="None";
		}
		return output;
	} 
	
}
