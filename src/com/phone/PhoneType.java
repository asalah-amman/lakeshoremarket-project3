package com;


public enum PhoneType {
	Home_Number,
	Cell_Phone_Number,
	Office_Phone_Numer;
	
	
	public int getPhoneTypeId(){
		switch(this){
			case Cell_Phone_Number:{
				return 1;
			}
			case Home_Number:{
				return 2;
			}
			case Office_Phone_Numer:{
				return 3;
			}
			default:
				return -1;
		}
	}
	
	public PhoneType getPhoneType(int typeId){
		
		PhoneType pt=null;
		
		if (typeId==1){
			 pt=PhoneType.Cell_Phone_Number;
		}
		if(typeId==2){
			 pt=PhoneType.Home_Number;
		}
		if(typeId==3){
			 pt=PhoneType.Office_Phone_Numer;
		}
	
		return pt;
	}
}
