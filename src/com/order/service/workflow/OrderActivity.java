package com.order.service.workflow;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.customer.Customer;
import com.customer.service.representation.CustomerRepresentation;
import com.order.Order;
import com.order.OrderManager;
import com.order.OrderStatus;
import com.order.dal.*;
import com.order.service.representation.OrderRepresentation;
import com.order.service.representation.OrderRequest;
import com.product.Product;
import com.product.service.representation.ProductRepSummary;
import com.product.service.representation.ProductRepresentation;
import com.product.service.workflow.Link;


public class OrderActivity {
	
	private static OrderManager oMngr = new OrderManager();
	
	
public ArrayList<OrderRepresentation> getOrders() {
		
		ArrayList<Order> orders = new ArrayList<Order>();
		ArrayList<OrderRepresentation> orderRepresentations = new ArrayList<OrderRepresentation>();
		orders = oMngr.getAllOrders();
		
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order ord = (Order)it.next();
			OrderRepresentation orderRepresentation = new OrderRepresentation();
			orderRepresentation.setCustomerId(ord.getCustomerId());
			orderRepresentation.setorderId(ord.getOrderId());
			orderRepresentation.setGrandTotal(ord.getGrandTotal());
			//orderRepresentation.setOrderDate(ord.getOrderDate());
			orderRepresentation.setQuantity(ord.getQuantity());
			//orderRepresentation.setItems(ord.getItems());
			orderRepresentation.setOrderStatus(ord.getOrderStatus());
          //now add this representation in the list
			orderRepresentations.add(orderRepresentation);
        }
		return orderRepresentations;
	}


public OrderRepresentation getOrder(String  orderId) {
	
	Order ord = oMngr.getOrder(orderId);
	//System.out.println("OrderID= "+ord.getOrderId());
	
	OrderRepresentation ordRep = new OrderRepresentation();
	ordRep.setorderId(ord.getOrderId());
	ordRep.setCustomerId(ord.getCustomerId());
	ordRep.setCustomerAddrId(ord.getCustomerAddrId());
	ordRep.setProductId(ord.getProductId());
	ordRep.setQuantity(ord.getQuantity());
	ordRep.setProductPrice(ord.getProductPrice());
	float tempGrandTotal = (float) (Math.round(ord.getGrandTotal()*100) / 100.0); // limit to 2 decimal places
	ordRep.setGrandTotal(tempGrandTotal);
	String timeStampString = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(ord.getOrderTimestamp());
	ordRep.setOrderTimestamp(timeStampString);
	ordRep.setOrderStatus(ord.getOrderStatus());
	
	setLinks(ordRep, "Cancel");
	// Went with the Links solution above that matches existing Product implementation. Can change if preferred
	//ordRep.setNextState(this.setLinks(ordRep,"Cancel"));
	//System.out.println("Completed the createOrder in OrderActivity class");
	
	return ordRep;
    }

public String deleteOrder(String  orderId) {
	
	int id = Integer.valueOf(orderId);	
	
	boolean deleted = false;
	deleted = oMngr.deleteOrder(id);
	if (deleted == true) {
		//System.out.println("Returning OK");
		return "OK";
	}
	else {
		//System.out.println("Returning failed");
		return "Failed";
	}
	
}


//KM update to take in an OrderRequest and return more attributes
public OrderRepresentation createOrder(OrderRequest orderRequest) {
	
	
	Order order = new Order();
	order.setCustomerId(orderRequest.getCustomerId());
	order.setOrderStatus(orderRequest.getOrderStatus());
	order.setCustomerAddrId(orderRequest.getCustAddrId());
	order.setProductId(orderRequest.getProductId());
	order.setQuantity(orderRequest.getQuantity());
	
	Order ord = oMngr.addOrder(order);
	
	OrderRepresentation ordRep = new OrderRepresentation();
	ordRep.setorderId(ord.getOrderId());
	ordRep.setCustomerId(ord.getCustomerId());
	ordRep.setCustomerAddrId(ord.getCustomerAddrId());
	ordRep.setProductId(ord.getProductId());
	ordRep.setQuantity(ord.getQuantity());
	ordRep.setProductPrice(ord.getProductPrice());
	float tempGrandTotal = (float) (Math.round(ord.getGrandTotal()*100) / 100.0); // limit to 2 decimal places
	ordRep.setGrandTotal(tempGrandTotal);
	String timeStampString = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(ord.getOrderTimestamp()); // convert Timestamp to String for display
	ordRep.setOrderTimestamp(timeStampString);
	ordRep.setIsCanceled(ord.getIsCanceled());
	ordRep.setOrderStatus(ord.getOrderStatus());
	
	 // Add the links
    setLinks(ordRep, "View");
    // Went with the Links solution above that matches existing Product implementation. Can change if preferred
	//ordRep.setNextState(this.setLinks(ordRep,"View"));
	
	//System.out.println("Completed the createOrder in OrderActivity class");
	return ordRep;
}

private void setLinks(OrderRepresentation ordRep, String state) {
	// Set up the activities that can be performed on orders
	String action = state;
	OrderRepresentation oRep = ordRep;
	
	if (action == "View") {
		String url = "http://localhost:8081/orderservice/orders/" + oRep.getOrderId();
		Link view = new Link("View", url);
		view.setType("GET");
		oRep.setLinks(view);
	}
	
	if (action == "Cancel") {
		String url = "http://localhost:8081/orderservice/orders/" + oRep.getOrderId();
		Link cancel = new Link("Cancel", url);
		cancel.setType("DELETE");
		oRep.setLinks(cancel);
	}
	
	/*
	 * NextState functionality we can use, if preferred. Going with Link solution above which matches the 
	 * existing Product implementation
	Link newLink= new Link();
	
	if ("View".equals(action)) {
		String url = "http://localhost:8081/orderservice/orders/" + oRep.getOrderId();
		newLink.setAction("view");  
		newLink.setType("GET"); 
		newLink.setUrl(url);
	}
	
	if ("Cancel".equals(action)) {
		String url = "http://localhost:8081/orderservice/orders/"+ oRep.getOrderId();
		newLink.setAction("cancel");
		newLink.setType("DELETE"); 
		newLink.setUrl(url);
	
	}
	return newLink;
	*/
}

public String deleteOrder(int orderId) {
	
	oMngr.deleteOrder(orderId);;
	
	return "OK";
}


}