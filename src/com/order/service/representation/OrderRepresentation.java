package com.order.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.product.service.representation.AbstractRepresentation;
import com.product.service.workflow.Link;

import java.util.ArrayList;
import java.util.Date;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.product.Product;
import com.order.*;
import com.product.service.workflow.Link;

@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")


public class OrderRepresentation extends AbstractRepresentation {
	private int orderId;
	//private ArrayList<Integer> items;
	//private Date orderDate;
	private float grandTotal;
	private int customerId;
	private int quantity;
	private OrderStatus orderStatus;
	//private ArrayList<Order> pastOrders;
	private String gid;
	//private Link nextState;
	
	//KM added
	private int customerAddrId;
	private long productId;
	private double productPrice;
	private String orderTimestamp;
	private boolean isCanceled;
	
	
	public int getOrderId() {
		return orderId;
	
	}
	
	/*public ArrayList<Integer> getItems(){
		return items;
		
	}
	*/
	/*public Date getOrderDate() {
		return orderDate;
	}
	*/
	
	public String getOrderTimestamp() {
		return orderTimestamp;
	}
	
	public float getGrandTotal() {
		return grandTotal;
	}
	
	public int getCustomerId() {
		return customerId;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	
	/*public ArrayList<Order> getPastOrders() {
		return pastOrders;
	}
	*/
	
	/*public Link getNextState() {
		return nextState;
	}
	*/
	
	// KM added
	public int getCustomerAddrId() {
		return customerAddrId;
	}
	
	public long getProductId() {
		return productId;
	}
	
	public double getProductPrice() {
		return productPrice;
	}
	
	public boolean getIsCanceled() {
		return isCanceled;
	}
	

	/*public void setNextState(Link newNextState) {
		this.nextState = newNextState;
	}
	*/
	
	public void setorderId(int orderId) {
		this.orderId = orderId;
	}
	
	/*public void setItems(ArrayList<Integer> items) {
		this.items = items;
	}
	*/
	
	/*public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	*/
	
	
	public void setGrandTotal(float grandTotal) {
		this.grandTotal = grandTotal; 
	}
	
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	/*public void setPastOrders(ArrayList<Order> pastOrders) {
		this.pastOrders = pastOrders;
	}
	*/
	
	//KM added
	public void setCustomerAddrId(int customerAddrId) {
		this.customerAddrId = customerAddrId;
	}
	
	public void setProductId(long productId) {
		this.productId = productId;
	}
	
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public void setOrderTimestamp(String orderTimestamp) {
		this.orderTimestamp = orderTimestamp;	
	}
	
	public void setIsCanceled(boolean isCanceled) {
		this.isCanceled = isCanceled;	
	}
}
