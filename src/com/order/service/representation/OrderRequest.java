package com.order.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import java.util.ArrayList;
import java.util.Date;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.product.Product;
import com.order.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")


public class OrderRequest {
	private int orderId;
	//private ArrayList<Product> items;
	private ArrayList <Integer> items;
	private Date orderDate;
	private float grandTotal;
	private int customerId;
	private int quantity;
	private OrderStatus orderStatus;
	private ArrayList<Order> pastOrders;
	private long productId;
	private int productQty;
	
	//KM added
	private int customerAddrId;
	private double productPrice;
	private String orderTimestamp;
	private boolean isCanceled;
	
	public int getOrderId() {
		return orderId;
	
	}
	
	public ArrayList<Integer> getItems(){
		return items;
	}
	
	public Date getOrderDate() {
		return orderDate;
	}
	
	public String getOrderTimestamp() {
		return orderTimestamp;
	}
	
	public float getGrandTotal() {
		return grandTotal;
	}
	
	public int getCustomerId() {
		return customerId;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	
	public ArrayList<Order> getPastOrders() {
		return pastOrders;
	}
	
	// KM added
	public int getCustAddrId() {
		return customerAddrId;
	}
	
	public long getProductId() {
		return productId;
	}
	
	public int getProductQty() {
		return productQty;
	}
	
	public double getProductPrice() {
		return productPrice;
	}
	
	public boolean getIsCanceled() {
		return isCanceled;
	}
	
	
	public void setorderId(int orderId) {
		this.orderId = orderId;
	}
	
	public void setItems(ArrayList<Integer> items) {
		this.items = items;
	}
	
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
	public void setOrderTimestamp(String orderTimestamp) {
		this.orderTimestamp = orderTimestamp;
	}
	
	public void setGrandTotal(float grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
			
	public void setPastOrders(ArrayList<Order> pastOrders) {
		this.pastOrders = pastOrders;
	}
	
	// KM added
	public void setCustAddrId(int addrId) {
		this.customerAddrId = addrId;
	}
	
	public void setProductId(long productId) {
		this.productId = productId;
	}
	
	public void setProductQty(int productQty) {
		this.productQty = productQty;
	}
	
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	
	public void setIsCanceled(boolean isCanceled) {
		this.isCanceled = isCanceled;	
	}
	
}
