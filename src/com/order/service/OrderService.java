package com.order.service;

import java.util.ArrayList;

import com.order.service.representation.OrderRepresentation;
import com.order.service.representation.OrderRequest;


public interface OrderService {
	public ArrayList<OrderRepresentation> getOrders();
	public OrderRepresentation getOrder(String orderId);
	public OrderRepresentation createOrder(OrderRequest orderRequest);
}
