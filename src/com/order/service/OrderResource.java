package com.order.service;
import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.CacheControl;

import com.order.service.representation.OrderRepresentation;
import com.order.service.representation.OrderRequest;
import com.order.service.workflow.OrderActivity;

@Path ("/orderservice/")
public class OrderResource implements OrderService
{
		@GET
		@Produces({"application/xml" , "application/json"})
		@Path("/orders")
		public ArrayList<OrderRepresentation> getOrders() {
			System.out.println("GET METHOD Request for all orders .............");
			OrderActivity orderActivity = new OrderActivity();
			return orderActivity.getOrders();
		}

		@GET
		@Produces({"application/xml" , "application/json"})
		@Path("/orders/{orderId}")
		public OrderRepresentation getOrder(@PathParam("orderId") String orderId) {
			System.out.println("GET METHOD Request from Client with orderRequest String ............." + orderId);
			OrderActivity orderActivity = new OrderActivity();
			return orderActivity.getOrder(orderId);
		}

		@POST
		@Produces({"application/xml" , "application/json"})
		@Path("/orders")
		public OrderRepresentation createOrder(OrderRequest orderRequest) {
			System.out.println("POST METHOD Request from Client with ............." + orderRequest.getOrderId());
			OrderActivity orderActivity = new OrderActivity();
			// updated to just send the entire OrderRequest object to the Activity
			return orderActivity.createOrder(orderRequest);
		}
		
		@DELETE
		@Produces({"application/xml" , "application/json"})
		@Path("/orders/{orderId}")
		public Response deleteOrder(@PathParam("orderId") String orderId) {
			System.out.println("DELETE METHOD Request from Client with orderRequest String ............." + orderId);
			OrderActivity orderActivity = new OrderActivity();
			String res = orderActivity.deleteOrder(orderId);
			if (res.equals("OK")) {
				return Response.status(Status.OK).build();
			}
			if (res.equals("Failed")) {
				return Response.status(Status.NOT_MODIFIED).build();
			}
			else return null;
		}
}