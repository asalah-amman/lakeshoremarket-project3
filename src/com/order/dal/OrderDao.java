package com.order.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.customer.Customer;
import com.order.*;
import com.product.Product;
import com.product.dal.ProductDao;

public class OrderDao {

	
	  final private String host = "localhost:3306";
	  final private String user = "root";
	  final private String passwd = "";
	  
	  
	  private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;
	  
	  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	  
	  
	//----------------------------------------------------------------------------------------
	  /**
	   * Method is responsible for initializing a connection to the database
	   * 
	   * @return Connection object
	   */
	  public Connection initializeDB()throws Exception{
		  Connection dbConnection=null;
		 try{
			 Class.forName("com.mysql.jdbc.Driver");
			 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
		 } 
		 catch (SQLException e) {
	      throw e;
	     } 
		 finally {
	      close();
	    }
	      
	      return dbConnection;
	  }//end method
	  //----------------------------------------------------------------------------------------
	  //----------------------------------------------------------------------------------------
	  
	  // KM updated to add more attributes
	  private int insertOrder(Connection dbConnection, Order o)throws Exception{
		  int newOrderId=-1;
		  Date date = new Date();
		  try{
		      preparedStatement = dbConnection.prepareStatement("INSERT INTO orders(customer_id, status_id, customer_address_id, order_date_time, order_total, is_canceled)"+
		    		  										"SELECT ?, ?, ? , ?, ?, ? ",  Statement.RETURN_GENERATED_KEYS);
		      preparedStatement.setInt(1, o.getCustomerId());
		      preparedStatement.setInt(2, OrderStatus.Processing.getOrderStatusEnum());
		      preparedStatement.setInt(3, o.getCustomerAddrId());
		      //preparedStatement.setDate(4,new java.sql.Date(System.currentTimeMillis()));
		      preparedStatement.setTimestamp(4, new java.sql.Timestamp(date.getTime()));
		      preparedStatement.setFloat(5, o.getGrandTotal());
		      preparedStatement.setInt(6, o.getIsCanceled()? 1:0);
		           
		      preparedStatement.executeUpdate();		      		      
		      // now get the ID:
		      ResultSet rs = preparedStatement.getGeneratedKeys();
		      if (rs.next()) {
		         newOrderId = rs.getInt(1);
		      }		      
		      if(newOrderId > 0 )
		    	  o.setorderId(newOrderId);		     
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return newOrderId;
		  
	  }
	  
	  // KM added custAddrId and removed status
	  private int insertOrder(Connection dbConnection, int custId, OrderStatus orderstatus, int custAddrId)throws Exception{
		  int newOrderId=-1;
		  try{
		      preparedStatement = dbConnection.prepareStatement("INSERT INTO orders(customer_id, status_id, customer_address_id, order_date_time)"+
		    		  										"SELECT ?, ?, ?, ? ",  Statement.RETURN_GENERATED_KEYS);
		      preparedStatement.setInt(1, custId);
		      preparedStatement.setInt(2, OrderStatus.Processing.getOrderStatusEnum());  
		      preparedStatement.setInt(3, custAddrId);
		      preparedStatement.setDate(4,new java.sql.Date(System.currentTimeMillis()));
		      preparedStatement.executeUpdate();		      		      
		      // now get the ID:
		      ResultSet rs = preparedStatement.getGeneratedKeys();
		      if (rs.next()) {
		         newOrderId = rs.getInt(1);
		      }		   
		      //System.out.println("DAO New order ID is: " + newOrderId);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return newOrderId;
		  
	  }
	  
	  // KM updated to add more attributes
	  private boolean insertOrderDetails(Connection dbConnection, Order o)throws Exception{
		  boolean result=false;
		  try{
		      preparedStatement = dbConnection.prepareStatement("INSERT INTO order_details(order_id, product_id, quantity, price, item_removed)"+
		    		  										"SELECT ?, ?, ?, ?, ? ");
		      preparedStatement.setInt(1, o.getOrderId());
		      preparedStatement.setLong(2, o.getProductId());
		      preparedStatement.setInt(3, o.getQuantity());
		      preparedStatement.setDouble(4, 0);
		     //preparedStatement.setFloat(4, o.getAmountPaid());
		      preparedStatement.setNull(5, 0);    
		      int insertResult=preparedStatement.executeUpdate();		      
		      result = (insertResult > 0);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;
		  
	  }
	  
	  
	  private boolean updateOrder(Connection dbConnection, int orderId, OrderStatus status)throws Exception{
		  boolean result = false;
		  try{
		      preparedStatement = dbConnection.prepareStatement("UPDATE orders set status_id = ? where order_id = ?");
		      preparedStatement.setInt(1, status.getOrderStatusEnum());		     
		      preparedStatement.setInt(2, orderId);     
		      int updateResult=preparedStatement.executeUpdate();
		      result = (updateResult > 0);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;		  
	  }
	  
	  private boolean updateOrder(Connection dbConnection, Order o)throws Exception{
		  boolean result = false;
		  //System.out.println("OrderDAO updateOrder has ID: " + o.getOrderId());
		  //System.out.println("OrderDAO OrderStatus is: " + o.getOrderStatus());
	
		  try{
		      preparedStatement = dbConnection.prepareStatement("UPDATE orders "
		    		  													+ "SET customer_id = ? "
		    		  													+ " , status_id = ? "
		    		  													+ " , customer_address_id = ? "
		    		  													+ " , order_total = ? "
		    		  													+ " , is_canceled = ? "
		    		  													+ " WHERE order_id ="+o.getOrderId());
		      preparedStatement.setInt(1, o.getCustomerId());
		      preparedStatement.setInt(2, o.getOrderStatus().getOrderStatusEnum());
		      preparedStatement.setInt(3, o.getCustomerAddrId());
		      preparedStatement.setFloat(4, o.getGrandTotal());
		      preparedStatement.setInt(5, o.getIsCanceled()? 1:0);
		      
		      int updateResult=preparedStatement.executeUpdate();
		      result = (updateResult > 0);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;		  
	  }
	  
	  
	  private boolean updateOrderDetails(Connection dbConnection, int quantity, double price, int orderId)throws Exception{
		  boolean result = false;
		  try{
		      preparedStatement = dbConnection.prepareStatement("UPDATE order_details set quantity = ?, price = ? where order_id = ?");
		      preparedStatement.setInt(1, quantity);		     
		      preparedStatement.setDouble(2, price);
		      preparedStatement.setInt(3, orderId);
		      
		      int updateResult=preparedStatement.executeUpdate();
		      result = (updateResult > 0);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;		  
	  }
	  /**
	   * Method is public method to call the private method insertIntoOrders()
	   * 
	   * @return void
	   */
	  	public int addOrder(Order o) throws Exception{
	  		Connection insertCon=null;
	  		int newOrderId=-1;
	  		// get price for product
	  		ProductDao prodDao = new ProductDao();
	  		Product prod = prodDao.getProduct(o.getProductId());
	  	    double price = prod.getProductPrice();
	  	    // calculate order total
	  	    o.setSubtotal(price, o.getQuantity());
	  	    o.setTax(o.getSubtotal());
	  	    o.setGrandTotal(o.getSubtotal() + o.getTax());
	  	    o.setIsCanceled(false);
	  		
	  		try{
	  	  		insertCon= this.initializeDB();
	  	  	    newOrderId = this.insertOrder(insertCon, o);
	  	  	    o.setorderId(newOrderId);
	  	  	    //System.out.println("Completed insert into orders table. The OrderID is: " + newOrderId);	  	  	    
	  	  	    this.insertOrderDetails(insertCon, o);	  	  	    
	  	  	    //System.out.println("Completed insert into order_details table.");
	  	  	    this.updateOrderDetails(insertCon, o.getQuantity(), price, o.getOrderId());
	  		}
	  		catch(Exception e){
	  			throw e;
	  		}
	  		finally{
	  			close();
	  		}
	  		
	  		return newOrderId;
	  	}
	  	
	  	// KM updated to include more attributes
	  	public Order addOrder(int orderId, OrderStatus orderstatus, int custAddrId, long productId, int productQty) throws Exception{
	  		Connection insertCon=null;
	  		int newOrderId=-1;
	  		boolean addResult=false;
	  		Order newOrder= new Order();
	  		ProductDao prodDao = new ProductDao();
	  		try{
	  	  		insertCon= this.initializeDB();
	  	  		newOrderId= this.insertOrder(insertCon, orderId, orderstatus, custAddrId);
	  	  	    if(newOrderId> 0){
	  	  	    	Product prod = prodDao.getProduct(productId);
	  	  	    	double price = prod.getProductPrice();
	  	  	    	this.updateOrderDetails(insertCon, productQty, price, newOrderId);
	  	  	    	//System.out.println("Completed insert into orders table. The OrderID is: " + newOrderId);
	  	  	    newOrder= this.getOrder(newOrderId);
	  	  	    }
	  	  		//System.out.println("Completed insert into o table, new order ID="+newOrder.getOrderId());

	  	  	
	  		}
	  		catch(Exception e){
	  			throw e;
	  		}
	  		finally{
	  			close();
	  		}
	  		
	  		return newOrder ;
	  	}
	  	
	  	/**
		   * Method is public method to call the private method updateOrder()
		   * 
		   * @return void
		   */
		  	public boolean updateOrderStatus(int orderId, OrderStatus status) throws Exception{
		  		Connection conn=null;
		  		boolean result=false;
		  		try{
		  			conn= this.initializeDB();
		  	  	    result = this.updateOrder(conn, orderId, status);
		  	  		//System.out.println("Completed updating order table.");
		  		}
		  		catch(Exception e){
		  			throw e;
		  		}
		  		finally{
		  			close();
		  		}
		  		
		  		return result;
		  	}
		  	
		  	public boolean updateOrderTable(int quantity, int price, int orderId) throws Exception{
		  		Connection conn=null;
		  		boolean result=false;
		  		try{
		  			conn= this.initializeDB();
		  	  	    result = this.updateOrderDetails(conn, quantity, price, orderId);
		  	  		//System.out.println("Completed updating orderDetails table.");
		  		}
		  		catch(Exception e){
		  			throw e;
		  		}
		  		finally{
		  			close();
		  		}
		  		
		  		return result;
		  	}
		  	
		  	//GET
		  	public ArrayList <Order> getAllOrders() {
		  		ArrayList <Order> OrderList= new ArrayList<Order>();
		  		Connection getConnection=null;			  		
		  		try {
		  			getConnection= this.initializeDB();			  			
		  			OrderList= this.getAllOrders(getConnection);
		  			
				} catch (Exception e) {
					e.printStackTrace();
				}
		  		return OrderList;
		  	}

		  	public Order getOrder(int orderId){
		  		//System.out.println("DAO getOrder Order ID is: " + orderId);
		  		Order matchingOrder= new Order();
		  		Connection getConnection=null;
		  		try {
		  			getConnection= this.initializeDB();
		  			matchingOrder=this.getOrderInfo(getConnection, orderId);
		  			//System.out.println("Grand total for this order is: " + matchingOrder.getGrandTotal());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  		return matchingOrder;
		  	}
		  	
		  	
		  	private ArrayList<Order> getAllOrders(Connection dbConnection) throws Exception{
		  		ResultSet resultSet;
		  		ArrayList <Order> orderList= new ArrayList<Order>();
		  		Order newOrder;
		  		try{
		  	  		String queryStr="select a.*, b.product_id, b.quantity, b.price, b.item_removed FROM orders as a INNER JOIN order_details as b on a.order_id=b.order_id";
		 			preparedStatement = dbConnection.prepareStatement(queryStr);		  	  		
		  	  		// execute select SQL statement
		 			boolean r = preparedStatement.execute();
		  	        if(r){
		  	        	resultSet= preparedStatement.getResultSet();
		  	        	while(resultSet.next()){
		  	        		newOrder= new Order();
		  	  	        	int cId= resultSet.getInt(1);
		  	  	             //Date orderDate= resultSet.getDate(2);
		  	  	        	float grandTotal= resultSet.getFloat(3);
		  	  	            float amountPaid= resultSet.getFloat(4);
		  	  	        	 	
		  	  	        	int orderId= resultSet.getInt(5);	
		  	  	        	int quantity = resultSet.getInt(6);
		  	  	            float shippingTotal= resultSet.getFloat(7);
		  	  	            float tax= resultSet.getFloat(8);
		  	  	            
		  	  	            
		  	  	            
		  	  	        	newOrder.setCustomerId(cId);
		  	  	            //newOrder.setOrderDate(orderDate);
				  	  	    newOrder.setGrandTotal(grandTotal);
				  	  	    newOrder.setAmountPaid(amountPaid);
				  	  	    
				  	  	    newOrder.setorderId(orderId);
				  	  	    newOrder.setQuantity(quantity);
				  	  	    newOrder.setShippingTotal(shippingTotal);
				  	  	    newOrder.setTax(tax);
				  	  	    
				  	  	    orderList.add(newOrder);
		  	        	}
		  	        	
		  	        }//end if (r)
		  		}
		  		catch (Exception e) {
		  		      throw e;
		  		} 
		  	    finally {
		  		      close();
		  		}
		       
		        return orderList;
		  	}
		  	
		  	private Order getOrderInfo(Connection dbConnection,int orderId) throws Exception{
		  		Order matchingOrder= new Order();
		  		ResultSet resultSet;
		  		
		  		//System.out.println("DAO getOrder ID is: " + orderId);
		  		
		  		try{ // KM edited query
		  	  		String queryStr="select a.*, b.product_id, b.quantity, b.price, b.item_removed FROM orders as a INNER JOIN order_details as b on a.order_id=b.order_id WHERE a.order_id=?";
		  		
		  	  		preparedStatement = dbConnection.prepareStatement(queryStr);
		  	  		preparedStatement.setInt(1, orderId);
		  	  		// execute select SQL statement
					resultSet = preparedStatement.executeQuery();
  	  	        	
					while (resultSet.next()){ // KM updated to include more attributes 
						
						matchingOrder.setCustomerId(resultSet.getInt("customer_id"));//customer id
	  	  	        	matchingOrder.setorderId(resultSet.getInt("order_id"));//order id
	  	  	        	//System.out.println("DAO customer_id: " + matchingOrder.getCustomerId());
	  	  	        	matchingOrder.setCustomerAddrId(resultSet.getInt("customer_address_id"));//address id
	  	  	        	matchingOrder.setQuantity(resultSet.getInt("quantity"));//quantity
	  	  	        	matchingOrder.setProductPrice(resultSet.getDouble("price"));//price
	  	  	        	matchingOrder.setProductId(resultSet.getLong("product_id"));//product id
	  	  	        	matchingOrder.setOrderTimestamp(resultSet.getTimestamp("order_date_time"));//order date
	  	  	        	matchingOrder.setGrandTotal(resultSet.getFloat("order_total"));//order total
	  	  	        	matchingOrder.setIsCanceled(resultSet.getBoolean("is_canceled"));//is canceled
	  	  	        	int orderStatus = resultSet.getInt("status_id");
	  	  	        	switch(orderStatus) {
	  	  	        		case 1: matchingOrder.setOrderStatus(OrderStatus.Processing);
	  	  	        			break;
	  	  	        		case 2: matchingOrder.setOrderStatus(OrderStatus.Completed_Preparing_Order);
	  	  	        			break;
	  	  	        		case 3: matchingOrder.setOrderStatus(OrderStatus.Partially_Shipped);
	  	  	        			break;
	  	  	        		case 4: matchingOrder.setOrderStatus(OrderStatus.Shipped);
	  	  	        			break;
	  	  	        		case 5: matchingOrder.setOrderStatus(OrderStatus.Delivered);
	  	  	        			break;
	  	  	        		case 6: matchingOrder.setOrderStatus(OrderStatus.Fulfilled);
	  	  	        			break;
	  	  	        		case 7: matchingOrder.setOrderStatus(OrderStatus.Cancelled);
	  	  	        			break;
	  	  	        		case 8: matchingOrder.setOrderStatus(OrderStatus.Received_Return);
	  	  	        			break;
	  	  	        		case 9: matchingOrder.setOrderStatus(OrderStatus.Processing_Return);
	  	  	        			break;
	  	  	        		case 10: matchingOrder.setOrderStatus(OrderStatus.Completed_Return);
	  	  	        			break;
	  	  	        	
	  	  	        	}
	  	  	        	 
		  	  	    }
		  		}
		  		catch (Exception e) {
		  		      throw e;
		  		} 
		  	    finally {
		  		      close();
		  		}
		       
		        return matchingOrder;
		  	}
		  	
		    //Delete
		  	public boolean deleteOrder(int orderId){
		  		Connection delConnection=null;
		  		try {
		  			delConnection= this.initializeDB();
		  			this.deleteOrderRecord(delConnection, orderId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  		return true;
		  	}
		  
		  	private boolean deleteOrderRecord(Connection dbConnection,int orderId) throws Exception{
		  				
		  		Order targetOrder = null;
		  		Connection getConnection=null;
		  		boolean deleted = false;
		  		
		  		try {
		  			getConnection= this.initializeDB();
			  		targetOrder = this.getOrder(orderId);
			  		//System.out.println("DAO DeleteOrderRecord has ID: " + targetOrder.getOrderId());
			  		targetOrder.setIsCanceled(true);
			  		targetOrder.setOrderStatus(OrderStatus.Cancelled);
		  			
		  			deleted = this.updateOrder(dbConnection, targetOrder);
		  		
		  		}
		  		catch (Exception e) {
		  		      throw e;
		  		} 
		  	    finally {
		  		      close();
		  		}		       
		        return deleted;
		  	}
	
	 // You need to close the resultSet
	  private void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }


}
