package com.order.client;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Response;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;


import com.order.service.representation.OrderRequest;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;


public class OrderServiceClient{
	
	public static void main(String args[]) throws Exception {
		


   	 	List<Object> providers = new ArrayList<Object>();
        JacksonJsonProvider provider = new JacksonJsonProvider();
        provider.addUntouchable(Response.class);
        providers.add(provider);
        
        
        /*****************************************************************************************
         * GET METHOD invoke
        *****************************************************************************************/
        System.out.println("GET METHOD .........................................................");
        WebClient getClient = WebClient.create("http://localhost:8081", providers);
        WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
        WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        getClient = getClient.accept("application/xml").type("application/json").path("/orderservice/order");
     	
        String getRequestURI = getClient.getCurrentURI().toString();
        System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
        String getRequestHeaders = getClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + getRequestHeaders);
       
        
      //to see as raw XML/json
        String response =  getClient.get(String.class);
        System.out.println("GET MEDTHOD Response ........." + response);
        
        /*****************************************************************************************
         * POST METHOD invoke
        *****************************************************************************************/
        System.out.println("POST METHOD .........................................................");
        WebClient postClient = WebClient.create("http://localhost:8081", providers);
        WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
        WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());
                 
        // change application/xml  to application/json get in json format
        postClient = postClient.accept("application/xml").type("application/json").path("/orderservice/order");
     	
        String postRequestURI = postClient.getCurrentURI().toString();
        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
        String postRequestHeaders = postClient.getHeaders().toString();
        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setorderId(3);
        orderRequest.setCustomerId(2);
        orderRequest.setGrandTotal(100f);
        orderRequest.setQuantity(5);
        
        String responsePost =  postClient.post(orderRequest, String.class);
        System.out.println("POST MEDTHOD Response ........." + responsePost);
        
	}
	
}