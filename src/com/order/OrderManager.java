package com.order;

import java.util.ArrayList;
import java.util.Date;



import com.customer.Customer;
import com.order.dal.OrderDao;
import com.product.Product;
import com.order.*;

public class OrderManager {

	
	
	private static OrderDao ord = new OrderDao();
	
	//Get methods
	
	public ArrayList<Order> getAllOrders(){
		//System.out.println("DAO getAllOrders  Complete");
		return ord.getAllOrders();
		
	}
	

	public Order getOrder(String orderId) {
			Order ordr = ord.getOrder(Integer.valueOf(orderId)); //changed for troubleshooting to see the value
			//System.out.println("OrderManager Order ID is: " + ordr.getOrderId());
			//System.out.println("OrderManager Grand Total is: " + ordr.getGrandTotal());
			return ordr;
		
	}

	
	//Add methods
	// KM updated to just pass order object and return order object from db
	public Order addOrder(Order order) {
		Order newOrder = new Order();
		int orderId = 0;
		
		try {
			orderId = ord.addOrder(order);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if (orderId > 0) {
			newOrder = ord.getOrder(orderId);
		}
		
		//System.out.println("Completed the createOrder in OrderManager class");
		//System.out.println(newOrder.toString());

		return newOrder;
	}
	
	//Update methods
	
	public void  updateOrderStatus(int orderId, OrderStatus status) {
		
		
		try {
			ord.updateOrderStatus(orderId, status);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
public void updateOrderTable(int orderId, OrderStatus status) {
		
		
		try {
			ord.updateOrderStatus(orderId, status);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}


//Delete methods

public boolean deleteOrder(int orderId) {
	
	boolean deleted = false;
	deleted = ord.deleteOrder(orderId);
	return deleted;

}


	
	public OrderManager() {
		// TODO Auto-generated constructor stub
	}

}
