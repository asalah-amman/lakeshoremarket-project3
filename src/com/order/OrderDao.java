package com.order;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import com.customer.Customer;
import com.order.*;

public class OrderDao {

	 private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;
	  
	  
	  final private String host = "localhost:3306";
	  final private String user = "root";
	  final private String passwd = "";
	  
	//----------------------------------------------------------------------------------------
	  /**
	   * Method is responsible for initializing a connection to the database
	   * 
	   * @return Connection object
	   */
	  public Connection initializeDB()throws Exception{
		  Connection dbConnection=null;
		 try{
			 Class.forName("com.mysql.jdbc.Driver");
			 dbConnection = DriverManager.getConnection("jdbc:mysql://" + host + "/lakeshore?"+ "user=" + user + "&password=" + passwd );
		 } 
		 catch (SQLException e) {
	      throw e;
	     } 
		 finally {
	      close();
	    }
	      
	      return dbConnection;
	  }//end method
	  //----------------------------------------------------------------------------------------
	  //----------------------------------------------------------------------------------------
	  
	
	  private int insertOrder(Connection dbConnection, Order o)throws Exception{
		  int newOrderId=-1;
		  try{
		      preparedStatement = dbConnection.prepareStatement("INSERT INTO orders(customer_id, order_date_time, order_total, status_id)"+
		    		  										"SELECT ?, ?, ? , ?, ? ",  Statement.RETURN_GENERATED_KEYS);
		      preparedStatement.setInt(1, o.getCustomerId());
		      preparedStatement.setDate(2,new java.sql.Date(System.currentTimeMillis()));
		      preparedStatement.setFloat(3, o.getGrandTotal());
		      preparedStatement.setInt(4, OrderStatus.Processing.getOrderStatusEnum());     
		      preparedStatement.executeUpdate();		      		      
		      // now get the ID:
		      ResultSet rs = preparedStatement.getGeneratedKeys();
		      if (rs.next()) {
		         newOrderId = rs.getInt(1);
		      }		      
		      if(newOrderId > 0 )
		    	  o.setorderId(newOrderId);		     
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return newOrderId;
		  
	  }
	  
	  private boolean insertOrderDetails(Connection dbConnection, Order o)throws Exception{
		  boolean result=false;
		  try{
		      preparedStatement = dbConnection.prepareStatement("INSERT INTO order_details(order_id, product_id, quantity, price, item_removed)"+
		    		  										"SELECT ?, ?, ? , ?, ? ");
		      preparedStatement.setInt(1, o.getOrderId());
		      preparedStatement.setInt(2, 1);
		      preparedStatement.setInt(3,o.getQuantity());
		      preparedStatement.setFloat(4, o.getAmountPaid());
		      preparedStatement.setNull(5, 0);    
		      int insertResult=preparedStatement.executeUpdate();		      
		      result = (insertResult > 0);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;
		  
	  }
	  
	  
	  private boolean updateOrder(Connection dbConnection, int orderId, OrderStatus status)throws Exception{
		  boolean result = false;
		  try{
		      preparedStatement = dbConnection.prepareStatement("UPDATE orders set status_id = ? where order_id = ?");
		      preparedStatement.setInt(1, status.getOrderStatusEnum());		     
		      preparedStatement.setInt(2, orderId);     
		      int updateResult=preparedStatement.executeUpdate();
		      result = (updateResult > 0);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;		  
	  }
	  
	  
	  private boolean updateOrderDetails(Connection dbConnection, int quantity, int price, int orderId)throws Exception{
		  boolean result = false;
		  try{
		      preparedStatement = dbConnection.prepareStatement("UPDATE order_details set quantity = ?, price = ? where order_id = ?");
		      preparedStatement.setInt(1, quantity);		     
		      preparedStatement.setInt(2, price);
		      preparedStatement.setInt(3, orderId);
		      
		      int updateResult=preparedStatement.executeUpdate();
		      result = (updateResult > 0);
		  }
		  catch (Exception e) {
		      throw e;
		    } 
		  finally {
		      close();
		    }
		  
		  return result;		  
	  }
	  /**
	   * Method is public method to call the private method insertIntoOrders()
	   * 
	   * @return void
	   */
	  	public boolean addOrder(Order o) throws Exception{
	  		Connection insertCon=null;
	  		int newOrderId=-1;
	  		try{
	  	  		insertCon= this.initializeDB();
	  	  	    newOrderId = this.insertOrder(insertCon, o);
	  	  	    System.out.println("Completed insert into orders table. The OrderID is: " + newOrderId);	  	  	    
	  	  	    this.insertOrderDetails(insertCon, o);	  	  	    
	  	  	    System.out.println("Completed insert into order_details table.");
	  		}
	  		catch(Exception e){
	  			throw e;
	  		}
	  		finally{
	  			close();
	  		}
	  		
	  		return (newOrderId > 0);
	  	}
	  	
	  	/**
		   * Method is public method to call the private method updateOrder()
		   * 
		   * @return void
		   */
		  	public boolean updateOrderStatus(int orderId, OrderStatus status) throws Exception{
		  		Connection conn=null;
		  		boolean result=false;
		  		try{
		  			conn= this.initializeDB();
		  	  	    result = this.updateOrder(conn, orderId, status);
		  	  		System.out.println("Completed updating order table.");
		  		}
		  		catch(Exception e){
		  			throw e;
		  		}
		  		finally{
		  			close();
		  		}
		  		
		  		return result;
		  	}
		  	
		  	public boolean updateOrderTable(int quantity, int price, int orderId) throws Exception{
		  		Connection conn=null;
		  		boolean result=false;
		  		try{
		  			conn= this.initializeDB();
		  	  	    result = this.updateOrderDetails(conn, quantity, price, orderId);
		  	  		System.out.println("Completed updating orderDetails table.");
		  		}
		  		catch(Exception e){
		  			throw e;
		  		}
		  		finally{
		  			close();
		  		}
		  		
		  		return result;
		  	}
		  	
		  	//GET	  	
		  	public Order getOrder(int orderId){
		  		Order matchingOrder= new Order();
		  		Connection getConnection=null;
		  		try {
		  			getConnection= this.initializeDB();
		  			matchingOrder=this.getOrderInfo(getConnection, orderId);
		  			System.out.println("Grand total for this order is: " + matchingOrder.getGrandTotal());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  		return matchingOrder;
		  	}
		  	
		  	private Order getOrderInfo(Connection dbConnection,int orderId) throws Exception{
		  		Order matchingOrder= new Order();
		  		ResultSet resultSet;
		  		
		  		try{
		  	  		String queryStr="select a.*, b.product_id, b.quantity, b.price, b.item_removed FROM orders as a INNER JOIN order_details as b on a.order_id=b.order_id WHERE a.order_id=?";
		 			preparedStatement = dbConnection.prepareStatement(queryStr);
		  	  		preparedStatement.setInt(1, orderId);
		  	  		// execute select SQL statement
					resultSet = preparedStatement.executeQuery();
					while (resultSet.next()){
	  	  	        	 matchingOrder.setCustomerId(resultSet.getInt("customer_id"));//customer id
	  	  	        	 matchingOrder.setGrandTotal(resultSet.getFloat("order_total"));//order total
	  	  	        	 matchingOrder.setOrderDate(resultSet.getDate("order_date_time"));//order date
		  	  	    }
		  		}
		  		catch (Exception e) {
		  		      throw e;
		  		} 
		  	    finally {
		  		      close();
		  		}
		       
		        return matchingOrder;
		  	}
		  	
		    //Delete
		  	public boolean deleteOrder(int orderId){
		  		Connection delConnection=null;
		  		try {
		  			delConnection= this.initializeDB();
		  			this.deleteOrderRecord(delConnection, orderId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  		return true;
		  	}
		  
		  	private boolean deleteOrderRecord(Connection dbConnection,int orderId) throws Exception{
		  		try{
		  	  		String queryStr="delete from order_details WHERE order_id = ?";
		 			preparedStatement = dbConnection.prepareStatement(queryStr);
		  	  		preparedStatement.setInt(1, orderId);
		  	  		// execute select SQL statement
		  	  		preparedStatement.executeUpdate();
			  	  	queryStr = "delete from orders WHERE order_id = ?";
		 			preparedStatement = dbConnection.prepareStatement(queryStr);
		  	  		preparedStatement.setInt(1, orderId);
		  	  		// execute select SQL statement
		  	  		preparedStatement.executeUpdate();
		  		}
		  		catch (Exception e) {
		  		      throw e;
		  		} 
		  	    finally {
		  		      close();
		  		}		       
		        return true;
		  	}
	
	 // You need to close the resultSet
	  private void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }

}
