package com.order;

import java.util.ArrayList;
import java.util.Date;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.product.Product;
import com.order.*;

public class Order {
   
	//Attributes
	
	private int orderId;
	private ArrayList<Product> items;
	private Date orderDate;
	private float grandTotal;
	private float shippingTotal;
	private float tax;
	private float amountPaid;
	private int customerId;
	private int productQty;
	private OrderStatus orderStatus;
	private ArrayList<Order> pastOrders;
	
	// KM added
	private int customer_address_id; 
	private long productId;
	private double productPrice;
	private float subtotal; // this is the total of the product before tax
	private Timestamp orderTimestamp;
	private boolean isCanceled;
	
	//Constructors
	
	public Order() {}
	
	public Order(int ordId, Date orderDt, float grandTtl, float shippingTtl, float Ttltax, float amtpaid, int custId) {
		
		this.items = new ArrayList<Product>();
		this.orderId = ordId;
		this.orderDate = orderDt;
		this.grandTotal = grandTtl;
		this.shippingTotal = shippingTtl;
		this.tax = Ttltax;
		this.amountPaid = amtpaid;
		
		
	}	
	
	public Order( Date orderDt, float grandTtl, float shippingTtl, float Ttltax, float amtpaid, int custId) {
		
		this.items = new ArrayList<Product>();	
		this.orderDate = orderDt;
		this.grandTotal = grandTtl;
		this.shippingTotal = shippingTtl;
		this.tax = Ttltax;
		this.amountPaid = amtpaid;		
	}
	
	public Order(int custId, OrderStatus orderStatus, int customer_address_id, long productId, int productQty) {
		
		this.customerId = custId;
		this.customer_address_id = customer_address_id;
		this.orderStatus = orderStatus;
		this.productId = productId;
		this.productQty = productQty;
	}
	
	//Methods
			public int getOrderId() {
				return orderId;
			
			}
			
			public ArrayList<Product> getItems(){
				return items;
				
			}
			
			public Date getOrderDate() {
				return orderDate;
			}
			
			public Timestamp getOrderTimestamp() {
				return orderTimestamp;
			}
			
			public float getGrandTotal() {
				return grandTotal;
			}
			
			public float getShippingTotal() {
				return shippingTotal;
			}
			
			public float getTax() {
				return tax;
			}
			
			public float getAmountPaid() {
				return amountPaid;
			}
			
			public int getCustomerId() {
				return customerId;
			}
			
			public int getQuantity() {
				return productQty;
			}
			
			public OrderStatus getOrderStatus() {
				return orderStatus;
			}
			
			public ArrayList<Order> getPastOrders() {
				return pastOrders;
			}
			
			// KM added
			public int getCustomerAddrId() {
				return customer_address_id;
			}
			
			public long getProductId() {
				return productId;
			}
			
			public double getProductPrice() {
				return productPrice;
			}
			
			public float getSubtotal() {
				return subtotal;
			}
			
			public boolean getIsCanceled() {
				return isCanceled;
			}
			
			
			
			
			
			public void setorderId(int orderId) {
				this.orderId = orderId;
			}
			
			public void setItems(ArrayList<Product> items) {
				this.items = items;
			}
			
			public void setOrderDate(Date orderDate) {
				this.orderDate = orderDate;
			}
			
			public void setOrderTimestamp(Timestamp orderTimestamp) {
				this.orderTimestamp = orderTimestamp;
			}
			
			public void setGrandTotal(float grandTotal) {
				this.grandTotal = grandTotal;
			}
			
			public void setShippingTotal(float shippingTotal) {
				this.shippingTotal = shippingTotal;
			}
			
			public void setTax(float subTotal) {
				this.tax = (float) (subtotal * .085);
			}
			
			public void setAmountPaid(float amountPaid) {
				this.amountPaid = amountPaid;
			}
			
			public void setCustomerId(int customerId) {
				this.customerId = customerId;
			}
			
			public void setQuantity(int quantity) {
				this.productQty = quantity;
			}
			
			public void setOrderStatus(OrderStatus orderStatus) {
				this.orderStatus = orderStatus;
			}
			
			public void setPastOrders(ArrayList<Order> pastOrders) {
				this.pastOrders = pastOrders;
			}
			//KM added
			public void setCustomerAddrId(int customerAddrId) {
				this.customer_address_id = customerAddrId;
			}
			
			public void setProductId(long productId) {
				this.productId = productId;
			}
			
			public void setProductPrice(double productPrice) {
				this.productPrice = productPrice;
			}
			
			public void setSubtotal(double price, int quantity) {
				this.subtotal = (float) price * quantity;
				
			}
			
			public void setIsCanceled(boolean isCanceled) {
				this.isCanceled = isCanceled;
				
			}
				
					
			
}
