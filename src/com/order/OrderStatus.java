package com.order;



/*
 * STAGE DESCRIPTIONS:
 * 1. Step One immediately after the order is made and customer is charged is to update order status to Processing
 * 2. Step two is to update status to Completed_Preparing_Order when payment is accepted and packaging phase begins
 * 3. Next step is to either update the status to Partially_Shipped or Shipped (when all ordered items are packaged for shipment)
 * 4. Order status is then updated to Delivered when either all items ordered are received by the customer
 * 5. Order status is updated to Fulfilled only when all items ordered is completely shipped
 * 6. Cancelled status refers to either when the customer wants to return the order or 
 * 7. Received_Return status refers to when the customer has sent back the item(s).
 * 8. Processing_Return is when Lakeshore is in the process of performing the refund
 * 9. Completing the return is when the return/refund process is complete.
 * 
 * 	Purchase Workflow: Processing --> Completed_Preparing_Order --> (Partially_Shipped / Shipped) --> Delivered --> Fulfilled
 *  Return Workflow:   Initially order must have a (Delivered or Fulfilled status) to return it 
 */

public enum OrderStatus {
	Processing,						//First-stage in order
	Completed_Preparing_Order,  
	Partially_Shipped,
	Shipped,
	Delivered,
	Fulfilled,
	
	Cancelled,
	Received_Return,
	Processing_Return,
	Completed_Return;
	
	public int getOrderStatusEnum()
	{
		switch(this) {
		    case Processing: return 1;
		    case Completed_Preparing_Order: return 2;
		    case Partially_Shipped: return 3;
		    case Shipped: return 4;
		    case Delivered: return 5;
		    case Fulfilled: return 6;
		    case Cancelled: return 7;
		    case Received_Return: return 8;
		    case Processing_Return: return 9;
		    case Completed_Return: return 10;
			default: return -1;
		} 	
	}	
}