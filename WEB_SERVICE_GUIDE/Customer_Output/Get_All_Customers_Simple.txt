http://localhost:8081/customerservice/customer



CONSOLE OUTPUT:

GET METHOD Request for all customers .............
CustomerDAO: getAllCustomers found 3 customers
Customer DAO,  CustomerId =92, Customer Name: Salah, Alla
Customer DAO,  CustomerId =93, Customer Name: Edwards, Sammy
Customer DAO,  CustomerId =94, Customer Name: Smith, Marry





OUTPUT:

[
    {
        "cid": "92",
        "lastName": "Salah",
        "firstName": "Alla",
        "email": "asalah3@luc.edu"
    },
    {
        "cid": "93",
        "lastName": "Edwards",
        "firstName": "Sammy",
        "email": "sedwards@luc.edu"
    },
    {
        "cid": "94",
        "lastName": "Smith",
        "firstName": "Marry",
        "email": "marry@luc.edu"
    }
]